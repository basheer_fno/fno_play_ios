//
//  ViewController.swift
//  TradingApp
//
//  Created by Pandiyaraj on 06/06/17.
//  Copyright © 2017 ideas2it. All rights reserved.
//

import UIKit
import Charts
//import SocketRocket
import CoreGraphics


//#define DEGREES_TO_RADIANS(__ANGLE) ((__ANGLE) / 180.0 * M_PI)

func BG(_ block: @escaping ()->Void) {
  DispatchQueue.global(qos: .background).async(execute: block)
}

func UI(_ block: @escaping ()->Void) {
  DispatchQueue.main.async(execute: block)
}

class AppTheme : NSObject {
  struct Galaxy {
    static let gradientUpColor  = UIColor.init(hex: 0xda4930) //UIColor(red:0.82, green:0.11, blue:0.40, alpha:1.0)
    static let gradientDownColor = UIColor.init(hex: 0x30ab49) //UIColor(red:0.43, green:0.85, blue:0.48, alpha:1.0)
    
    static let segmentNormalTextColor = UIColor.init(hex: 0x4e5c78)
    static let segmentSelectedTextColor = UIColor.white
    static let segmentSelectedImage = "segmentSelected"
    
    static let textColor = UIColor.init(hex: 0x5d6c82)
    static let notSureTextColor = UIColor.white
    static let notSureTitleTextColor = UIColor.white
    
    static let accountCaptionColor = UIColor.init(hex: 0x4e5c78)
    static let accountValueColor = UIColor.init(hex: 0xE8A61F)
    
    static let amountValueColor = UIColor.init(hex: 0xE8A61F)
    static let investAmountValueColor = UIColor.white
    
    static let profitValueColor = UIColor.init(hex: 0x12C233)
    static let lossValueColor = UIColor.init(hex: 0x3091D6)
    
    static let totalValueColor = UIColor.init(hex: 0xE52A6A)
    
    static let bgImage = "galaxyBG"
    static let rightBGImage = "rightBG"
    
    static let btnGBColor = UIColor.blue
    static let notSureEnabledBG = ""
    static let notSureDisabledBG = ""
    
    static let upDirectionSelecgedBGImage = "ArrUp_Sel"
    static let downDirectionSelectedBGImage = "ArrDown_Sel"
    static let upDirectionNormalBGImage = "ArrUp_UnSel"
    static let downDirectionNormalBGImage = "ArrDown_UnSel"
    
    static let separatorView = UIColor.clear
    
    static let amountButtonColor = UIColor.white
    
    static let notSureColor = UIColor.init(hex: 0xf6ac13)
    
    static let menuButtonImage = "Menu"
    
    static let tabViewSeparatorColor = UIColor.clear
    
    
    static let actionButtonDisabledTextColor = UIColor.init(hex: 0xf1f0ff)
    static let actionButtonNormalTextColor = UIColor.white
    static let actionButtonDisabledBGColor = UIColor.init(hex: 0x192231)
    static let actionButtonSelectedBGColor = UIColor.init(hex: 0x233791)
    
    static let tradeNowButtonDisabledBGColor = UIColor.init(hex: 0x192231)
    static let tradeNowButtonSelectedBGColor = UIColor.init(hex: 0x233791)
    
    static let pointViewBGColor = UIColor.white
    
    static let flagBGColor = UIColor.init(hex: 0xC0402C)
    static let flagImage = "newFlagSet"
    
    static let totalAmountTextColor = UIColor.init(hex: 0xE8A620)
    
    static let chartAxisTextColor = UIColor.lightGray
    
    static let adviceMeImage = "advise_me"
    static let flipACoinImage = "flip_a_coin"
    static let followCrowdImage = "follow_crowd"
    static let notSureCancelImage = "closeIcon"
    
    static let upLineColor = UIColor.init(hex: 0x66CB73)
    static let downLineColor = UIColor.init(hex: 0xCE1B63)
    
    static let percentTopTextColor = UIColor.init(hex: 0xCE1B63)
    static let percentBottomTextColor = UIColor.init(hex: 0x66CB73)
    
    static let downDirBGColor = UIColor.init(hex: 0xda4930)
    static let upDirBGColor = UIColor.init(hex: 0x30ab49)
    
    static let fireworkRedBG = "redFWD"
    static let fireworkRedMiniBG = "redMINI"
    static let fireworkBlueBG = "blueFWD"
    static let fireworkYelloBG = "yellowFWD"
    
    static let gradientTopImage = "gradientUp"
    static let gradientBottomImage = "gradientDown"
    
    static let tradeBtnDisabledBGImage = "trade_bg"
    static let tradeBtnNormalBGImage = "trade_bg_selected"
    
    static let thinLineBGColor = UIColor.init(hex: 0x440626)
    
    static let leftBGColor = UIColor.init(hex: 0x17161A)
  }
  
  struct Galaxy2 {
    static let gradientUpColor  = UIColor.init(hex: 0xFF7800)
    static let gradientDownColor = UIColor.init(hex: 0x6DD97A)
    
    static let segmentNormalTextColor = UIColor.init(hex: 0x4e5c78)
    static let segmentSelectedTextColor = UIColor.white
    static let segmentSelectedImage = "segmentSelected"
    
    static let textColor = UIColor.init(hex: 0x5d6c82)
    static let notSureTextColor = UIColor.white
    static let notSureTitleTextColor = UIColor.white
    
    static let accountCaptionColor = UIColor.init(hex: 0x4e5c78)
    static let accountValueColor = UIColor.init(hex: 0xE8A61F)
    
    static let amountValueColor = UIColor.init(hex: 0xE8A61F)
    static let investAmountValueColor = UIColor.white
    
    static let profitValueColor = UIColor.init(hex: 0x67cd74)
    static let lossValueColor = UIColor.init(hex: 0xa50a4b)
    
    static let totalValueColor = UIColor.init(hex: 0xf6b00a)
    
    static let bgImage = "galaxyBG"
    static let rightBGImage = "rightBG"
    
    static let btnGBColor = UIColor.blue
    static let notSureEnabledBG = ""
    static let notSureDisabledBG = ""
    
    static let upDirectionSelecgedBGImage = "ArrUp_Sel"
    static let downDirectionSelectedBGImage = "ArrDown_Sel"
    static let upDirectionNormalBGImage = "ArrUp_UnSel"
    static let downDirectionNormalBGImage = "ArrDown_UnSel"
    
    static let separatorView = UIColor.clear
    
    static let amountButtonColor = UIColor.white
    
    static let notSureColor = UIColor.init(hex: 0xf6ac13)
    
    static let menuButtonImage = "Menu"
    
    static let tabViewSeparatorColor = UIColor.clear
    
    
    static let actionButtonDisabledTextColor = UIColor.init(hex: 0xf1f0ff)
    static let actionButtonNormalTextColor = UIColor.white
    static let actionButtonDisabledBGColor = UIColor.init(hex: 0x192231)
    static let actionButtonSelectedBGColor = UIColor.init(hex: 0x233791)
    
    
    static let tradeNowButtonDisabledBGColor = UIColor.init(hex: 0x09082B)
    static let tradeNowButtonSelectedBGColor = UIColor.init(hex: 0x243694)
    
    
    
    static let pointViewBGColor = UIColor.white
    
    static let flagBGColor = UIColor.init(hex: 0xC0402C)
    static let flagImage = "newFlagSet"
    
    static let totalAmountTextColor = UIColor.init(hex: 0xE8A620)
    
    static let chartAxisTextColor = UIColor.lightGray
    
    static let adviceMeImage = "advise_me"
    static let flipACoinImage = "flip_a_coin"
    static let followCrowdImage = "follow_crowd"
    static let notSureCancelImage = "closeIcon"
    
    static let upLineColor = UIColor.init(hex: 0x12c233)
    static let downLineColor = UIColor.init(hex: 0xff7827)
    
    static let percentTopTextColor = UIColor.init(hex: 0x798ac2)
    static let percentBottomTextColor = UIColor.init(hex: 0x798ac2)
    
    static let downDirBGColor = UIColor.init(hex: 0xda4930)
    static let upDirBGColor = UIColor.init(hex: 0x30ab49)
    
    static let fireworkRedBG = "redFWD"
    static let fireworkRedMiniBG = "redMINI"
    static let fireworkBlueBG = "blueFWD"
    static let fireworkYelloBG = "yellowFWD"
    
    static let gradientTopImage = "gradientUp"
    static let gradientBottomImage = "gradientDown"
    
    static let tradeBtnDisabledBGImage = "trade_bg"
    static let tradeBtnNormalBGImage = "trade_bg_selected"
    
    static let thinLineBGColor = UIColor.init(hex: 0x440626)
    
    static let leftBGColor = UIColor.init(hex: 0x17161A)
  }
  
  struct White {
    static let gradientUpColor  = UIColor.init(hex: 0xFF7800)
    static let gradientDownColor = UIColor.init(hex: 0x6DD97A)
    
    static let segmentNormalTextColor = UIColor.init(hex: 0x4E5C78)
    static let segmentSelectedTextColor = UIColor.init(hex: 0x2E3B81)
    static let segmentSelectedImage = "segmentSelectedWhite"
    
    static let textColor = UIColor.init(hex: 0x5d6c82)
    static let notSureTextColor = UIColor.init(hex: 0x2E3B81)
    static let notSureTitleTextColor = UIColor.init(hex: 0x768cb5)
    
    
    static let accountCaptionColor = UIColor.init(hex: 0x4E5C78)
    static let accountValueColor = UIColor.init(hex: 0x2E3B81)
    
    static let amountValueColor = UIColor.init(hex: 0x3091D6)
    static let investAmountValueColor = UIColor.init(hex: 0x3091D6)
    
    static let profitValueColor = UIColor.init(hex: 0x12C233)
    static let lossValueColor = UIColor.init(hex: 0xaf1857)
    
    static let totalValueColor = UIColor.init(hex: 0xf6b00a)
    
    static let bgImage = "whiteBG"
    static let rightBGImage = "rightBGWhite"
    
    static let btnGBColor = UIColor.blue
    static let notSureEnabledBG = ""
    static let notSureDisabledBG = ""
    
    static let upDirectionSelecgedBGImage = "ArrUp_SelWhite"
    static let downDirectionSelectedBGImage = "ArrDown_SelWhite"
    static let upDirectionNormalBGImage = "ArrUp_UnSelWhite"
    static let downDirectionNormalBGImage = "ArrDown_UnSelWhite"
    
    static let separatorView = UIColor.init(hex: 0x419cdc)
    
    static let amountButtonColor = UIColor.init(hex: 0x3091d6)
    
    static let notSureColor = UIColor.init(hex: 0x3091D6)
    
    static let menuButtonImage = "MenuWhite"
    
    static let tabViewSeparatorColor = UIColor.init(hex: 0x419cdc)
    
    static let actionButtonDisabledTextColor = UIColor.init(hex: 0xa0d5ef)
    static let actionButtonNormalTextColor = UIColor.white
    static let actionButtonDisabledBGColor = UIColor.init(hex: 0xE7EBED)
    static let actionButtonSelectedBGColor = UIColor.init(hex: 0x2B7BB3)
    
    
    static let tradeNowButtonDisabledBGColor = UIColor.init(hex: 0xE7EBED)
    static let tradeNowButtonSelectedBGColor = UIColor.init(hex: 0x2B7BB3)
    
    static let pointViewBGColor = UIColor.init(hex: 0xb21658)
    
    static let flagBGColor = UIColor.init(hex: 0xb21658)
    static let flagImage = "newFlagSetWhite"
    
    static let totalAmountTextColor = UIColor.init(hex: 0xe52a6a)
    
    static let chartAxisTextColor = UIColor.init(hex: 0x768cb5)
    
    static let adviceMeImage = "advise_meWhite"
    static let flipACoinImage = "flip_a_coinWhite"
    static let followCrowdImage = "follow_crowdWhite"
    static let notSureCancelImage = "closeIconWhite"
    
    static let upLineColor = UIColor.init(hex: 0x6DD97A)
    static let downLineColor = UIColor.init(hex: 0xFF7800)
    
    static let percentTopTextColor = UIColor.init(hex: 0x798ac2)
    static let percentBottomTextColor = UIColor.init(hex: 0x798ac2)
    
    static let downDirBGColor = UIColor.init(hex: 0xFF7800)
    static let upDirBGColor = UIColor.init(hex: 0x6DD97A)
    
    static let fireworkRedBG = "redFWDWhite"
    static let fireworkRedMiniBG = ""
    static let fireworkBlueBG = "blueFWDWhite"
    static let fireworkYelloBG = "yellowFWDWhite"
    
    static let gradientTopImage = "gradientUpWhite"
    static let gradientBottomImage = "gradientDownWhite"
    
    static let tradeBtnDisabledBGImage = "trade_bg_white"
    static let tradeBtnNormalBGImage = "trade_bg_selected_white"
    
    static let thinLineBGColor = UIColor.init(hex: 0xD796B2)
    
    static let leftBGColor = UIColor.white
  }
}


enum Theme {
  case galaxy
  case galaxy2
  case white
}

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,ChartViewDelegate,UISearchBarDelegate,UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
  
  //MARK: Outlet Properties
  // For Menu Option
  
  var orderDetailRetryDuration = 3.0
  var theme : Theme = .galaxy2
  
  var chartReloadCount = 0
  
  @IBOutlet weak var bgImageView : UIImageView!
  
  var menuArray = [String]()
  @IBOutlet weak var menuBtn: UIButton!
  @IBOutlet weak var leftMenuView :UIView!
  
  let cellReuseIdentifier = "cell"
  
  var dataFeedTimer = Timer()  // For whitebi
  var labelTimer = Timer()
  var tradeTimer = Timer()
  var exitCompareTimer = Timer()
  var drawChartTimer : Timer?
  
  var accessToken : String = ""
  
  @IBOutlet weak var chartView: LineChartView!
  
  @IBOutlet weak var sliderXposition: NSLayoutConstraint!
  @IBOutlet weak var notSureSliderXposition: NSLayoutConstraint!
  
  @IBOutlet weak var redFlagView: UIView!
  @IBOutlet weak var redFlagLeadingConstraint: NSLayoutConstraint!
  
  var isFutureOrOption : Bool = true
  var isChartDataAvaible : Bool = false
  var is60MinChartCompleted : Bool = false
  // For chart gradient Line
  var gradientTimer = Timer()
  var notSureGradientTimer = Timer()
  var values = [-15,-30,-40,-10,0,10,15,25,-25,-35]
  @IBOutlet weak var topView: UIView!
  @IBOutlet weak var bottomView: UIView!
  @IBOutlet weak var topImageView: UIImageView!
  @IBOutlet weak var bottomImageView: UIImageView!
  
  @IBOutlet weak var notSureTopView: UIView!
  @IBOutlet weak var notSureBottomView: UIView!
  
  var gradient1 = CAGradientLayer()
  var gradient2 = CAGradientLayer()
  
  var viewFrame = CGRect()
  @IBOutlet weak var callLabel: UILabel!
  @IBOutlet weak var putLabel: UILabel!
  //* Top amount label *//
  @IBOutlet weak var amountLabel: UILabel!
  @IBOutlet weak var topPercentlbl: UILabel!
  @IBOutlet weak var botPercentLbl: UILabel!
  
  @IBOutlet weak var notSureTopPercentlbl: UILabel!
  @IBOutlet weak var notSureBottomPercentLbl: UILabel!
  
  // for tabView
  var tabCount: Int = 1
  @IBOutlet weak var selCollectionView: UICollectionView!
  @IBOutlet weak var addCountryBtn: UIButton!
  var segmentItems = ["1M","5D","2D","1D","60m","15m","5m"]
  var selectedCellIndex:IndexPath?
  var preSelectedCellIndex:IndexPath?
  var currentPrice : Double = 0.0
  var isUporDown : Int = 0
  var axisIncrementValue : Double = 2.0
  
  @IBOutlet weak var customAlertView: UIView!
  @IBOutlet weak var thinLineView : UIView!
  var chartViewDelegate :  ChartViewDelegate!
  let arrowPopupView = ArrowPopUp()
  
  var timeDuration = 90.0
  // For tab Collection View
  @IBOutlet weak var collectionViewLeadingConstraint: NSLayoutConstraint!
  
  //Temporary array
  var currentDocumentsDict = [String : Any]()
  
  var tabDocumentsDict = [[String : Any]]()
  
  var instrumentDict = [[String : Any]]()  // By default Futures
  
  var tradePointsDict = Dictionary<String,String>()
  var olderEntries = Dictionary<String,AnyObject>()
  
  // For PopUp TableView
  @IBOutlet weak var popUpView: UIView!
  @IBOutlet weak var popUpTableView: UITableView!
  @IBOutlet weak var popUpViewLeadingConstraint: NSLayoutConstraint!
  @IBOutlet weak var dateLbl: UILabel!
  @IBOutlet weak var dateLblHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var gradientLineView: UIView!
  
  @IBOutlet weak var notSureGradientLineView: UIView!
  
  //For Buttonsincrem
  @IBOutlet weak var futuresBtn: UIButton!
  @IBOutlet weak var optionsBtn: UIButton!
  @IBOutlet weak var indiciesBtn: UIButton!
  @IBOutlet weak var searchBtn: UIButton!
  @IBOutlet weak var searchbar: UISearchBar!
  @IBOutlet weak var  buttonLeftBorderConstraint: NSLayoutConstraint!
  @IBOutlet weak var  filterBtnTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var  searchViewHeightConstraint: NSLayoutConstraint!
  
  
  @IBOutlet weak var takeProfitViewLblLeadingConstraint: NSLayoutConstraint!
  
  var previousTag : Int = 0
  var buttonsScrollView = UIScrollView()
  
  
  /// Create a variables for a diffent chart data set
  var xAxisDateArray  = [String]()
  var xAxisDateTimeArray = [Date]()
  var historicalYvaluesArray  = [Double]()
  
  var hoursXAxisDateArray  = [String]()
  var hoursHistoricalYvaluesArray  = [Double]()
  var hoursxAxisDateTimeArray = [Date]()
  
  var minuteXAxisDateArray  = [String]()
  var minuteHistoricalYvaluesArray  = [Double]()
  var minutexAxisDateTimeArray = [Date]()
  
  var secondXAxisDateArray  = [String]()
  var secondHistoricalYvaluesArray  = [Double]()
  var secondxAxisDateTimeArray = [Date]()
  
  var tenSecondsXAxisDateArray  = [String]()
  var tenSecondseHistoricalYvaluesArray  = [Double]()
  var tenSecondsxAxisDateTimeArray = [Date]()
  
  var tenMinutesXAxisDateArray  = [String]()
  var tenMinutesHistoricalYvaluesArray  = [Double]()
  var tenMinutesxAxisDateTimeArray = [Date]()
  
  var fiveSecondsXAxisDateArray  = [String]()
  var fiveSecondseHistoricalYvaluesArray  = [Double]()
  var fiveSecondsxAxisDateTimeArray = [Date]()
  
  var loadingXAxisDateArray  = [String]()
  var loadingHistoricalYvaluesArray  = [Double]()
  var loadingXAxisDateTimeArray = [Date]()
  
  var currentDataCount = 0
  var timeIntervalForFlag: Int = 0
  
  
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  // Bottom - Segment Control
  @IBOutlet weak var  pointViewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var  pointViewLeadingConstraint: NSLayoutConstraint!
  
  
  @IBOutlet weak var greenLineViewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var redLineViewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var greenLineView: UIView!
  @IBOutlet weak var redLineView: UIView!
  @IBOutlet weak var greenLineLabelValue: UILabel!
  @IBOutlet weak var redLineLabelValue: UILabel!
  
  // Animating View
  @IBOutlet weak var outerView: UIView!
  @IBOutlet weak var pointView: UIView!
  
  
  // Marker Lines
  @IBOutlet weak var dataPointView: UIView!
  @IBOutlet weak var dataPointViewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var dataFeedYPoint: UILabel!
  @IBOutlet weak var tagDataValue: UILabel!
  
  var pointValue : CGFloat = 0
  
  var prevDataPoint :CGPoint?
  var prevActualDataPoint :CGPoint?
  weak var shapeLayer: CAShapeLayer?
  
  // chartData
  var lastTradePrice: Double = 0
  var isMarketTimeClosed : Bool = false
  
  var selectedFilter = "All Assets"
  
  var isAlreadyDownloaded : Bool = false
  var is60MinProcessing : Bool = false
  var performCloseOrder : Bool = false
  
  //MARK: New side menu
  
  //Trade view
  
  @IBOutlet weak var accountTitleLabel: UILabel!
  @IBOutlet weak var fnoAccountTitleLabel: UILabel!
  @IBOutlet weak var fnoTradingTitleLabel: UILabel!
  @IBOutlet weak var fnoAccountView: UIView!
  @IBOutlet weak var fnoAccountImageView: UIImageView!
  @IBOutlet weak var fnoAccountGestureView: UIView!
  @IBOutlet weak var investTitleLabel: UILabel!
  @IBOutlet weak var predictDirectionTitleLabel: UILabel!
  
  @IBOutlet weak var tradeView: UIView!
  
  @IBOutlet weak var amountDecrementBtn: UIButton!
  @IBOutlet weak var amountIncrementBtn: UIButton!
  @IBOutlet weak var amountView: UIView!
  @IBOutlet weak var amountValuelbl: UILabel!
  @IBOutlet weak var downDirBtn: UIButton!
  @IBOutlet weak var upDirBtn: UIButton!
  @IBOutlet weak var notSureBtn: UIButton!
  
  @IBOutlet weak var tradeBtn: UIButton!
  
  //Notsure View
  @IBOutlet weak var notsureTitleLabel: UILabel!
  @IBOutlet weak var adviseMeTitleLabel: UILabel!
  @IBOutlet weak var followTheCrowdTitleLabel: UILabel!
  @IBOutlet weak var flipACoinTitleLabel: UILabel!
  
  @IBOutlet weak var notSureView: UIView!
  @IBOutlet weak var cancelBtn: UIButton!
  @IBOutlet weak var adviseMeBtn: UIButton!
  @IBOutlet weak var followCrowdBtn: UIButton!
  @IBOutlet weak var flipCoinBtn: UIButton!
  
  // Take profit view
  @IBOutlet weak var investedTitleLabel: UILabel!
  @IBOutlet weak var directionTitleLabel: UILabel!
  @IBOutlet weak var profitTitleLabel: UILabel!
  @IBOutlet weak var totalTitleLabel: UILabel!
  
  @IBOutlet weak var takeProfitView: UIView!
  @IBOutlet weak var investedValuelbl: UILabel!
  @IBOutlet weak var profitPercentValuelbl: UILabel!
  @IBOutlet weak var totalValuelbl: UILabel!
  @IBOutlet weak var takeProfitBtn: UIButton!
  @IBOutlet weak var takeProfitDirectionBtn: UIButton!
  
  // Trade & Take profit alert View
  
  @IBOutlet weak var takeProfitAlertView: UIView!
  @IBOutlet weak var takeProfitviewAmountLabel : UILabel!
  @IBOutlet weak var takeProfitCongratsLabel: UILabel!
  @IBOutlet weak var takeProfitSuccessLabel: UILabel!
  @IBOutlet weak var takeProfitRedBG: UIImageView!
  @IBOutlet weak var takeProfitRedMini: UIImageView!
  @IBOutlet weak var takeProfitBlueBG: UIImageView!
  @IBOutlet weak var takeProfitYellowBG: UIImageView!
  
  @IBOutlet weak var tradeAlertView: UIView!
  @IBOutlet weak var tradedAmountLabel: UILabel!
  @IBOutlet weak var tradeExcuteLabel: UILabel!
  @IBOutlet weak var keepOneyeLabel: UILabel!
  #if ENABLE_DB
  
  var currentDocumentsDictDB = Instruments()
  
  var tabDocumentsDictDB = [Instruments]()
  var instrumentDictDB = [Instruments]()  // By default Futures
  
  var futureSegmentsDB = [Instruments]()
  var optionSegmentsDB = [Instruments]()
  
  var loadingHistoryInSecondsDB = [InstrumentsInSeconds]()
  var loadingHistoryInMinutesDB = [InstrumentsInMinutes]()
  var loadingHistoryInHoursDB = [InstrumentsInHours]()
  
  var xAxisArrayDB = [InstrumentsInSeconds]()
  var secondsXAxisArrayDB = [InstrumentsInSeconds]()
  var minutesXAxisArrayDB = [InstrumentsInMinutes]()
  var hoursXAxisArrayDB = [InstrumentsInHours]()
  var tenSecondsXAxisArrayDB = [InstrumentsInSeconds]()
  var tenMinutesXAxisArrayDB = [InstrumentsInMinutes]()
  var fiveSecondsXAxisArrayDB = [InstrumentsInSeconds]()
  
  #endif
  var futureSegments = [Dictionary<String,Any>]()
  var optionSegments = [Dictionary<String,Any>]()
  
  
  // For Filter
  
  @IBOutlet weak var filterTitleLabel: UILabel!
  
  @IBOutlet weak var filterTableView: UITableView!
  var filterArray = ["All Assets", "Indices","Equities", "Forex","Commodities"]
  var showAllFilter:Bool = false
  @IBOutlet weak var filterTableHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var activityView: UIView!
  @IBOutlet weak var rightActivityView: UIView!
  @IBOutlet weak var rightActivityLabeltext : UILabel!
  
  @IBOutlet weak var  tradeViewtopConstraint: NSLayoutConstraint!
  @IBOutlet weak var  takeProfitViewDirectionBtmConstraint: NSLayoutConstraint!
  @IBOutlet weak var  takeProfitViewProfitLblTopConstraint: NSLayoutConstraint!
  
  
  @IBOutlet weak var  advisemeTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var  crowdTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var  flipTopConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var toptabView: UIView!
  @IBOutlet weak var activityLabeltext : UILabel!
  @IBOutlet weak var insideActivityView: UIView!
  
  var tradePoints = Dictionary<String,String>()
  
  // For right side BG
  var currentAmountValue : Int = 0  // It is invested amount value
  var depositAmountVlue : Int = 0   // Total amount from ur account .. each day reset to 10000
  let iniitalAmountValue : Int = 0   // By default which is inital value
  var currentDepositAmountValue : Int = 0  //Change deposit amount value
  var lastClosedPrice:Double = 0.0
  var timelimit : Double = 0
  var lastTimeStamp:Double!
  var selectedSegmentIndex = 0
  var previousSelectedSegmentIndex = 0
  
  var secondsLimit = 0
  var minutesLimit = 0
  var hoursLimit = 0
  var lowestVisibleX = 0.0
  var tenSecondsLimt = 0
  var tenMinutesLimit = 0.0
  var fiveSecondsLimt = 0
  
  var isResetTrade : Bool = false
  var isCurrentNiftyLoaded : Bool = false
  var isCurrentExistingSymbol : Bool = false
  var isSecondsCompleted : Bool = false
  var isMarketClosd : Bool = false
  var islast10Obj : Bool = false
  
  var relaunchButtonAction = true
  
  @IBOutlet weak var  yellowPointViewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var  yellowLineView: UIView!
  @IBOutlet weak var  orangeLineView: UIView! // mark the end of green and orange line +-100%
  
  
  //Right BG
  @IBOutlet weak var notSureRightBG: UIImageView!
  @IBOutlet weak var tradeViewRightBG: UIImageView!
  @IBOutlet weak var tradeAlertViewRightBG: UIImageView!
  @IBOutlet weak var takeProfitViewRightBG: UIImageView!
  @IBOutlet weak var popupViewRightBG: UIImageView!
  @IBOutlet weak var takeProfitAlertViewRightBG: UIImageView!
  
  @IBOutlet weak var separatorView: UIView!
  @IBOutlet weak var tabViewSeparatorLbl: UILabel!
  @IBOutlet weak var switchTheme : UISwitch!
  @IBOutlet weak var switchBtn: UIButton!
  
  @IBOutlet weak var flagView: UIView!
  @IBOutlet weak var flagImageView: UIImageView!
  @IBOutlet weak var switchBGImageView: UIImageView!
  @IBOutlet weak var tradeAgainBtn: UIButton!
  
  var appThemeUpdating : Bool = false
  
  var leftChartBarClicked : Bool = false
  @IBOutlet weak var leftBarChartButton : UIButton!
  
  @IBOutlet weak var notsureAdviceMeView : UIView!
  @IBOutlet weak var notsureAdviceMeCloseBtn : UIButton!
  @IBOutlet weak var notsureAdviceMeBackBtn : UIButton!
  
  @IBOutlet weak var notsureFollowTheCrowdView : UIView!
  @IBOutlet weak var notsureFollowTheCrowdCloseBtn : UIButton!
  @IBOutlet weak var notsureFollowTheCrowdBackBtn : UIButton!
  @IBOutlet weak var notsureFollowTheCrowdUpArrow : UIImageView!
  @IBOutlet weak var notsureFollowTheCrowdDownArrow : UIImageView!
  
  @IBOutlet weak var notsureFlipACoinView : UIView!
  @IBOutlet weak var notsureFlipACoinCloseBtn : UIButton!
  @IBOutlet weak var notsureFlipACoinBackBtn : UIButton!
  @IBOutlet weak var notsureFlipACoinDirectionBtn : UIButton!
  @IBOutlet weak var notsureFlipACoinDirectionLabel : UILabel!
  
  
  @IBOutlet weak var notsureFlipACoinFrontView : UIView!
  @IBOutlet weak var notsureFlipACoinBackView : UIView!
  
  @IBOutlet var frontView : UIImageView!
  @IBOutlet var backView : UIImageView!
  
  
  /// Set font for label and buttons
  func loadDefaultFont() -> Void {
    // Set font for all labels
    // FilterView
    self.filterTitleLabel.font = AppFont.getRegular(pixels: 23)
    
    // Gradient View
    self.callLabel.font = AppFont.getRegular(pixels: 18)
    self.putLabel.font = AppFont.getRegular(pixels: 18)
    self.topPercentlbl.font = AppFont.getRegular(pixels: 20)
    self.botPercentLbl.font = AppFont.getRegular(pixels: 20)
//    self.notSureTopPercentlbl.font = AppFont.getRegular(pixels: 20)
//    self.notSureBottomPercentLbl.font = AppFont.getRegular(pixels: 20)
    
    //-- Trade view
    self.accountTitleLabel.font = AppFont.getLight(pixels: 25)
    self.fnoAccountTitleLabel.font = AppFont.getLight(pixels: 25)
    self.fnoTradingTitleLabel.font = AppFont.getLight(pixels: 25)
    self.investTitleLabel.font = AppFont.getLight(pixels: 25)
    self.predictDirectionTitleLabel.font = AppFont.getLight(pixels: 25)
    self.amountLabel.font = AppFont.getBold(pixels: 35)
    self.amountValuelbl.font = AppFont.getBold(pixels: 55)
    
    //-- Notsure View
    self.notsureTitleLabel.font = AppFont.getLight(pixels: 35)
    self.adviseMeTitleLabel.font = AppFont.getLight(pixels: 25)
    self.followTheCrowdTitleLabel.font = AppFont.getLight(pixels: 25)
    self.flipACoinTitleLabel.font = AppFont.getLight(pixels: 25)
    
    // Take profit View
    self.investedTitleLabel.font = AppFont.getLight(pixels: 25)
    self.directionTitleLabel.font = AppFont.getLight(pixels: 25)
    self.profitTitleLabel.font = AppFont.getLight(pixels: 25)
    self.totalTitleLabel.font = AppFont.getLight(pixels: 25)
    
    self.investedValuelbl.font = AppFont.getBold(pixels: 35)
    //        self.profitPercentValuelbl.font = AppFont.getBold(pixels: 43)
    //        self.totalValuelbl.font = AppFont.getBold(pixels: 48)
    
    //-- line view labels
    
    self.greenLineLabelValue.font = AppFont.getMedium(pixels: 30)
    self.redLineLabelValue.font = AppFont.getMedium(pixels: 30)
    
    //-- Tag view labels
    self.tagDataValue.font = AppFont.getRegular(pixels: 35)
    self.dataFeedYPoint.font = AppFont.getRegular(pixels: 25)
    
    
    // set font for all buttons
    self.notSureBtn.titleLabel?.font = AppFont.getRegular(pixels: 25)
    self.tradeBtn.titleLabel?.font = AppFont.getBold(pixels: 40)
    self.futuresBtn.titleLabel?.font = AppFont.getRegular(pixels: 25)
    self.optionsBtn.titleLabel?.font = AppFont.getRegular(pixels: 25)
    self.takeProfitBtn.titleLabel?.font = AppFont.getBold(pixels: 40)
    self.tradeBtn.titleLabel?.font = AppFont.getBold(pixels: 40)
    
    
    //        self.amountLabel.textAlignment = .center
    //For $ Options
    self.amountLabel.font = AppFont.getBold(pixels: 30)
    self.amountValuelbl.font = AppFont.getBold(pixels: 43)
    self.profitPercentValuelbl.font = AppFont.getBold(pixels: 47)
    self.totalValuelbl.font = AppFont.getBold(pixels: 60)
    self.investTitleLabel.font = AppFont.getLight(pixels: 24)
    self.predictDirectionTitleLabel.font = AppFont.getLight(pixels: 24)
    self.tradeBtn.isUserInteractionEnabled = false
    self.tradeBtn.titleLabel?.textColor = UIColor.init(hex: 0x3a475e)
    
    
    //Trade alert view
    
    self.tradeExcuteLabel.font = AppFont.getMedium(pixels: 33)
    self.keepOneyeLabel.font = AppFont.getBold(pixels: 52)
    self.tradedAmountLabel.font = AppFont.getBold(pixels: 38)
    //Take profit view
    
    self.takeProfitviewAmountLabel.font = AppFont.getBold(pixels: 30)
    self.takeProfitSuccessLabel.font = AppFont.getMedium(pixels: 33)
    self.takeProfitCongratsLabel.font = AppFont.getBold(pixels: 52)
  }
  
  //MARK: View
  
  /// Viewdidload
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.view.sendSubview(toBack: self.notsureAdviceMeView)
    self.view.sendSubview(toBack: self.notsureFlipACoinView)
    self.view.sendSubview(toBack: self.notsureFollowTheCrowdView)
    
    AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeLeft)
    
    orangeLineView.backgroundColor =  UIColor(patternImage: UIImage.drawDottedImage(width: (orangeLineView?.frame.size.width)!, height: (orangeLineView?.frame.size.height)!, color:AppTheme.Galaxy.gradientDownColor))
    
    
    leftMenuView.backgroundColor = UIColor.init(hex: 0x17161A)
    chartReloadCount = 0
    
    self.topImageView.image = UIImage.init(named: AppTheme.Galaxy.gradientTopImage)
    self.bottomImageView.image = UIImage.init(named: AppTheme.Galaxy.gradientBottomImage)
    
    tradeBtn.setTitleColor(AppTheme.Galaxy.actionButtonDisabledTextColor, for: .normal)
    tradeBtn.setBackgroundImage(UIImage(named: AppTheme.Galaxy.tradeBtnDisabledBGImage), for: .normal)
    
    callLabel.textColor = AppTheme.Galaxy.gradientDownColor
    putLabel.textColor = AppTheme.Galaxy.gradientUpColor
    topPercentlbl.textColor = AppTheme.Galaxy.percentTopTextColor
    botPercentLbl.textColor = AppTheme.Galaxy.percentBottomTextColor
    
    filterTableView.tableHeaderView = nil
    // hide the yellow line
    self.yellowLineView.isHidden = true
    
    //
    isFutureOrOption = true
    
    
    
    self.resetTrade()
    
    self.tagDataValue.isHidden = false
    
    // clear the stored values from array
    self.clearAllArrays()
    
    // blur the backgroud view
    self.activityView.backgroundColor = UIColor.clear.withAlphaComponent(0.7)
    
    // Hide inital white dot and tag line view
    self.showOrHidePointValues(isHide: true)
    
    // update the ui with default font
    self.loadDefaultFont()
    
    // hide the views initailly
    takeProfitView.isHidden = true
    notSureView.isHidden = true
    tradeView.isHidden = false
    
    // set the first index as selected
    selectedCellIndex = IndexPath(row: 0, section: 0)
    
    // if lot size is available in currentDocumentDict, update user defaults
    let lotSize = currentDocumentsDict["lot_size"] as? String
    if lotSize != nil{
      let lotSizeVar = lotSize
      let lotSizeValue = Double(lotSizeVar!)
      UserDefaults.standard.set(lotSizeValue!, forKey: "lot_size")
    }else{
      UserDefaults.standard.set(currentDocumentsDict["lot_size"], forKey: "lot_size")
    }
    
    // create notification observers, this would be called from other classes to update chart and ui in this class
    NotificationCenter.default.addObserver(self, selector: #selector(self.updatelayers(notification:)), name: Notification.Name("updatelayers"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(self.updateSegmentButtonForScale(notification:)), name: Notification.Name("UpdateSegmentControl"), object: nil)
    
    
    NotificationCenter.default.addObserver(self, selector: #selector(updateYaxisValue), name: NSNotification.Name(rawValue: "updateYaxis"), object: nil)
    
    
    NotificationCenter.default.addObserver(self, selector: #selector(self.resetTrade), name: NSNotification.Name(rawValue: "resetTrade"), object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(self.addNewChartData), name: Notification.Name("chartReloaded"), object: nil)
    
    
    // add tap gesture to chart view
    let tapGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(chartViewTapped))
    chartView.addGestureRecognizer(tapGestureRecogniser)
    
    // update ui positions for labels
    callLabel.frame = CGRect(x: bottomView.frame.origin.x, y: self.bottomView.frame.origin.y + self.bottomView.height - 30, width: 8.5, height: 35)
    
    callLabel.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
    if DeviceType.IS_IPHONE_6P{
      callLabel.frame = CGRect(x: bottomView.frame.origin.x, y: self.bottomView.frame.origin.y + self.bottomView.height+20, width: 8.5, height: 35)
    }else{
      callLabel.frame = CGRect(x: bottomView.frame.origin.x, y: self.bottomView.frame.origin.y + self.bottomView.height - 30, width: 8.5, height: 35)
    }
    putLabel.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
    putLabel.frame = CGRect(x: topView.frame.origin.x, y: topView.frame.origin.y - 35, width:8.5, height: 30)
    
    selCollectionView.register(UINib(nibName: "TabCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "TabCollectionViewCell")
    
    popUpTableView.register(UINib(nibName: "PopUpTableViewCell", bundle:nil), forCellReuseIdentifier: "PopUpTableViewCell")
    popUpTableView.separatorStyle = .none
    popUpTableView.tableFooterView = UIView()
    
    
    viewFrame = topView.frame
    gradient1.frame = topView.bounds
    gradient2.frame = bottomView.bounds
    
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTap))
    tapGesture.numberOfTapsRequired = 1
    topView.addGestureRecognizer(tapGesture)
    bottomView.addGestureRecognizer(tapGesture)
    gradientLineView.addGestureRecognizer(tapGesture)
    //setSliderColor()
    
    
    // create timer for loading data feed every 1 second
    dataFeedTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.animatePoint), userInfo: nil, repeats: true)
    
    // create timer to update gradient slider every 5 second
    gradientTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
    
    
    // Do any additional setup after loading the view, typically from a nib.
    //        self.view.bringSubview(toFront: self.menuTableView)
    
    self.popUpViewLeadingConstraint.constant = -350
    //self.moreOptionLeadingConstraint.constant = 60
    
    
    self.loadSegmentControl()
    self.initialChartViewUpdate()
    self.initialSetUpOfTradeNowView()
    // For Side View
    
    
    dateLbl.text = self.stringFrom(given: Date(), formatType: "dd MMM HH:mm:ss").uppercased()
    //dateLbl.backgroundColor = UIColor.init(hex: 0x272e42)
    dateLbl.textColor = UIColor.init(hex: 0x4e5c78)
    dateLbl.font = AppFont.getRegular(pixels: 22)//UIFont.init(name: "Roboto-Regular", size: 10.0)
    // Amount Section
    amountValuelbl.text = "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
    //AmountView.roundCorners([.topLeft,.topRight], radius: 5)
    
    amountDecrementBtn.roundCornersButton([.bottomLeft], radius: 5)
    amountIncrementBtn.roundCornersButton([.bottomRight], radius: 5)
    
    // For Direction
    
    
    searchbar.isHidden = true
    searchViewHeightConstraint.constant = 40
    filterBtnTopConstraint.constant = 7
    menuBtn.tintColor = UIColor(white: 0.75, alpha: 0.75)
    
    //Top depost amount
    amountLabel.text = "\(CommonValues.indianRpsUTF)\((depositAmountVlue - currentAmountValue).rupeesFormatt())"
    
    if DeviceType.IS_IPHONE_6P || DeviceType.IS_IPHONE_6 {
      //chartView.backgroundColor = UIColor(patternImage: UIImage.init(named: "ChartNew_BG")!)
    }
    else{
      chartView.backgroundColor = UIColor(patternImage: UIImage.init(named: "chart_BG")!)
    }
    
    
    // Animating Point
    
    if ( DeviceType.IS_IPHONE_6P) {
      pointValue = 450
      tradeViewtopConstraint.constant = 45
      //            takeProfitViewProfitLblTopConstraint.constant = -30
      advisemeTopConstraint.constant = -5
      flipTopConstraint.constant = -5
      crowdTopConstraint.constant = -5
    }else if (DeviceType.IS_IPHONE_6 ){
      pointValue = 420
      tradeViewtopConstraint.constant = 30
      //            takeProfitViewProfitLblTopConstraint.constant = -20
      advisemeTopConstraint.constant = -5
      flipTopConstraint.constant = -5
      crowdTopConstraint.constant = -5
    }
    else if (DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS)
    {
      pointValue = 420
      tradeViewtopConstraint.constant = 15
      //takeProfitViewProfitLblTopConstraint.constant = -20
      advisemeTopConstraint.constant = -5
      flipTopConstraint.constant = -5
      crowdTopConstraint.constant = -5
    }else{
      
      pointValue = 800
    }
    
    self.showOrHideLineViews(isShow: false, upOrDown: false)
    
    
    //#-- Trade market closed
    // boolean value defines market open/close time
    self.isMarketTimeClosed = Date().isMarketTime
    
    // create timer to monitor market open/close timings
    self.compareCloseTime()
    exitCompareTimer =  Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(compareCloseTime), userInfo: nil, repeats: true)
    
    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(dateLblTimeTick), userInfo: nil, repeats: true)
    
    // resetting the values
    instrumentDict = []
    currentDocumentsDict = [:]
    tabDocumentsDict = []
    
    investedValuelbl.text = "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
    redLineView.backgroundColor = UIColor.init(hex: 0xda4930)
    greenLineView.backgroundColor = UIColor.init(hex: 0x28e028)
    amountView.backgroundColor = UIColor.clear //UIColor.init(patternImage: UIImage(named: "rightSideBG")!)
    
    arrowPopupView.frame = CGRect(x: 80, y:  150, width: 200, height: 50)  // For showing arrow in selectedsegment
    arrowPopupView.backgroundColor = UIColor.clear
    arrowPopupView.layer.cornerRadius = 5.0
    self.view.addSubview(arrowPopupView)
    arrowPopupView.isHidden = true
    
    
    UserDefaults.standard.set(currentAmountValue, forKey: "investedAmount")
    searchbar.textColor = UIColor.white
    
    
    
    //Yellow view
    yellowLineView.backgroundColor =  UIColor(patternImage: UIImage.drawDottedImage(width: (yellowLineView?.frame.size.width)!, height: (yellowLineView?.frame.size.height)!, color:UIColor.init(hex: 0xf49000)))
    
    // by default, show search bar
    searchbar.isHidden = false
    searchViewHeightConstraint.constant = 80
    filterBtnTopConstraint.constant = 50
    
    amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
    amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
    
    
    UI {
      self.activityView.isHidden = false
      self.view.bringSubview(toFront: self.activityView)
      self.activityView.bringSubview(toFront: self.insideActivityView)
    }
    
    //    // app should always connect to FnO backend to get data
    //    self.perform(#selector(downloadConfig), with: nil, afterDelay: 1.0)
    
    
    // remove existing saved values
    
    
    let tap =  UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
    tap.delegate = self
    accountTitleLabel.isUserInteractionEnabled = true
    accountTitleLabel.addGestureRecognizer(tap)
    
    amountView.isUserInteractionEnabled = true
    amountView.addGestureRecognizer(tap)
    
    fnoAccountView.isUserInteractionEnabled = true
    fnoAccountView.addGestureRecognizer(tap)
    
    fnoAccountTitleLabel.isUserInteractionEnabled = true
    fnoTradingTitleLabel.isUserInteractionEnabled = true
    
    fnoAccountTitleLabel.addGestureRecognizer(tap)
    fnoTradingTitleLabel.addGestureRecognizer(tap)
    
    ConfigManager.shared.enableFnOServer = true
    
    fnoAccountGestureView.isUserInteractionEnabled = true
    fnoAccountGestureView.addGestureRecognizer(tap)
    //Initial setup values  for deposit amount value is not set, take the default value
    
    
    
    if ConfigManager.shared.enableFnOServer == false {
      
      // show dummy trade
      accountTitleLabel.text = "ACCOUNT"
      self.fnoAccountView.isHidden = true
      
      self.accountTitleLabel.isHidden = false
      
      if (UserDefaults.standard.integer(forKey: CommonValues.depositAmountValue) != 0) {
        depositAmountVlue = UserDefaults.standard.integer(forKey: CommonValues.depositAmountValue)
      }else{
        depositAmountVlue = 10000
      }
    }
    else {
      
      
      self.fnoAccountView.isHidden = false
      
      self.accountTitleLabel.isHidden = true
      
      fnoAccountTitleLabel.text = "ACCOUNT"
      fnoTradingTitleLabel.text = "TRADING"
      
      if (UserDefaults.standard.integer(forKey: CommonValues.depositFnOAmountValue) != 0) {
        depositAmountVlue = UserDefaults.standard.integer(forKey: CommonValues.depositFnOAmountValue)
      }else{
        depositAmountVlue = 10000
      }
    }
    
    
    
    self.currentDocumentsDict[CommonValues.instrumentIdentifier] = ConfigManager.shared.exchange.identifierId
    
    self.perform(#selector(getFnOFeedInMinutes), with: false, afterDelay: 1.0)
    
    //    if ConfigManager.shared.enableFnOServer == true {//} && self.isMarketTimeClosed == false {
    //      // connect to FnO backend server
    //      // clear the locally saved data, temp.json
    ////      self.perform(#selector(downloadConfig), with: nil, afterDelay: 1.0)
    //
    //    }
    //    else {
    //      // clear the locally saved data, temp.json
    //      self.perform(#selector(removeExistingJsonFile), with: nil, afterDelay: 1.0)
    //    }
    
    self.perform(#selector(performGetBalanceAPI), with: false, afterDelay: 1.0)
    
  }
  
  @objc func handleTap(_ sender: UITapGestureRecognizer?) {
    
    
    
    
    // handling code
    if ConfigManager.shared.enableFnOServer == false {
      ConfigManager.shared.enableFnOServer = true
      
      
      
      self.fnoAccountView.isHidden = false
      //self.accountTitleLabel.isHidden = true
      
      
      fnoAccountTitleLabel.text = "ACCOUNT"
      fnoTradingTitleLabel.text = "TRADING"
      fnoAccountImageView.image = UIImage.init(named: "iconAccountSwitch")
      fnoTradingTitleLabel.textColor = UIColor.init(hex: 0xe8a61f)
      if (UserDefaults.standard.integer(forKey: CommonValues.depositFnOAmountValue) != 0) {
        self.currentDepositAmountValue = UserDefaults.standard.value(forKey: CommonValues.depositFnOAmountValue) as! Int
      }
      else {
        self.currentDepositAmountValue = 10000
      }
      self.depositAmountVlue = currentDepositAmountValue
      
    }
    else {
      if (UserDefaults.standard.integer(forKey: CommonValues.depositAmountValue) != 0) {
        self.currentDepositAmountValue = UserDefaults.standard.value(forKey: CommonValues.depositAmountValue) as! Int
      }else{
        self.currentDepositAmountValue = 10000
      }
      
      self.fnoAccountView.isHidden = false
      //self.accountTitleLabel.isHidden = false
      
      
      self.depositAmountVlue = currentDepositAmountValue
      
      ConfigManager.shared.enableFnOServer = false
      fnoAccountTitleLabel.text = "ACCOUNT"
      fnoTradingTitleLabel.text = "DEMO"
      fnoTradingTitleLabel.textColor = UIColor.init(hex: 0x2659b9)
      fnoAccountImageView.image = UIImage.init(named: "iconAccountSwitchDemo")
    }
    
    self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(self.currentDepositAmountValue.rupeesFormatt())"
  }
  
  
  func showRightActivityIndicatory () {
    UI {
      self.rightActivityView.isHidden = false
      self.view.bringSubview(toFront: self.rightActivityView)
    }
  }
  
  func hideRightActivityIndicatory () {
    UI {
      self.rightActivityView.isHidden = true
      self.view.sendSubview(toBack: self.rightActivityView)
    }
  }
  
  
  /// Add new data while scrolling to the left
  func addNewChartData() -> Void {
    
    
    if selectedSegmentIndex == 5 {
      let lastDownloadedDate = self.tenSecondsxAxisDateTimeArray[0]
      let startDate = lastDownloadedDate.yesterday.marketOpenTime
      let toDate = self.tenSecondsxAxisDateTimeArray.last?.unixTimestamp
      self.download60min(toDate: toDate!, fromDate: startDate.unixTimestamp ,isRedownload: false)
    }else if selectedSegmentIndex == 7 || selectedSegmentIndex == 6 {
      
      
      
      let lastDownloadedDate = self.secondxAxisDateTimeArray[0]
      let startDate = lastDownloadedDate.previousFiveMinutes
      //            let toDate = self.secondxAxisDateTimeArray.last?.unixTimestamp
      self.incrementalDownloadHistory(toDate: lastDownloadedDate.unixTimestamp, fromDate: startDate.unixTimestamp ,isRedownload: false)
    }
    
    return
    /*var set1: LineChartDataSet? = nil
     var values1 = [ChartDataEntry]()
     
     if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 0 {
     set1 = (chartView.data?.dataSets[2] as? LineChartDataSet)!
     values1 = (set1?.values)!
     for i in 0..<500 {
     var lower : UInt32 = 9950
     var upper : UInt32 = 9960
     
     let value = (set1?.entryCount)! % 100
     if value >= 1 && value < 30 {
     lower = 9950//UInt32(value)
     upper = 9955//UInt32(value) + 5
     }else if value >= 32 && value < 60 {
     lower = 9955//UInt32(value)
     upper = 9960//UInt32(value) + 5
     }else if value >= 62 && value < 99 {
     lower = 9940//lower - UInt32(value)
     upper = 9950//upper - UInt32(value) + 18
     }
     
     
     let randomNumber = arc4random_uniform(upper - lower) + lower
     var val = Double(randomNumber)
     values1.insert(ChartDataEntry(x: 0, y: 9500, data: UIImage(named: "icon")), at: 0)
     }
     
     }
     set1?.values = values1
     
     chartView.data?.notifyDataChanged()
     chartView.notifyDataSetChanged()
     self.updateYaxisValue()*/
  }
  
  
  /// Update date and time in bottom bar
  func dateLblTimeTick() -> Void {
    self.dateLbl.text = self.stringFrom(given: Date(), formatType: "dd MMM HH:mm:ss").uppercased()
  }
  
  
  /// Show or hide the animated white point and data feed live point
  ///
  /// - Parameter isHide: true - Hidden , false - Visible
  func showOrHidePointValues(isHide : Bool) -> Void {
    
    self.view.bringSubview(toFront: self.dataPointView)
    self.dataPointView.isHidden = isHide
    self.outerView.isHidden = isHide
    self.pointView.isHidden = isHide
    //        self.redFlagView.isHidden = isHide
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    AppUtility.lockOrientation(.landscape)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    dataFeedTimer.invalidate()
    AppUtility.lockOrientation(.landscape)
  }
  
  
  //MARK: Duration segment
  //*************************** Add buttons *****************************////
  
  /// Create a duration segment buttons
  func loadSegmentControl() -> Void{
    
    var xVal = 40
    var buttonWidth = 54
    
    if DeviceType.IS_IPHONE_6P{
      buttonWidth = 40 //54
    }else if DeviceType.IS_IPHONE_6{
      buttonWidth = 48
    }
    
    let buttonHeight = buttonWidth
    
    buttonsScrollView.frame =  CGRect(x: 0, y:self.view.frame.size.height - CGFloat(buttonHeight+20), width: self.view.frame.size.width-230, height: CGFloat(buttonHeight+20))
    buttonsScrollView.backgroundColor = UIColor.clear //UIColor.init(hex: 0x272e42) //UIColor(colorLiteralRed: 38.0/255.0, green: 46.0/255.0, blue: 65.0/255.0, alpha: 1.0)
    
    
    let count = segmentItems.count
    
    
    for i in 0..<count {
      let button = UIButton(type: .custom)
      button.frame = CGRect(x: xVal, y: 10, width: buttonWidth, height: buttonHeight)
      button.setTitle(segmentItems[i], for: .normal)
      button.tag = i+1
      button.titleLabel?.font = AppFont.getBold(pixels: 22)//UIFont.init(name: "Roboto-Regular", size: 10.0)
      button.setTitleColor(UIColor.init(hex: 0x4e5c78), for: .normal)
      button.setTitleColor(UIColor.white, for: .selected)
      button.backgroundColor = UIColor.clear
      button.setBackgroundColor(color: UIColor.clear, forState: .selected)
      button.setBackgroundImage(UIImage.init(named: "segmentSelected"), for: .selected)
      button.addTarget(self, action: #selector(segmentButtonAction), for: .touchUpInside)
      buttonsScrollView.addSubview(button)
      
      if i+1 != count {
        let separtorView = UIView()
        separtorView.frame = CGRect(x: xVal + Int(button.frame.size.width)  , y: 8, width: 1, height: 15)
        //                separtorView.image = UIImage.init(named: "buttonSepartor")
        separtorView.tag = i + 10
        separtorView.backgroundColor = UIColor.clear //UIColor.init(patternImage: UIImage.init(named: "buttonSepartor2")!) //UIColor.init(hex: 0x4e5c78) //UIColor(colorLiteralRed: 67.0/255.0, green: 81.0/255.0, blue: 103.0/255.0, alpha: 1.0)
        // buttonsScrollView.addSubview(separtorView)
      }
      
      buttonsScrollView.bringSubview(toFront: button)
      
      xVal += buttonWidth + 18
      
      if i == 6 {
        self.segmentButtonAction(sender: button)
      }
    }
    
    buttonsScrollView.contentSize = CGSize(width: 40*segmentItems.count, height: buttonHeight)
    self.view.addSubview(buttonsScrollView)
    
    
    dateLbl.backgroundColor = UIColor.clear //UIColor.init(hex: 0x272e42)
    dateLbl.textColor = UIColor.init(hex: 0x4e5c78)
    dateLbl.font = AppFont.getRegular(pixels: 8)//UIFont.init(name: "Roboto-Regular", size: 10.0)
    
    self.dateLblHeightConstraint.constant = CGFloat(buttonHeight+20)
    
    self.view.bringSubview(toFront: dateLbl)
    self.view.bringSubview(toFront: self.switchBtn)
  }
  
  
  /// Hide all chartview datasets
  func hideAllDataSets() -> Void {
    self.chartView.data?.dataSets[0].visible = false
    self.chartView.data?.dataSets[1].visible = false
    self.chartView.data?.dataSets[2].visible = false
    self.chartView.data?.dataSets[3].visible = false
    self.chartView.data?.dataSets[4].visible = false
    self.chartView.data?.dataSets[5].visible = false
  }
  
  
  /// Duration button action, like 1D , 5m action
  ///
  /// - Parameter sender: selected button action 1D, 3D,etc...
  func segmentButtonAction(sender : UIButton) -> Void {
    
    
    var selectedBtn = sender
    
    for subview in buttonsScrollView.subviews {
      if let button = subview as? UIButton {
        button.isSelected = false
        button.backgroundColor = UIColor.clear
        if button.tag == sender.tag{
          selectedBtn = button
        }
      }else if let sepView = subview as? UIView {
        sepView.backgroundColor = UIColor.init(patternImage: UIImage.init(named: "buttonSepartor2")!) //UIColor.init(hex: 0x4e5c78)
        if  sepView.tag == (selectedBtn.tag+10)-1 || sepView.tag == (selectedBtn.tag+10)-2
        {
          sepView.backgroundColor = UIColor.clear//UIColor.init(hex: 0x4e5c78)
        }
      }
      else{
        print("error")
      }
    }
    //        }
    selectedBtn.isSelected = !selectedBtn.isSelected
    if selectedBtn.isSelected {
      selectedBtn.backgroundColor = UIColor.clear //UIColor.init(hex: 0x384764) // UIColor(colorLiteralRed: 52.0/255.0, green: 62.0/255.0, blue: 86.0/255.0, alpha: 1.0)
      sender.frame.size.width = sender.frame.size.width
    }
    
    self.chartView.xAxis.labelCount = 5
    timeIntervalForFlag = 0
    //for 1 y data starts from 1.0
    self.timelimit = 0
    var key : String = ""
    if currentDocumentsDict["PRODUCT"] != nil {
      key = currentDocumentsDict["PRODUCT"] as! String
    }else{
      key = "NIFTY"
    }
    self.hideAllDataSets()
    if self.isChartDataAvaible == false{
      self.noChartDataAvailable()
    }else{
      switch selectedBtn.tag {
      case 1:
        selectedSegmentIndex = 1
        timeDuration = 3600
        self.historicalYvaluesArray = self.hoursHistoricalYvaluesArray
        self.xAxisDateArray = self.hoursXAxisDateArray
        
        
        self.chartView.xAxis.axisMaximum = Double(self.hoursHistoricalYvaluesArray.count)
        self.chartView.setVisibleXRangeMaximum(6.25*28)
        self.chartView.setVisibleXRangeMinimum(6.25*28)
        self.chartView.setVisibleXRange(minXRange: 5, maxXRange: 375*28)
        if (chartView.data?.dataSets.count)! > 2 {
          self.chartView.data?.dataSets[2].visible = true
        }
        if self.tradePoints.has(key: "\(key)_hour") {
          UserDefaults.standard.setValue(Int(self.tradePoints["\(key)_hour"]!), forKey: CommonValues.tradeNowMarkerPoint)
        }else{
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeNowMarkerPoint)
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
          
        }
        
        break
      case 2:
        selectedSegmentIndex = 2
        timeDuration = 3600
        self.historicalYvaluesArray = self.tenMinutesHistoricalYvaluesArray
        self.xAxisDateArray = self.tenMinutesXAxisDateArray
        
        self.chartView.xAxis.axisMaximum = Double(self.tenMinutesHistoricalYvaluesArray.count)
        self.chartView.setVisibleXRangeMaximum(37*5)
        self.chartView.setVisibleXRangeMinimum(37*5)
        self.chartView.setVisibleXRange(minXRange: 5, maxXRange: CommonValues.dataPointsPerMin * 375*28)
        if (chartView.data?.dataSets.count)! > 4 {
          self.chartView.data?.dataSets[4].visible = true
        }
        if self.tradePoints.has(key: "\(key)_tenMin") {
          UserDefaults.standard.setValue(Int(self.tradePoints["\(key)_tenMin"]!), forKey: CommonValues.tradeNowMarkerPoint)
        }
        else{
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeNowMarkerPoint)
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
          
        }
        
        break
      case 3:
        selectedSegmentIndex = 3
        timeDuration = 3600
        self.historicalYvaluesArray = self.minuteHistoricalYvaluesArray
        self.xAxisDateArray = self.minuteXAxisDateArray
        self.chartView.xAxis.axisMaximum = Double(self.minuteHistoricalYvaluesArray.count)
        self.chartView.setVisibleXRangeMaximum(375*2)
        self.chartView.setVisibleXRangeMinimum(375*2)
        self.chartView.setVisibleXRange(minXRange: 5, maxXRange: CommonValues.dataPointsPerMin * 375*28)
        if (chartView.data?.dataSets.count)! > 1{
          self.chartView.data?.dataSets[1].visible = true
        }
        if self.tradePoints.has(key: "\(key)_min") {
          UserDefaults.standard.setValue(Int(self.tradePoints["\(key)_min"]!), forKey: CommonValues.tradeNowMarkerPoint)
        }
        else{
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeNowMarkerPoint)
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
          
        }
        
        
        break
      case 4:
        selectedSegmentIndex = 4
        if historicalYvaluesArray.count > 0{ //For 1D
          self.historicalYvaluesArray = self.minuteHistoricalYvaluesArray
          self.xAxisDateArray = self.minuteXAxisDateArray
          self.chartView.xAxis.axisMaximum = Double(self.minuteHistoricalYvaluesArray.count)
          
          self.chartView.setVisibleXRangeMaximum(375)
          self.chartView.setVisibleXRangeMinimum(375)
          self.chartView.xAxis.labelCount = 6
          
          self.chartView.setVisibleXRange(minXRange: 5, maxXRange: CommonValues.dataPointsPerMin * 375*28)
          timeDuration = 60
          if (chartView.data?.dataSets.count)! > 2 {
            self.chartView.data?.dataSets[1].visible = true
          }
        }
        if self.tradePoints.has(key: "\(key)_min") {
          UserDefaults.standard.setValue(Int(self.tradePoints["\(key)_min"]!), forKey: CommonValues.tradeNowMarkerPoint)
        }
        else{
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeNowMarkerPoint)
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
        }
        
        break
      case 5:
        //60m view
        selectedSegmentIndex = 5
        timeIntervalForFlag = 60
        timeDuration = 60
        self.historicalYvaluesArray = self.tenSecondseHistoricalYvaluesArray
        self.xAxisDateArray = self.tenSecondsXAxisDateArray
        self.chartView.xAxis.axisMaximum = Double(self.tenSecondseHistoricalYvaluesArray.count)
        self.chartView.setVisibleXRangeMaximum(CommonValues.dataPointsPerMin * 6)
        self.chartView.setVisibleXRangeMinimum(CommonValues.dataPointsPerMin * 6)
        //            self.chartView.xAxis.labelCount = 3
        self.chartView.xAxis.labelCount = 6
        
        self.chartView.setVisibleXRange(minXRange: 5, maxXRange: CommonValues.dataPointsPerMin * 375*28)
        if (chartView.data?.dataSets.count)! > 3 {
          self.chartView.data?.dataSets[3].visible = true
        }
        if self.tradePoints.has(key: "\(key)_tenSec") {
          UserDefaults.standard.setValue(Int(self.tradePoints["\(key)_tenSec"]!), forKey: CommonValues.tradeNowMarkerPoint)
        }
        else{
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeNowMarkerPoint)
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
          
        }
        
        
        break
      case 6:
        selectedSegmentIndex = 6
        timeDuration = 1
        timeIntervalForFlag = 15
        self.historicalYvaluesArray = self.fiveSecondseHistoricalYvaluesArray
        self.xAxisDateArray = self.fiveSecondsXAxisDateArray
        self.chartView.xAxis.axisMaximum = Double(self.fiveSecondseHistoricalYvaluesArray.count)
        
        self.chartView.setVisibleXRangeMaximum(30*15)
        self.chartView.setVisibleXRangeMinimum(30*15)
        self.chartView.xAxis.labelCount = 6
        
        
        self.chartView.setVisibleXRange(minXRange: 5, maxXRange: CommonValues.dataPointsPerMin * 375*28)
        if (chartView.data?.dataSets.count)! > 5 {
          self.chartView.data?.dataSets[5].visible = true
        }
        if self.tradePoints.has(key: "\(key)_fiveSec") {
          UserDefaults.standard.setValue(Int(self.tradePoints["\(key)_fiveSec"]!), forKey: CommonValues.tradeNowMarkerPoint)
        }
        else{
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeNowMarkerPoint)
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
          
        }
        
        break
      case 7:
        
        self.chartView.xAxis.labelCount = 6
        selectedSegmentIndex = 7
        timeIntervalForFlag = 5
        //if historicalYvaluesArray.count > 0{ //For 1D
        timeDuration  = 1
        timeIntervalForFlag = 5
        self.chartView.xAxis.labelCount = 6
        self.historicalYvaluesArray = self.secondHistoricalYvaluesArray
        self.xAxisDateArray = self.secondXAxisDateArray
        self.chartView.xAxis.axisMaximum = Double(self.secondHistoricalYvaluesArray.count)
        
        self.chartView.setVisibleXRangeMaximum(CommonValues.dataPointsPerMin * 5)
        self.chartView.setVisibleXRangeMinimum(CommonValues.dataPointsPerMin * 5)
        
        self.chartView.setVisibleXRange(minXRange: 5, maxXRange: 2000)
        if chartView.data != nil{
          if (chartView.data?.dataSets.count)! > 2 {
            self.chartView.data?.dataSets[0].visible = true
          }
        }
        if self.tradePoints.has(key: "\(key)_sec") {
          UserDefaults.standard.setValue(Int(self.tradePoints["\(key)_sec"]!), forKey: CommonValues.tradeNowMarkerPoint)
          //  }
        }
        else{
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeNowMarkerPoint)
          UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
        }
        
        break
      default:
        break
      }
      
      if self.isMarketTimeClosed {
        if self.previousSelectedSegmentIndex != self.selectedSegmentIndex {
          self.relaunchButtonAction = true
        }
      }
      
      UserDefaults.standard.set(selectedSegmentIndex, forKey: CommonValues.selectedSegmentIndex)
      chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:self.xAxisDateArray)
      UI {
        self.setFlagView()
        self.chartView.notifyDataSetChanged()
        self.isResetTrade = true
        // self.chartView.setNeedsDisplay()
        switch selectedBtn.tag{
        case 1 :
          //                    self.chartView.moveViewToX(33) //138
          
          if self.hoursHistoricalYvaluesArray.count  < 138 {
            self.chartView.moveViewToX(Double(self.hoursHistoricalYvaluesArray.count)) //138
          }else{
            self.chartView.moveViewToX(Double(self.hoursHistoricalYvaluesArray.count-DataPointTrailigPoint.tralingPoint(138))) //138
          }
          // Deve
          break
        case 2 :
          if self.tenMinutesHistoricalYvaluesArray.count < 180{
            self.chartView.moveViewToX(Double(self.tenMinutesHistoricalYvaluesArray.count-DataPointTrailigPoint.tralingPoint(130))) //26  -- //32
          }else{
            self.chartView.moveViewToX(Double(self.tenMinutesHistoricalYvaluesArray.count-DataPointTrailigPoint.tralingPoint(180))) //26  -- //32
          }
          
          break
        case 3 :
          self.chartView.moveViewToX(Double(self.minuteHistoricalYvaluesArray.count-DataPointTrailigPoint.tralingPoint(750))) //600
          
          break
        case 4 :
          
          var index = self.minuteXAxisDateArray.index(of: "\n9:15")
          if index == nil {
            index = self.minuteXAxisDateArray.index(of: "\n11:45")
          }
          
          if index != nil {
            if self.minuteXAxisDateArray.count - index! >= 300 {
              self.chartView.moveViewToX(Double(self.minuteHistoricalYvaluesArray.count-DataPointTrailigPoint.tralingPoint(375))) //300
            }else{
              self.chartView.moveViewToX(Double(index!))
            }
          }else{
            self.chartView.moveViewToX(Double(self.minuteHistoricalYvaluesArray.count-DataPointTrailigPoint.tralingPoint(375))) //300
          }
          
          
          
          break
        case 5 :
          
          if self.tenSecondseHistoricalYvaluesArray.count < 180 {
            self.chartView.moveViewToX(Double(self.tenSecondseHistoricalYvaluesArray.count-DataPointTrailigPoint.tralingPoint(100))) //48
            
          }else{
            self.chartView.moveViewToX(Double(self.tenSecondseHistoricalYvaluesArray.count-DataPointTrailigPoint.tralingPoint(360))) //48
            
          }
          break
        case 6 :
          self.chartView.moveViewToX(Double(self.fiveSecondseHistoricalYvaluesArray.count-DataPointTrailigPoint.tralingPoint(450)))
          
          break
        case 7 :
          self.chartView.moveViewToX(Double(self.secondHistoricalYvaluesArray.count-DataPointTrailigPoint.tralingPoint(300)))
          break
        default:
          break
          
        }
        self.perform(#selector(self.setFlagView), with: nil, afterDelay: 1.0)
        self.updateYaxisValue()
        if self.isMarketTimeClosed {
          if self.relaunchButtonAction == true {
            let button = UIButton.init()
            button.tag = self.selectedSegmentIndex
            self.segmentButtonAction(sender: button)
            self.relaunchButtonAction = false
            
          }
        }
      }
      
      previousSelectedSegmentIndex = selectedSegmentIndex
    }
    
    
    UserDefaults.standard.setValue(Int(self.selectedSegmentIndex), forKey: "segmentIndex")
    UserDefaults.standard.synchronize()
    
  }
  
  //*************************** Buttons end *****************************////
  
  /// Zoomin and out the chart view and change the duration segment
  ///
  /// - Parameter notification: Post notification from the chart view
  func updateSegmentButtonForScale(notification: Notification){
    if self.isChartDataAvaible == false {
      return
    }
    self.chartView.xAxis.labelCount = 5
    timeIntervalForFlag = 0
    self.timelimit = 0
    self.timeDuration = 90
    var btn = UIButton()
    let visibleRange = UserDefaults.standard.value(forKey: "currentScaleValue") as! CGFloat
    switch selectedSegmentIndex {
    case 7:// 5 minute
      if  visibleRange >= 0 && visibleRange <= 600{
        btn = self.view.viewWithTag(7) as! UIButton
        
      }else{
        btn = self.view.viewWithTag(6) as! UIButton
        segmentButtonAction(sender: btn)
      }
      break
      
    case 6:// 30 minute
      if  visibleRange >= 5 && visibleRange <= 600{
        btn = self.view.viewWithTag(7) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 600 && visibleRange <= 900{
        btn = self.view.viewWithTag(6) as! UIButton
        segmentButtonAction(sender: btn)
      }else{
        btn = self.view.viewWithTag(5) as! UIButton
        segmentButtonAction(sender: btn)
        
      }
      break
      
    case 5:// 60 minute
      if  visibleRange >= 5 && visibleRange <= 45{
        btn = self.view.viewWithTag(7) as! UIButton
        segmentButtonAction(sender: btn)
      }else if visibleRange >= 46 && visibleRange <= 120{
        btn = self.view.viewWithTag(6) as! UIButton
        segmentButtonAction(sender: btn)
      }else if visibleRange >= 121 && visibleRange <= 420{
        btn = self.view.viewWithTag(5) as! UIButton
      }else{
        btn = self.view.viewWithTag(4) as! UIButton
        segmentButtonAction(sender: btn)
        
      }
      break
      
    case 4:// 1D
      if  visibleRange >= 5 && visibleRange <= 15{
        btn = self.view.viewWithTag(7) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 16 && visibleRange <= 45{
        btn = self.view.viewWithTag(6) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 45 && visibleRange <= 300{
        btn = self.view.viewWithTag(5) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 301 && visibleRange <= 475{
        btn = self.view.viewWithTag(4) as! UIButton
      }else{
        btn = self.view.viewWithTag(3) as! UIButton
        segmentButtonAction(sender: btn)
      }
      break
      
    case 3:// 3D
      if  visibleRange >= 5 && visibleRange <= 10{
        btn = self.view.viewWithTag(7) as! UIButton
        segmentButtonAction(sender: btn)
        
        
      }else if visibleRange >= 10 && visibleRange <= 45{
        btn = self.view.viewWithTag(6) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 45 && visibleRange <= 300{ //75
        btn = self.view.viewWithTag(5) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 301 && visibleRange <= 475{
        btn = self.view.viewWithTag(4) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 475 && visibleRange <= 900{
        btn = self.view.viewWithTag(3) as! UIButton
        
      }else{
        btn = self.view.viewWithTag(2) as! UIButton
        segmentButtonAction(sender: btn)
      }
      break
      
    case 2:// 5D
      if  visibleRange >= 6 && visibleRange <= 10{
        btn = self.view.viewWithTag(4) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 10 && visibleRange <= 26{
        btn = self.view.viewWithTag(3) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 26 && visibleRange <= 100{
        btn = self.view.viewWithTag(2) as! UIButton
      }else{
        btn = self.view.viewWithTag(1) as! UIButton
        segmentButtonAction(sender: btn)
        
      }
      break
      
      
    case 1:// 1M
      if  visibleRange >= 6 && visibleRange <= 10{
        btn = self.view.viewWithTag(4) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 10 && visibleRange <= 26{
        btn = self.view.viewWithTag(3) as! UIButton
        segmentButtonAction(sender: btn)
        
      }else if visibleRange >= 26 && visibleRange <= 190{
        btn = self.view.viewWithTag(2) as! UIButton
      }else{
        btn = self.view.viewWithTag(1) as! UIButton
        segmentButtonAction(sender: btn)
        
      }
      break
      
      
    default:
      break
    }
    
    
    
    for subview in buttonsScrollView.subviews {
      if let button = subview as? UIButton {
        button.isSelected = false
        button.backgroundColor = UIColor.clear
        if button.tag == btn.tag{
          btn = button
        }
      }else if let sepView = subview as? UIView {
        sepView.backgroundColor = UIColor.init(patternImage: UIImage.init(named: "buttonSepartor2")!) //UIColor.init(hex: 0x4e5c78)
        if  sepView.tag == (btn.tag+10)-1 || sepView.tag == (btn.tag+10)-2
        {
          sepView.backgroundColor = UIColor.clear//UIColor.init(hex: 0x4e5c78)
        }
      }
      else{
        print("error")
      }
    }
    btn.isSelected = !btn.isSelected
    if btn.isSelected {
      btn.backgroundColor = UIColor.clear //UIColor.init(hex: 0x384764) // UIColor(colorLiteralRed: 52.0/255.0, green: 62.0/255.0, blue: 86.0/255.0, alpha: 1.0)
      btn.frame.size.width = btn.frame.size.width
    }
    self.setFlagView()
  }
  
  func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat) {
    //            chartView.scale(by: CGPoint(x: scaleX, y: scaleY))
    
  }
  
  //MARK: Chart
  //*************************** ChartView *****************************////
  
  /// Initial setup of chartview
  func initialChartViewUpdate(){
    chartView.legend.enabled = false
    self.historicalYvaluesArray.removeAll()
    self.xAxisDateArray.removeAll()
    chartView.delegate = self
    chartView.chartDescription?.enabled = false
    chartView.dragEnabled = true
    chartView.doubleTapToZoomEnabled = false
    chartView.scaleXEnabled =   true   // For zoom ... True means enabled , false means disabled  -----******------
    chartView.pinchZoomEnabled =  true    // For zoom ... True means enabled , false means disabled  -----******------
    chartView.drawGridBackgroundEnabled = false
    chartView.scaleYEnabled = false
    //        chartView.animate(xAxisDuration: 2)
    chartView.setDragOffsetX(150)
    chartView.minOffset = 0
    //        chartView.extraBottomOffset = -50
    
    //_chartView.xAxis._axisMaximum = 250;
    
    // x-axis limit line
    let llXAxis = ChartLimitLine(limit: 10.0, label: "Index 10")
    llXAxis.lineWidth = 0.0
    llXAxis.lineDashLengths = [(10.0), (10.0), (0.0)]
    llXAxis.labelPosition = .rightBottom
    llXAxis.valueFont = UIFont.systemFont(ofSize: CGFloat(10.0))
    chartView.xAxis.labelTextColor = UIColor.lightGray
    chartView.xAxis.labelPosition = .bottomInside
    chartView.xAxis.axisLineColor = UIColor.clear
    chartView.xAxis.axisLineWidth = 0.0
    //[_chartView.xAxis addLimitLine:llXAxis];
    chartView.xAxis.gridLineDashLengths = [] //[5.0, 3.0]
    chartView.xAxis.gridLineDashPhase = 0.0
    chartView.xAxis.labelFont = AppFont.getRegular(pixels: 23)//UIFont.systemFont(ofSize: 7)
    
    xAxisDateArray.append(Date().dateStringFromDate(toFormat: CommonValues.xAxisDateFormat))
    //        xAxisDateArray.append(Date().dateStringFromDate(toFormat: "HH:mm:ss"))
    chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:xAxisDateArray)
    chartView.xAxis.labelCount = 5
    chartView.xAxis.granularity = 1.0
    chartView.xAxis.avoidFirstLastClippingEnabled = false
    chartView.xAxis.forceLabelsEnabled = true
    chartView.xAxis.drawGridLinesEnabled = false
    
    // y-axis limit line
    let leftAxis: YAxis? = chartView.leftAxis
    leftAxis?.xOffset = self.view.frame.size.width-280
    leftAxis?.labelCount = 5
    leftAxis?.removeAllLimitLines()
    //        leftAxis?.spaceBottom
    leftAxis?.axisMaximum = 200
    leftAxis?.axisMinimum = 0.0
    leftAxis?.axisLineColor = UIColor.clear
    leftAxis?.axisLineWidth = 0.0
    leftAxis?.labelTextColor = UIColor.lightGray// need to change
    leftAxis?.labelFont = AppFont.getRegular(pixels: 23)
    leftAxis?.labelPosition = .insideChart
    leftAxis?.gridLineDashLengths = [] //[5.0, 3.0]
    leftAxis?.drawGridLinesEnabled = false
    leftAxis?.drawAxisLineEnabled = false
    leftAxis?.gridColor = NSUIColor.clear //NSUIColor.gray.withAlphaComponent(0.3) //UIColor.lightGray
    leftAxis?.drawZeroLineEnabled = false
    leftAxis?.drawLimitLinesBehindDataEnabled = false
    chartView.rightAxis.enabled = false
    chartView.legend.form = .line
    chartView.animate(xAxisDuration: 2.5)
    chartView.noDataTextColor = UIColor.white
    //        chartView.setViewPortOffsets(left: 0, top: 0, right: 0, bottom: 0)
    
    var values1 = [ChartDataEntry]()
    values1.append(ChartDataEntry(x: Double(0), y: 0, data: UIImage(named: "icon")))
    let set1 = self.updateChartView(values: values1)
    let set2 = self.updateChartView(values: values1)
    let set3 = self.updateChartView(values: values1)
    let set4 = self.updateChartView(values: values1)
    let set5 = self.updateChartView(values: values1)
    let set6 = self.updateChartView(values: values1)
    let data = LineChartData(dataSets: [set1,set2,set3,set4,set5,set6])
    chartView.data = data
    
    //chartView.data?.notifyDataChanged()
    //chartView.notifyDataSetChanged()
    
    chartView.xAxis.labelTextColor = UIColor.clear
    chartView.leftAxis.labelTextColor = UIColor.clear
  }
  
  /// Get visible chartview - get chart view lowest and highest axis values
  func updateYaxisValue() -> Void{
    
    let visibleCount = (chartView.highestVisibleX > 0.0 ? Int(chartView.highestVisibleX) : 0) - (chartView.lowestVisibleX > 0.0 ? Int(chartView.lowestVisibleX) : 0)
    var tempArr  = [Double]()
    if visibleCount > 0{
      for i in 0...visibleCount {
        let index = Int(chartView.lowestVisibleX)+i
        if index < historicalYvaluesArray.count && index >= 0{
          tempArr.append(historicalYvaluesArray[index])
        }
      }
    }
    
    if tempArr.count > 0 {
      UI {
        
        self.chartView.xAxis.labelTextColor = UIColor.lightGray
        self.chartView.leftAxis.labelTextColor = UIColor.lightGray
        
        self.chartView.leftAxis.axisMinimum = tempArr.min()! - 15
        self.chartView.leftAxis.axisMaximum = tempArr.max()! + 15
        if self.upDirBtn.isSelected || self.downDirBtn.isSelected {
          self.updateLimitLines()
        }else{
          // true means hide red and green line if data point is hidden
          self.redLineView.isHidden = true
          self.greenLineView.isHidden = true
          self.redLineLabelValue.isHidden = true
          self.greenLineLabelValue.isHidden = true
          self.orangeLineView.isHidden = true
          self.chartView.layoutIfNeeded()
        }
      }
    }
    
    
  }
  
  /// Update Red & Green lines based on direction
  func updateLimitLines() -> Void {
    
    if self.upDirBtn.isSelected || self.downDirBtn.isSelected {
      let actualTopDatapoint : CGPoint = CGPointFromString(UserDefaults.standard.value(forKey: "ActualTopLinepoint") as! String)
      let actualBottomDatapoint : CGPoint = CGPointFromString(UserDefaults.standard.value(forKey: "ActualBottomLinepoint") as! String)
      let percent = ((self.chartView.leftAxis.axisMaximum - self.chartView.leftAxis.axisMinimum)/100) * 10
      //print("percent \(percent)")
      if self.dataPointView.isHidden {
        var add = 0
        let max = Double(actualTopDatapoint.y) + percent
        let min = Double(actualBottomDatapoint.y) - percent
        add = Int(max - min) / 5
        // print("add datapoint view hidden \(add)")
        UI {
          if Double(actualBottomDatapoint.y) - percent < self.chartView.leftAxis.axisMinimum {
            self.chartView.leftAxis.axisMinimum = Double(actualBottomDatapoint.y - CGFloat(add))
            
          }
          if Double(actualTopDatapoint.y) + percent > self.chartView.leftAxis.axisMaximum{
            self.chartView.leftAxis.axisMaximum = Double(actualTopDatapoint.y + CGFloat(add))
          }
          
          
          // true means hide red and green line if data point is hidden
          self.redLineView.isHidden = self.yellowLineView.isHidden
          self.greenLineView.isHidden = self.yellowLineView.isHidden
          self.redLineLabelValue.isHidden = self.yellowLineView.isHidden
          self.greenLineLabelValue.isHidden = self.yellowLineView.isHidden
          self.orangeLineView.isHidden = self.yellowLineView.isHidden
        }
      }
      else{
        
        if Double(actualBottomDatapoint.y) - percent < self.chartView.leftAxis.axisMinimum || Double(actualTopDatapoint.y) + percent > self.chartView.leftAxis.axisMaximum {
          
          var add = 0
          let max = Double(actualTopDatapoint.y) + percent
          let min = Double(actualBottomDatapoint.y) - percent
          add = Int(max - min) / 5
          //   print("add datapoint view not-hidden \(add)")
          
          if Double(actualBottomDatapoint.y) - percent < self.chartView.leftAxis.axisMinimum {
            self.chartView.leftAxis.axisMinimum = Double(actualBottomDatapoint.y - CGFloat(add))
            self.chartView.leftAxis.axisMaximum = self.chartView.leftAxis.axisMaximum + Double(add)
            
          }
          if Double(actualTopDatapoint.y) + percent > self.chartView.leftAxis.axisMaximum{
            self.chartView.leftAxis.axisMinimum = self.chartView.leftAxis.axisMinimum - Double(add)
            self.chartView.leftAxis.axisMaximum = Double(actualTopDatapoint.y + CGFloat(add))
            
          }
          self.showRedLine()
        }else{
          self.showRedLine()
        }
      }
      self.isResetTrade = false
    }
    else if currentAmountValue <= 0 {
      self.redLineView.isHidden = true
      self.greenLineView.isHidden = true
      self.redLineLabelValue.isHidden = true
      self.greenLineLabelValue.isHidden = true
      self.orangeLineView.isHidden = true
    }
    UI {
      self.setFlagView()
      self.showRedLine()
      self.chartView.data?.notifyDataChanged()
      self.chartView.notifyDataSetChanged()
    }
    
  }
  
  /// Show top (Red) and bottom (green) lines based on data point view
  func showRedLine() -> Void {
    UI {
      
      if self.currentAmountValue > 0 {
        self.redLineView.isHidden = false
        self.greenLineView.isHidden = false
        self.redLineLabelValue.isHidden = false
        self.greenLineLabelValue.isHidden = false
        self.orangeLineView.isHidden = false
        self.view.bringSubview(toFront: self.greenLineView)
        self.view.bringSubview(toFront: self.greenLineLabelValue)
        self.view.bringSubview(toFront: self.redLineLabelValue)
        self.view.bringSubview(toFront: self.redLineView)
        self.view.bringSubview(toFront: self.orangeLineView)
      }
    }
  }
  
  
  /// Hide the loading activity
  func hideActivityIndicator(){
    print("control in  hiding activity indicator")
    self.activityView.isHidden = true
  }
  
  /// White dot animation with outer view
  func animatePoint() {
    
    if dataFeedTimer.isValid {
      
      UIView.animate(withDuration: 1.0, animations: {
          self.outerView.alpha = 0.0
          self.outerView.layoutIfNeeded()
      }) { (true) in
        UIView.animate(withDuration: 1.0, animations: {
            self.outerView.alpha = 0.8
        })
      }
    }
  }
  
  
  
  /// For Creating the chart data set
  ///
  /// - Parameter values: Chartdata entries [x and y axis]
  /// - Returns: Line chart data sets
  func updateChartView(values : [ChartDataEntry]) -> LineChartDataSet {
    let set = LineChartDataSet()
    set.values = values
    set.highlightLineDashLengths = [] //[5.0, 2.5]
    set.setColor(UIColor.init(hex: 0x9ab1d1))
    set.setCircleColor(UIColor.white)
    set.lineWidth = 1.0
    set.circleRadius = 2.0
    set.drawCirclesEnabled = false
    set.drawCircleHoleEnabled = false
    set.valueFont = UIFont.systemFont(ofSize: CGFloat(9.0))
    set.formLineDashLengths = [] //[5.0, 2.5]
    set.formLineWidth = 0.0
    set.formSize = 15.0
    set.mode = .cubicBezier
    
    
    
    set.drawValuesEnabled = !(set.isDrawValuesEnabled)
    
    //let gradientColors = [ChartColorTemplates.colorFromString("#9ab1cf").cgColor,ChartColorTemplates.colorFromString("#384865").cgColor]
    let gradientColors = [ChartColorTemplates.colorFromString("#3091d6").cgColor,ChartColorTemplates.colorFromString("#a4dafe").cgColor,ChartColorTemplates.colorFromString("#e2bfdf").cgColor,ChartColorTemplates.colorFromString("#f85487").cgColor]
    let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)
    set.fillAlpha = 0.3
    set.fill = Fill(linearGradient: gradient!, angle: 90.0)
    set.drawFilledEnabled = false
    return set
  }
  
  /// create all data sets and render the chart data
  func reloadChartViewUpdate()->Void
  {
    print("##### Chart view drawing stared ##### --- \(Date())")
    
    var values1 = [ChartDataEntry]()
    var values2 = [ChartDataEntry]()
    var values3 = [ChartDataEntry]()
    var values4 = [ChartDataEntry]()
    var values5 = [ChartDataEntry]()
    var values6 = [ChartDataEntry]()
    
    //print("secondXAxisDateArray \(secondXAxisDateArray.count)")
    
    //#-- For seconds dataset
    for i in 0..<self.secondHistoricalYvaluesArray.count {
      if i < secondXAxisDateArray.count {
        values1.append(ChartDataEntry(x: Double(i), y: secondHistoricalYvaluesArray[i], data: UIImage(named: "icon")))
      }
    }
    print("set value 1 \(values1.count)")
    
    for l in 0..<self.tenSecondseHistoricalYvaluesArray.count {
      if l < tenSecondsXAxisDateArray.count {
        values4.append(ChartDataEntry(x: Double(l), y: tenSecondseHistoricalYvaluesArray[l], data: UIImage(named: "icon")))
      }
    }
    
    print("set value 4 \(values4.count)")
    
    for n in 0..<self.fiveSecondseHistoricalYvaluesArray.count {
      if n < fiveSecondsXAxisDateArray.count {
        values6.append(ChartDataEntry(x: Double(n), y: fiveSecondseHistoricalYvaluesArray[n], data: UIImage(named: "icon")))
      }
    }
    print("set value 6 \(values6.count)")
    
    //#-- For minutes data set
    for j in 0..<self.minuteHistoricalYvaluesArray.count {
      if j < minuteXAxisDateArray.count {
        values2.append(ChartDataEntry(x: Double(j), y: minuteHistoricalYvaluesArray[j], data: UIImage(named: "icon")))
      }
    }
    print("set value 2 \(values2.count)")
    
    for m in 0..<self.tenMinutesHistoricalYvaluesArray.count {
      if m < tenMinutesXAxisDateArray.count {
        values5.append(ChartDataEntry(x: Double(m), y: tenMinutesHistoricalYvaluesArray[m], data: UIImage(named: "icon")))
      }
    }
    print("set value 5 \(values5.count)")
    
    
    //#--- For hours data set
    for k in 0..<self.hoursHistoricalYvaluesArray.count {
      if k < hoursXAxisDateArray.count {
        values3.append(ChartDataEntry(x: Double(k), y: hoursHistoricalYvaluesArray[k], data: UIImage(named: "icon")))
      }
    }
    print("set value 3 \(values3.count)")
    
    let set1 = self.updateChartView(values: values1)
    let set2 = self.updateChartView(values: values2)
    let set3 = self.updateChartView(values: values3)
    let set4 = self.updateChartView(values: values4)
    let set5 = self.updateChartView(values: values5)
    let set6 = self.updateChartView(values: values6)
    
    
    //            let data = LineChartData(dataSets: [set1])
    let data = LineChartData(dataSets: [set1,set2,set3,set4,set5,set6])
    chartView.data = data
    
    
    if (chartView.data?.dataSets.count)! > 0 {
      self.chartView.data?.dataSets[0].visible = true // seconds
      self.chartView.data?.dataSets[1].visible = false // minutes
      self.chartView.data?.dataSets[2].visible = false  // hour
      self.chartView.data?.dataSets[3].visible = false // 10 seconds
      self.chartView.data?.dataSets[4].visible = false // 10 minutes
      self.chartView.data?.dataSets[5].visible = false // 2 seconds
      
    }
    
    if isMarketTimeClosed{
      self.chartView.setDragOffsetX(100)
    }
    
    chartView.data?.notifyDataChanged()
    chartView.notifyDataSetChanged()
    
    
    if (chartView.data?.dataSets[0].entryCount)! > 0{
      self.updateYaxisValue()
    }
    print("##### Chart view drawing ended ##### --- \(Date())")
    
    //    self.performSelector(onMainThread: #selector(self.hideActivityIndicator), with: nil, waitUntilDone: true)
    
      self.showOrHidePointValues(isHide: false)
      self.hideActivityIndicator()
    
    
    if self.isMarketTimeClosed {
      self.updateTagViewMarketClosedTime()
    }
    
  }
  
  func process60MinHistoryDataInAsync() -> Void {
    
    
    
    //            let data = LineChartData(dataSets: [set1])
    if (self.chartView.data?.dataSets.count)! > 3 {
      let set1 = self.chartView.data?.dataSets[3] as! LineChartDataSet
      var values1 = [ChartDataEntry]()
      for k in 0..<self.tenSecondseHistoricalYvaluesArray.count {
        if k < tenSecondsXAxisDateArray.count {
          values1.append(ChartDataEntry(x: Double(k), y: tenSecondseHistoricalYvaluesArray[k], data: UIImage(named: "icon")))
        }
      }
      
      set1.values = values1
      
      
      let set2 = self.chartView.data?.dataSets[5] as! LineChartDataSet
      var values2 = [ChartDataEntry]()
      for l in 0..<self.fiveSecondseHistoricalYvaluesArray.count {
        if l < fiveSecondsXAxisDateArray.count {
          values2.append(ChartDataEntry(x: Double(l), y: fiveSecondseHistoricalYvaluesArray[l], data: UIImage(named: "icon")))
        }
      }
      
      set2.values = values2
      
      
      
      UI {
        
        if self.takeProfitView.isHidden == false {
          var key : String = ""
          if self.currentDocumentsDict["PRODUCT"] != nil {
            key = self.currentDocumentsDict["PRODUCT"] as! String
          }else{
            key = "NIFTY"
          }
          if self.tradePoints.has(key: "\(key)_fiveSec") {
            self.tradePoints["\(key)_fiveSec"] = "\(self.fiveSecondseHistoricalYvaluesArray.count-5)"
          }
          
          if self.tradePoints.has(key: "\(key)_tenSec") {
            self.tradePoints["\(key)_tenSec"] = "\(self.tenSecondseHistoricalYvaluesArray.count-2)"
            
          }
        }
        
        self.chartView.setNeedsDisplay()
        
        self.chartView.data?.notifyDataChanged()
        self.chartView.notifyDataSetChanged()
        self.is60MinProcessing = false
        
        self.updateYaxisValue()
        if self.selectedSegmentIndex == 5 || self.selectedSegmentIndex == 6{
          self.reloadBottomScrollView(selecetdIndex: self.selectedSegmentIndex)
        }
        self.chartView.noDataText = ""
        self.chartView.reloadInputViews()
        self.is60MinChartCompleted = true
        
        //self.performSelector(onMainThread: #selector(self.hideActivityIndicator), with: nil, waitUntilDone: true)
      }
      
    }
    
  }
  
  func minitesProcessDataInAsnyc() -> Void{
    
    print ("self.chartView.data?.dataSets.count  \(self.chartView.data?.dataSets.count)")
    
    guard let dataset = self.chartView.data?.dataSets else {
      
      return
    }
    
    if (self.chartView.data?.dataSets.count)! > 1 &&  (self.chartView.data?.dataSets.count)! > 4 {
      
      let set2 = self.chartView.data?.dataSets[1] as! LineChartDataSet
      var values2 = [ChartDataEntry]()
      for j in 0..<self.minuteHistoricalYvaluesArray.count {
        if j < minuteXAxisDateArray.count {
          values2.append(ChartDataEntry(x: Double(j), y: minuteHistoricalYvaluesArray[j], data: UIImage(named: "icon")))
        }
      }
      
      set2.values = values2
      
      
      let set1 = self.chartView.data?.dataSets[4] as! LineChartDataSet
      var values5 = [ChartDataEntry]()
      for m in 0..<self.tenMinutesHistoricalYvaluesArray.count {
        if m < tenMinutesXAxisDateArray.count {
          values5.append(ChartDataEntry(x: Double(m), y: tenMinutesHistoricalYvaluesArray[m], data: UIImage(named: "icon")))
        }
      }
      set1.values = values5
      
      
      
      
      UI {
        self.chartView.setNeedsDisplay()
        self.chartView.data?.notifyDataChanged()
        self.chartView.notifyDataSetChanged()
        self.updateYaxisValue()
        if self.selectedSegmentIndex == 2 || self.selectedSegmentIndex == 3 || self.selectedSegmentIndex == 4 {
          self.reloadBottomScrollView(selecetdIndex: self.selectedSegmentIndex)
        }
        self.chartView.noDataText = ""
        self.chartView.reloadInputViews()
      }
      
    }
  }
  
  
  func hoursProcessDataInAsnyc() -> Void{
    if (self.chartView.data?.dataSets.count)! > 2 {
      
      let set2 = self.chartView.data?.dataSets[2] as! LineChartDataSet
      var values3 = [ChartDataEntry]()
      for k in 0..<self.hoursHistoricalYvaluesArray.count {
        if k < hoursXAxisDateArray.count {
          values3.append(ChartDataEntry(x: Double(k), y: hoursHistoricalYvaluesArray[k], data: UIImage(named: "icon")))
        }
      }
      
      set2.values = values3
      
      
      
      chartView.setNeedsDisplay()
      chartView.data?.notifyDataChanged()
      chartView.notifyDataSetChanged()
      
      UI {
        self.updateYaxisValue()
        if self.selectedSegmentIndex == 1 {
          self.reloadBottomScrollView(selecetdIndex: self.selectedSegmentIndex)
        }
        self.chartView.noDataText = ""
        self.chartView.reloadInputViews()
      }
      
    }
  }
  
  /// Update live data point to the existing values and draw the chart
  ///
  /// - Parameters:
  ///   - yVal: Current value point
  ///   - xVal: Current time string
  ///   - xValTime: Current date and time
  func loadIncreMethod(yVal:Double , xVal : String, xValTime : Date) -> Void{
    
    if self.takeProfitView.isHidden == false && self.yellowLineView.isHidden == false{
      self.calculateTradePrice(isUpBtnSelected: self.upDirBtn.isSelected)
    }
    
    self.updateSlider()
    if UserDefaults.standard.value(forKey: "ChartViewLowestVal") != nil{
      self.lowestVisibleX = UserDefaults.standard.value(forKey: "ChartViewLowestVal") as! Double
    }
    
    secondsLimit = secondsLimit+1
    
    var set1: LineChartDataSet? = nil
    var values1 = [ChartDataEntry]()
    if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 0 {
      set1 = (chartView.data?.dataSets[0] as? LineChartDataSet)!
      values1 = (set1?.values)!
      values1.append(ChartDataEntry(x: Double((set1?.entryCount)! + 1), y: yVal, data: UIImage(named: "icon")))
      set1?.values = values1
      
    }
    
    self.secondXAxisDateArray.append(xVal)
    self.secondHistoricalYvaluesArray.append(yVal)
    self.secondxAxisDateTimeArray.append(xValTime)
    
    
    if secondsLimit == 2 {  // Five sec dataset
      fiveSecondsLimt = fiveSecondsLimt + 1
      secondsLimit = 0
      
      var set6: LineChartDataSet? = nil
      var values6 = [ChartDataEntry]()
      if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 5 {
        set6 = (chartView.data?.dataSets[5] as? LineChartDataSet)!
        values6 = (set6?.values)!
        
        values6.append(ChartDataEntry(x: Double((set6?.entryCount)! + 1), y: yVal, data: UIImage(named: "icon")))
        set6?.values = values6
      }
      self.fiveSecondsXAxisDateArray.append(xVal)
      self.fiveSecondseHistoricalYvaluesArray.append(yVal)
      self.fiveSecondsxAxisDateTimeArray.append(xValTime)
      
      
    }
    
    if fiveSecondsLimt ==  5 {  // append every 10 seconds data for 60m view
      tenSecondsLimt = tenSecondsLimt + 1
      fiveSecondsLimt = 0
      
      var set4: LineChartDataSet? = nil
      var values4 = [ChartDataEntry]()
      if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 3 {
        set4 = (chartView.data?.dataSets[3] as? LineChartDataSet)!
        values4 = (set4?.values)!
        values4.append(ChartDataEntry(x: Double((set4?.entryCount)! + 1), y: yVal, data: UIImage(named: "icon")))
        set4?.values = values4
      }
      
      self.tenSecondsXAxisDateArray.append(xVal)
      self.tenSecondseHistoricalYvaluesArray.append(yVal)
      self.tenSecondsxAxisDateTimeArray.append(xValTime)
    }
    
    if tenSecondsLimt == 6 {  // apppend every 60 seconds data for 1D & 2D view
      tenSecondsLimt = 0
      minutesLimit = minutesLimit + 1
      self.minuteXAxisDateArray.append(xVal)
      self.minuteHistoricalYvaluesArray.append(yVal)
      self.minutexAxisDateTimeArray.append(xValTime)
      
      var set2: LineChartDataSet? = nil
      var values2 = [ChartDataEntry]()
      if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 1 {
        set2 = (chartView.data?.dataSets[1] as? LineChartDataSet)!
        values2 = (set2?.values)!
        values2.append(ChartDataEntry(x: Double((set2?.entryCount)! + 1), y: yVal, data: UIImage(named: "icon")))
        set2?.values = values2
      }
      
    }
    
    if minutesLimit  == 10 {  // append every 10 minutes data for 5D view
      tenMinutesLimit = tenMinutesLimit + 1
      minutesLimit = 0
      self.tenMinutesXAxisDateArray.append(xVal)
      self.tenMinutesHistoricalYvaluesArray.append(yVal)
      self.tenMinutesxAxisDateTimeArray.append(xValTime)
      
      var set5: LineChartDataSet? = nil
      var values5 = [ChartDataEntry]()
      if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 4 {
        set5 = (chartView.data?.dataSets[4] as? LineChartDataSet)!
        values5 = (set5?.values)!
        values5.append(ChartDataEntry(x: Double((set5?.entryCount)! + 1), y: yVal, data: UIImage(named: "icon")))
        set5?.values = values5
      }
    }
    
    if tenMinutesLimit == 6{  // append every 60 minutes data for 1Month view
      tenMinutesLimit = 0
      self.hoursXAxisDateArray.append(xVal)
      self.hoursHistoricalYvaluesArray.append(yVal)
      self.hoursxAxisDateTimeArray.append(xValTime)
      
      var set3: LineChartDataSet? = nil
      var values3 = [ChartDataEntry]()
      if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 2 {
        set3 = (chartView.data?.dataSets[2] as? LineChartDataSet)!
        values3 = (set3?.values)!
        values3.append(ChartDataEntry(x: Double((set3?.entryCount)! + 1), y: yVal, data: UIImage(named: "icon")))
        set3?.values = values3
      }
      
    }
    
    self.updateXAxisMaxValues()
    
    xAxisDateTimeArray.append(xValTime)
    
    //self.chartView.setNeedsDisplay()
    
  }
  
  
  func updateXAxisMaxValues() -> Void{
    switch selectedSegmentIndex {
    case 1:  //1 hour  -- 1Month
      self.chartView.xAxis.axisMaximum = Double(self.hoursHistoricalYvaluesArray.count)
      chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:hoursXAxisDateArray)
      break
    case 2 :  // Every 10 min  -- 5D
      if self.tenMinutesHistoricalYvaluesArray.count < 180{
        self.chartView.xAxis.axisMaximum = Double(130)
      }else{
        self.chartView.xAxis.axisMaximum = Double(self.tenMinutesHistoricalYvaluesArray.count)
      }
      chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:tenMinutesXAxisDateArray)
      break
    case 3 :  // 2D
      self.chartView.xAxis.axisMaximum = Double(self.minuteHistoricalYvaluesArray.count)
      chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:minuteXAxisDateArray)
      break
    case 4 :  // Every min   --- 1D & 2D
      var index = self.minuteXAxisDateArray.index(of: "\n9:15")
      if index == nil {
        index = self.minuteXAxisDateArray.index(of: "\n11:45")
      }
      if index != nil {
        if self.minuteXAxisDateArray.count - index! <= 50 {
          self.chartView.xAxis.axisMaximum = Double(index! + 250)
        }
        else if self.minuteXAxisDateArray.count - index! >= 300 {
          self.chartView.xAxis.axisMaximum = Double(self.minuteHistoricalYvaluesArray.count)
          
        }else{
          self.chartView.xAxis.axisMaximum = Double(index! + 300)
        }
      }else{
        self.chartView.xAxis.axisMaximum = Double(self.minuteHistoricalYvaluesArray.count)
      }
      
      chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:minuteXAxisDateArray)
      break
    case 5:  // Every 10Sec  --- 60m
      if self.tenSecondseHistoricalYvaluesArray.count < 180 {
        self.chartView.xAxis.axisMaximum = Double(100)
        
      }else{
        self.chartView.xAxis.axisMaximum = Double(self.tenSecondseHistoricalYvaluesArray.count)
        
      }
      chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:tenSecondsXAxisDateArray)
      break
    case 6:
      self.chartView.xAxis.axisMaximum = Double(self.fiveSecondseHistoricalYvaluesArray.count)
      chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:fiveSecondsXAxisDateArray)
      break
    default:  //Every sec   -- 15m and 5m
      self.chartView.xAxis.axisMaximum = Double(self.secondHistoricalYvaluesArray.count)
      chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:secondXAxisDateArray)
    }
    
    chartView.data?.notifyDataChanged()
    chartView.notifyDataSetChanged()
    self.updateYaxisValue()
  }
  
  /// Update the yellow image and line at specific trade action
  ///
  /// - Parameter notification: Post nofitication from the chartview
  func updatelayers(notification: Notification){
    
    self.labelTimer.invalidate()
    
    let datapoint : CGPoint = CGPointFromString(UserDefaults.standard.value(forKey: "Datapoint") as! String)
    
    
    //        let actualDatapoint : CGPoint = CGPointFromString(UserDefaults.standard.value(forKey: "ActualDatapoint") as! String)
    
    let topDatapoint : CGPoint = CGPointFromString(UserDefaults.standard.value(forKey: "TopLinepoint") as! String)
    //        let actualTopDatapoint : CGPoint = CGPointFromString(UserDefaults.standard.value(forKey: "ActualTopLinepoint") as! String)
    
    let bottomDatapoint : CGPoint = CGPointFromString(UserDefaults.standard.value(forKey: "BottomLinepoint") as! String)
    //    print("top data point \(topDatapoint)")
    //    print("bottom data point \(bottomDatapoint)")
    var yellowPoint : CGPoint!
    if UserDefaults.standard.value(forKey: "YellowPointLine") != nil{
      yellowPoint = CGPointFromString(UserDefaults.standard.value(forKey: "YellowPointLine") as! String)
    }
    self.outerView.isHidden = true
    self.pointView.isHidden = true
    
    self.updateRedFlagView()
    
    //         Line view
    if datapoint.x > self.pointValue{
      self.showOrHidePointValues(isHide: true)
    }else{
      if (chartView.data?.dataSets[0].entryCount)! > 1 {
        self.showOrHidePointValues(isHide: false)
      }
    }
    
    UIView.animate(withDuration: 1, delay: 0, options: .curveEaseIn, animations: {
        self.pointViewLeadingConstraint.constant = datapoint.x-2
        self.pointViewTopConstraint.constant = datapoint.y-3
        if self.appThemeUpdating == false {
          self.view.layoutIfNeeded()
        }
      
    }) { (true) in
      let dataCount = self.chartView.data?.dataSets[0].entryCount
      if dataCount != nil && dataCount!%20 == 0{
        //                                self.chartView.moveViewToX(Double(dataCount!))
      }
      UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
        
          
          
          if yellowPoint != nil{
            
            self.yellowPointViewTopConstraint.constant = yellowPoint.y
          }
          self.greenLineViewTopConstraint.constant = topDatapoint.y
          self.redLineViewTopConstraint.constant = bottomDatapoint.y
          if self.appThemeUpdating == false {
            self.view.layoutIfNeeded()
          }
        
      }, completion: nil)
    }
    
    
    
  }
  
  
  /// Update the current value in tag line
  ///
  /// - Parameter dataPoint: Current value
  func updateTagViewDataPoints(dataPoint:Double) -> Void{
    let currentDataPoint = String(format: "%0.6f", dataPoint)
    
    if self.lastClosedPrice > dataPoint{
      
      self.tagDataValue.textColor = UIColor.red
    }else{
      self.tagDataValue.textColor = UIColor.green
    }
    
    
    
      self.tagDataValue.isHidden = false
      var token = currentDataPoint.components(separatedBy: ".")
      if token.count > 0 {
        self.dataFeedYPoint.text = token[0]//String(currentDataPoint.characters.prefix(currentDataPoint.characters.count-3))
      }
      self.tagDataValue.text = "\(abs(Int(self.lastClosedPrice - Double(dataPoint))))"
    
  }
  
  func updateTagViewMarketClosedTime() -> Void{
    print("updateTagViewMarketClosedTime")
    if self.secondHistoricalYvaluesArray.count > 0{
      let currentDataPoint = String(format: "%0.6f", self.secondHistoricalYvaluesArray.last!)
      
      if self.lastClosedPrice > self.secondHistoricalYvaluesArray.last!{
        
          self.tagDataValue.textColor = UIColor.red
        
      }else{
        
          self.tagDataValue.textColor = UIColor.green
        
      }
      var token = currentDataPoint.components(separatedBy: ".")
    
        
        self.dataFeedYPoint.text = token[0]
        self.tagDataValue.text = "\(abs(Int(self.lastClosedPrice - Double(self.secondHistoricalYvaluesArray.last!))))"
      
      
    }
  }
  
  
  /// Update Red flag view with the market closed time
  func updateRedFlagView()->Void{
    let existingdataPoint = UserDefaults.standard.value(forKey: "Datapoint")
    if existingdataPoint != nil {
      let datapoint : CGPoint = CGPointFromString(existingdataPoint as! String)
      
        self.pointViewLeadingConstraint.constant = datapoint.x-2
        self.dataPointViewTopConstraint.constant = datapoint.y-13
        
        if  self.isMarketTimeClosed == true{
          //          print("updateRedFlagView")
          self.redFlagLeadingConstraint.constant = datapoint.x + 14
          self.chartView.setDragOffsetX(100)
          
        }else{
          self.redFlagLeadingConstraint.constant = self.chartView.frame.size.width - 110
        }
      
    }
    
  }
  
  // MARK: - ChartViewDelegate
  func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
    print("chartValueSelected")
  }
  
  func chartValueNothingSelected(_ chartView: ChartViewBase) {
    print("chartValueNothingSelected")
  }
  
  
  // MARK:- TABLEVIEW DATASOURCE AND DELEGATES
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == popUpTableView{
      return instrumentDict.count
    }else if tableView == filterTableView{
      if showAllFilter{
        filterTableHeightConstraint.constant = 150
        return 5
      }
      filterTableHeightConstraint.constant = 30
      return 1
    }
    return menuArray.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if tableView == popUpTableView{
      let cell = tableView.dequeueReusableCell(withIdentifier: "PopUpTableViewCell", for: indexPath) as! PopUpTableViewCell
      DispatchQueue.global(qos: .background).async {
        //let companyProfitPercentage:String =
        let profitPercentage = self.instrumentDict[indexPath.row]["profitPercent"]
        if  profitPercentage != nil{
          let profitPercent = profitPercentage as! String?
          
          cell.companyProfitLbl.text = profitPercent
          let res = cell.companyProfitLbl.text?.characters.contains { ["-"].contains( $0 ) }
          if !res! {
            cell.companyProfitLbl.textColor = UIColor(colorLiteralRed: 44.0/255.0, green: 171.0/255.0, blue: 65.0/255.0, alpha: 1.0)
          }else{
            let cs = CharacterSet.init(charactersIn: "-")
            
            cell.companyProfitLbl.text = ("\(profitPercent!)%").trimmingCharacters(in: cs)
            cell.companyProfitLbl.textColor = UIColor.red
          }
        }else{
          
          self.calculateChangePercentage(accessToken: CommonValues.accessKey, instrumentToken: self.instrumentDict[indexPath.row][CommonValues.instrumentIdentifier] as! String, exchange: CommonValues.exchange, completionHandler: { (profitPer) in
            UI {
              cell.companyProfitLbl.text = "\(profitPer)%"
              let res = profitPer.characters.contains { ["-"].contains( $0 ) }
              if indexPath.row<self.instrumentDict.count{
                self.instrumentDict[indexPath.row]["profitPercent"] =  "\(profitPer)%"
              }
              if !res {
                cell.companyProfitLbl.textColor = UIColor(colorLiteralRed: 44.0/255.0, green: 171.0/255.0, blue: 65.0/255.0, alpha: 1.0)
              }else{
                let cs = CharacterSet.init(charactersIn: "-")
                cell.companyProfitLbl.text = ("\(profitPer)%").trimmingCharacters(in: cs)
                cell.companyProfitLbl.textColor = UIColor.red
              }
            }
          })
        }
      }
      cell.companyProfitLbl.font = cell.companyProfitLbl.font.withSize(10)
      cell.selectionStyle = .none
      cell.companyFavBtn.addTarget(self, action:#selector(favButtonTapped(_:)), for:.touchUpInside)
      if  instrumentDict[indexPath.row]["isFavourite"] != nil {
        cell.companyFavBtn.isSelected = instrumentDict[indexPath.row]["isFavourite"] as! Bool
      }else{
        cell.companyFavBtn.isSelected = false
      }
      
      let titleLabel = instrumentDict[indexPath.row][CommonValues.tradingSymbol] as? String
      if titleLabel?.range(of: CommonValues.currentYear) != nil {
        let range =  titleLabel?.range(of: CommonValues.currentYear)!
        let distance: Int = titleLabel!.distance(from: titleLabel!.startIndex, to: range!.lowerBound)
        
        let index: String.Index = titleLabel!.index(titleLabel!.startIndex, offsetBy: distance)
        let symbolName = titleLabel?.substring(to: index)
        let futureName = titleLabel?.substring(from: index).replace(CommonValues.currentYear, with: "")
        cell.companySymbolLbl.text = futureName
        cell.companyNameLbl.text = symbolName
      }else{
        cell.companyNameLbl.text = instrumentDict[indexPath.row]["PRODUCT"] as? String //;  assetItems[indexPath.row]
      }
      return cell
    }else if tableView == filterTableView{
      let cell = UITableViewCell.init(style: .default, reuseIdentifier: "")
      cell.textLabel?.text = filterArray[indexPath.row]
      //            cell.backgroundColor = UIColor.init(hex: 0x161c2d)
      if theme == .galaxy || theme == .galaxy2 {
        cell.backgroundColor = UIColor.init(hex: 0x201a54) //UIColor.init(hex: 0x1E242F)
        cell.textLabel?.textColor = AppTheme.Galaxy.textColor
      }
      else if theme == .white {
        cell.backgroundColor = UIColor.white
        cell.textLabel?.textColor = AppTheme.White.textColor
      }
      cell.textLabel?.font = AppFont.getRegular(pixels: 24)//UIFont.init(name: "Roboto-Regular", size: 12)
      
      cell.selectionStyle = .none
      if indexPath.row == 0{
        if theme == .galaxy || theme == .galaxy2 {
          cell.backgroundColor = UIColor.init(hex: 0x201a54) //UIColor.init(hex: 0x1E242F)
        }
        else if theme == .white {
          cell.backgroundColor = UIColor.white
        }
        if self.filterTableHeightConstraint.constant == 30{
          let triView = UIView(frame:CGRect(x: 100, y: 20, width: 10, height: 10))
          triView.triangledraw(triView.frame,corner: "BottomRight")
          triView.backgroundColor = UIColor.init(hex: 0x75839D)//IColor.green
          cell.addSubview(triView)
        }
      }
      return cell
    }else{
      let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as UITableViewCell
      cell.backgroundColor = UIColor(colorLiteralRed: 23.0/255.0, green: 30.0/255.0, blue: 50.0/255.0, alpha: 1.0)
      
      cell.textLabel?.text = menuArray[indexPath.row]
      cell.textLabel?.textColor = UIColor.white
      cell.selectionStyle = .none
      return cell
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if tableView == filterTableView{
      return 30
    }
    return 44
  }
  
  /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
   
   let view = UIView()
   
   if tableView != filterTableView {
   
   
   // The width will be the same as the cell, and the height should be set in tableView:heightForRowAtIndexPath:
   view.frame = CGRect(x: 0, y: 0, width: 240, height: 44)
   let label = UILabel()
   label.frame = CGRect(x: 10, y: 7, width: view.frame.size.width - 40, height: 30)
   
   let button   = UIButton(type: UIButtonType.custom)
   button.frame = CGRect(x : view.frame.size.width - 50, y: 0, width: 30, height: 30);
   
   label.textColor = UIColor.white
   label.font = AppFont.getRegular(pixels: 26)//UIFont.systemFont(ofSize: 15)
   button.setImage(UIImage.init(named: "close_btn"), for: .normal)
   button.addTarget(self , action: #selector(closeBtnPressed), for: .touchUpInside)
   
   view.addSubview(label)
   view.addSubview(button)
   view.backgroundColor = UIColor.init(hex: 0x201a54) //UIColor(colorLiteralRed: 23.0/255.0, green: 30.0/255.0, blue: 50.0/255.0, alpha: 1.0)
   }
   return view
   
   }*/
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    if UserDefaults.standard.value(forKey: CommonValues.tradeCGPoint) != nil {
      let key = currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
      tradePointsDict[key] = UserDefaults.standard.value(forKey: "TradeTime") as? String
      if UserDefaults.standard.value(forKey: "olderEntries") != nil {
        olderEntries[key] = UserDefaults.standard.value(forKey: "olderEntries") as AnyObject
      }
    }
    UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
    
    if tableView == popUpTableView {
      
      if isMarketTimeClosed && isChartDataAvaible {
        self.saveHistoryDatas()
      }
      if isChartDataAvaible{
        self.saveHistoryInSingleton(key: currentDocumentsDict["PRODUCT"] as! String)
      }
      
      if  selectedCellIndex != nil && selectedCellIndex?.row != 0{
        currentDocumentsDict = instrumentDict[indexPath.row]
        self.tabDocumentsDict[(selectedCellIndex?.row)!] = currentDocumentsDict
      }else{
        tabCount += 1
        if tabCount == 4{
          //collectionViewLeadingConstraint.constant = -40
        }
        if tabCount <= 4{
          currentDocumentsDict = instrumentDict[indexPath.row]
          
          UserDefaults.standard.setValue(nil, forKey: currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)
          self.tabDocumentsDict.append(currentDocumentsDict)
          selectedCellIndex = IndexPath(row:tabCount-1, section: 0)//IndexPath(row:0, section: 0)
        }
      }
      
      let lotSize = currentDocumentsDict["lot_size"] as? String
      if lotSize != nil{
        let lotSizeVar = lotSize!
        let lotSizeValue = Double(lotSizeVar)
        UserDefaults.standard.set(lotSizeValue!, forKey: "lot_size")
        
      }else{
        self.getLastQuoteOfInstrumentIdentifier(instrumentToken: self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)
      }
      
      UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
      self.hideMenu()
      self.hidePopupView()
      
      selCollectionView.reloadData()
      
      self.activityView.isHidden = false
      self.performSelector(onMainThread: #selector(self.updateText(textStr:)), with: "  ", waitUntilDone: true)
      
      self.drawChartTimer?.invalidate()
      self.drawChartTimer = nil
      self.resetInitialRightSide()
      self.clearAllArrays()
      //      self.downloadHistory(isExistingSymbol: false)
      //            self.getHistoryDataFromGlobalFeedInseconds(isExistingSymbol: false)
    }else if tableView == filterTableView{
      selectedFilter = filterArray[indexPath.row]
      filterArray.insert(filterArray[indexPath.row], at: 0)
      filterArray.remove(at: indexPath.row+1)
      
      if filterTableHeightConstraint.constant == 30{
        showAllFilter = true
        filterTableHeightConstraint.constant = 150
      }else{
        self.reloadInstrumentFilters()
        showAllFilter = false
        filterTableHeightConstraint.constant = 30
      }
      filterTableView.reloadData()
    }
    
  }
  //************************* Top Tab View *****************************///
  //MARK: Collectionview datasource &  delegates
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if tabDocumentsDict.count > 0 {
      return tabCount
    }else{
      return 0
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = selCollectionView.dequeueReusableCell(withReuseIdentifier: "TabCollectionViewCell", for: indexPath) as! TabCollectionViewCell
    cell.showCancelButtton(show: false)
    cell.tabCancelBtn.addTarget(self, action:#selector(cancelButtonTapped(_:)), for:.touchUpInside)
    cell.tabCancelBtn.tag = indexPath.row
    let titleLabel = self.tabDocumentsDict[indexPath.row][CommonValues.tradingSymbol] as? String
    if titleLabel?.range(of: CommonValues.currentYear) != nil {
      let range =  titleLabel?.range(of: CommonValues.currentYear)!
      let distance: Int = titleLabel!.distance(from: titleLabel!.startIndex, to: range!.lowerBound)
      
      let index: String.Index = titleLabel!.index(titleLabel!.startIndex, offsetBy: distance)
      let symbolName = titleLabel?.substring(to: index)
      var futureName = titleLabel?.substring(from: index).replace(CommonValues.currentYear, with: "")
      
      futureName = futureName?.replaceFutureOptions()
      
      guard let combinedText : String = symbolName! + " - " + futureName! else {
      }
      cell.tabTitleLbl.text = combinedText
      //            cell.tabDetailLbl.text = futureName
    }
    else if titleLabel?.range(of: CommonValues.nextYear) != nil {
      let range =  titleLabel?.range(of: CommonValues.nextYear)!
      let distance: Int = titleLabel!.distance(from: titleLabel!.startIndex, to: range!.lowerBound)
      
      let index: String.Index = titleLabel!.index(titleLabel!.startIndex, offsetBy: distance)
      let symbolName = titleLabel?.substring(to: index)
      var futureName = titleLabel?.substring(from: index).replace(CommonValues.currentYear, with: "")
      futureName = futureName?.replaceFutureOptions()
      
      guard let combinedText : String = symbolName! + " - " + futureName! else {
      }
      cell.tabTitleLbl.text = combinedText
      //            cell.tabDetailLbl.text = futureName
    }
    else{
      //            cell.tabTitleLbl.text = self.tabDocumentsDict[indexPath.row][CommonValues.tradingSymbol] as? String //self.documents[indexPath.row]
      //            cell.tabDetailLbl.text = self.tabDocumentsDict[indexPath.row][CommonValues.exchange] as? String //self.documents[indexPath.row]
      
      
      guard let combinedText : String? = (self.tabDocumentsDict[indexPath.row][CommonValues.tradingSymbol] as? String)! + " - " + (self.tabDocumentsDict[indexPath.row][CommonValues.exchange] as? String)! else {
      }
      cell.tabTitleLbl.text = combinedText
      //            cell.tabDetailLbl.text = futureName
    }
    
    cell.backgroundColor = UIColor.clear //UIColor.init(hex: 0x161c2d) //UIColor(colorLiteralRed: 22.0/255.0, green: 26.0/255.0, blue: 44.0/255.0, alpha: 1)
    cell.showPointerButtton(show: false)
    cell.tabTitleLbl.font = AppFont.getBold(pixels: 35)
    cell.tabDetailLbl.font = AppFont.getBold(pixels: 35)
    
    if indexPath ==  selectedCellIndex  {
      cell.showPointerButtton(show: true)
      
      if indexPath.row == 0 {
        //cell.backgroundColor = UIColor.init(hex: 0x161c2d)
      }
      if tabCount > 1{
        //cell.backgroundColor = UIColor.init(hex: 0x202b3f)// UIColor(colorLiteralRed: 35.0/255.0, green: 41.0/255.0, blue: 59.0/255.0, alpha: 1)
        if indexPath.row != 0 {
          cell.showCancelButtton(show:  true)
        }
      }
    }
    return cell
    
  }
  
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    if indexPath == selectedCellIndex{
      cell.layer.transform = CATransform3DMakeScale(0.8, 0.8, 1)
      UIView.animate(withDuration: 0.2, animations: {
        cell.layer.transform = CATransform3DMakeScale(1.1, 1.1, 1)
      },completion: { finished in
        UIView.animate(withDuration: 0.1, animations: {
          cell.layer.transform = CATransform3DMakeScale(1, 1, 1)
        })
      })
    }
  }
  
  
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if self.is60MinProcessing == true{
      print("------ Still Processing ...------")
      return
    }
    
    print("------- new segment changed -------\n\n\n\n")
    if UserDefaults.standard.value(forKey: CommonValues.tradeCGPoint) != nil {
      let key = currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
      tradePointsDict[key] = UserDefaults.standard.value(forKey: "TradeTime") as! String
      if UserDefaults.standard.value(forKey: "olderEntries") != nil {
        olderEntries[key] = UserDefaults.standard.value(forKey: "olderEntries") as AnyObject
      }
    }
    UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
    
    if isChartDataAvaible{
      self.saveHistoryInSingleton(key: currentDocumentsDict["PRODUCT"] as! String)
    }
    
    if isMarketTimeClosed && isChartDataAvaible {
      self.saveHistoryDatas()
    }
    
    var isReload : Bool = true
    self.view.bringSubview(toFront: popUpView)
    if  indexPath.row == 0 && tabCount == 1{
      if self.popUpViewLeadingConstraint.constant == 50 {
        self.popUpViewLeadingConstraint.constant = -350
      }else{
        self.popUpViewLeadingConstraint.constant = 50
      }
      isReload = false
    }else{
      preSelectedCellIndex = selectedCellIndex
      selectedCellIndex = indexPath
      var isFirstIndex = false
      if tabCount == 4{
        if indexPath.row == 0 {
          isFirstIndex = true
          if preSelectedCellIndex == selectedCellIndex {
            isReload = false
          }
        }else{
          isFirstIndex = false
        }
      }else{
        isFirstIndex = false
      }
      
      if isFirstIndex == false {
        if  preSelectedCellIndex == selectedCellIndex {
          //                self.popUpViewLeadingConstraint.constant = 50
          if self.popUpViewLeadingConstraint.constant == 50 {
            self.popUpViewLeadingConstraint.constant = -350
            
          }else{
            self.popUpViewLeadingConstraint.constant = 50
          }
          isReload = false
        }
        else{
          if self.popUpViewLeadingConstraint.constant == 50 {
            self.popUpViewLeadingConstraint.constant = -350
          }
        }
      }
      self.selCollectionView.reloadData()
      
      currentDocumentsDict = self.tabDocumentsDict[indexPath.row]
      let lotSize = currentDocumentsDict["lot_size"] as? String
      if lotSize != nil{
        let lotSizeVar = lotSize!
        let lotSizeValue = Double(lotSizeVar)
        UserDefaults.standard.set(lotSizeValue!, forKey: "lot_size")
        
      }else{
        DispatchQueue.global(qos: .background).async {
          self.getLastQuoteOfInstrumentIdentifier(instrumentToken: self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)
          UserDefaults.standard.set(self.currentDocumentsDict["lot_size"], forKey: "lot_size")
        }
      }
      if isReload {
        UI {
          self.view.bringSubview(toFront: self.activityView)
          self.activityView.isHidden = false
          self.performSelector(onMainThread: #selector(self.updateText(textStr:)), with: "  ", waitUntilDone: true)
          
        }
        self.resetInitialRightSide()
        self.fetchFromSingleton()
      }
    }
    self.updateYaxisValue()
  }
  
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    if indexPath.row < self.tabDocumentsDict.count {
      let titleLabel = self.tabDocumentsDict[indexPath.row][CommonValues.tradingSymbol] as? String
      let width = titleLabel?.getSizeOfString()
      if indexPath ==  selectedCellIndex  {
        if indexPath.row == 0 {
          return CGSize(width: width!, height: 44)
        }else{
          return CGSize(width: width!+40, height: 44)
        }
      }else{
        return CGSize(width: width!, height: 44)
      }
    }else{
      return CGSize(width: 125, height: 44)
    }
    
  }
  
  // MARK:- Filter table view action
  
  func reloadInstrumentFilters() -> Void {
    if selectedFilter == "Equities" {
      self.equitiesSegment()
    }else if selectedFilter == "Indices"{
      self.indicesSegment()
    }else if selectedFilter == "All Assets"{
      self.allAssets()
    }else{
      instrumentDict = []
      popUpTableView.reloadData()
    }
  }
  
  func allAssets() -> Void {
    UI {
      self.instrumentDict.removeAll()
      if self.isFutureOrOption{
        self.instrumentDict = self.futureSegments
      }else{
        self.instrumentDict = self.optionSegments
        
      }
      self.popUpTableView.reloadData()
    }
  }
  
  @IBAction func indicesSegment() -> Void{
    instrumentDict.removeAll()
    if isFutureOrOption {
      instrumentDict = futureSegments.filter({ (obj) -> Bool in
        let tradingSymbol = obj[CommonValues.tradingSymbol]
        if tradingSymbol as? String != nil{
          let segmentStr = tradingSymbol as! String
          return segmentStr.contain("NIFTY")
        }else{
          return false
        }
      })
    }else{
      instrumentDict = optionSegments.filter({ (obj) -> Bool in
        let tradingSymbol = obj[CommonValues.tradingSymbol]
        if tradingSymbol as? String != nil{
          let segmentStr = tradingSymbol as! String
          return segmentStr.contain("NIFTY")
        }else{
          return false
        }
      })
    }
    
    UI {
      self.popUpTableView.reloadData()
    }
    
    
  }
  
  @IBAction func equitiesSegment() -> Void{
    instrumentDict.removeAll()
    if isFutureOrOption {
      instrumentDict = futureSegments.filter({ (obj) -> Bool in
        let tradingSymbol = obj[CommonValues.tradingSymbol]
        if tradingSymbol as? String != nil{
          let segmentStr = tradingSymbol as! String
          return !segmentStr.notcontain("NIFTY")
        }else{
          return false
        }
      })
    }else{
      instrumentDict = optionSegments.filter({ (obj) -> Bool in
        let tradingSymbol = obj[CommonValues.tradingSymbol]
        if tradingSymbol as? String != nil{
          let segmentStr = tradingSymbol as! String
          return !segmentStr.notcontain("NIFTY")
        }else{
          return false
        }
      })
    }
    
    UI {
      self.popUpTableView.reloadData()
    }
  }
  
  
  /// search bar delegates
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
  }
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    searchBar.text = ""
  }
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    searchInstruments(searchStr: searchText)
    
  }
  
  func searchInstruments(searchStr : String) -> Void{
    var tempInstruments = [Dictionary<String,Any>]()
    if searchStr.characters.count > 2{
      
      if isFutureOrOption {
        tempInstruments = self.futureSegments.filter { (object) -> Bool in
          let tradingSymbol = object[CommonValues.tradingSymbol]
          if tradingSymbol as? String != nil{
            let segmentStr = tradingSymbol as! String
            return segmentStr.contain(searchStr, caseSensitive: false)
          }
          return false
        }
      }else{
        tempInstruments = self.optionSegments.filter { (object) -> Bool in
          let tradingSymbol = object[CommonValues.tradingSymbol]
          if tradingSymbol as? String != nil{
            let segmentStr = tradingSymbol as! String
            return segmentStr.contain(searchStr, caseSensitive: false)
          }
          return false
        }
      }
      
    }
    else if searchStr.characters.count == 0{
      self.reloadInstrumentFilters()
      return
    }
    if tempInstruments.count > 0{
      instrumentDict = tempInstruments
    }else{
      if isFutureOrOption {
        instrumentDict = self.futureSegments
        
      }else{
        instrumentDict = self.optionSegments
      }
    }
    
    UI {
      self.popUpTableView.reloadData()
    }
    
  }
  
  
  func stringFrom(given date: Date, formatType: String) -> String {
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = formatType
    let afterString: String = dateformatter.string(from: date)
    return afterString
  }
  
  func chartViewTapped(_ gesture: UITapGestureRecognizer) {
    self.hideMenu()
    self.hidePopupView()
  }
  
  func closeBtnPressed() -> Void {
    self.hideMenu()
    self.hidePopupView()
  }
  // Method to hide menu
  func hideMenu(){
    chartView.alpha = 1.0
    //        transparentView.isHidden = true
    UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations:
      {
        self.view.layoutIfNeeded()
    }, completion: nil)
  }
  // hide popupview
  func hidePopupView(){
    chartView.alpha = 1.0
    //        transparentView.isHidden = true
    addCountryBtn.isSelected = false
    self.popUpViewLeadingConstraint.constant = -350
    arrowPopupView.isHidden = true
    self.view.endEditing(true)
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    
  }
  
  func viewTap(sender:UITapGestureRecognizer){
    arrowPopupView.addMessage(textStr: "The Traders mood indicator displays the current Up/Down ratio.")
    arrowPopupView.isHidden = false
  }
  
  func setSliderColor() -> Void {
    
    let gradientDarkRed = AppTheme.Galaxy.gradientDownColor //UIColor(red:0.82, green:0.11, blue:0.40, alpha:1.0)
    //UIColor.red
    let gradientLight = UIColor.clear
    let gradientDarkBlue = AppTheme.Galaxy2.gradientUpColor  //UIColor(red:0.43, green:0.85, blue:0.48, alpha:1.0) //UIColor.green
    
    let color1 = gradientDarkRed.cgColor
    let color2 = gradientLight.cgColor
    let color3 = gradientDarkBlue.cgColor
    
    gradient1.colors = [color2, color1]
    gradient2.colors = [color3, color2]
    
    gradient1.backgroundColor = UIColor.clear.cgColor
    gradient2.backgroundColor = UIColor.clear.cgColor
    
    
    self.topView.layer.insertSublayer(gradient1, at: 0)
    self.bottomView.layer.insertSublayer(gradient2, at: 0)
    
    topView.layer.masksToBounds = true
    
  }
  
  func updateSlider(){
      let randomNum = arc4random_uniform(10)
      let updatedSliderPosition = CGFloat(self.values[Int(randomNum)])
      self.sliderXposition.constant = updatedSliderPosition
      
      UIView.animate(withDuration: 1.0, animations: {
          self.gradientLineView.layoutIfNeeded()
      }) { (true) in
        let topPercentVal = Int((self.topView.frame.size.height/(self.topView.frame.size.height + self.bottomView.frame.size.height))*100)
        self.topPercentlbl.text = "\(topPercentVal)%"
        self.botPercentLbl.text = "\(100-topPercentVal)%"
      }
    
  }
  
  func updateNotSureSlider(){
    let randomNum = arc4random_uniform(10)
    let updatedSliderPosition = CGFloat(self.values[Int(randomNum)])
    self.notSureSliderXposition.constant = updatedSliderPosition
    if randomNum > 5 {
      self.notsureFollowTheCrowdUpArrow.isHidden = true
      self.notsureFollowTheCrowdDownArrow.isHidden = false
    }
    else {
      self.notsureFollowTheCrowdUpArrow.isHidden = false
      self.notsureFollowTheCrowdDownArrow.isHidden = true
    }
    UIView.animate(withDuration: 1.0, animations: {
      self.notSureGradientLineView.layoutIfNeeded()
    }) { (true) in
      let topPercentVal = Int((self.notSureTopView.frame.size.height/(self.notSureTopView.frame.size.height + self.notSureBottomView.frame.size.height))*100)
      self.notSureTopPercentlbl.text = "\(topPercentVal)%"
      self.notSureBottomPercentLbl.text = "\(100-topPercentVal)%"
    }
    
  }
  
  // MARK:- Button Actions
  
  @IBAction func addCountryAction(_ sender: Any) {
    self.view.bringSubview(toFront: popUpView)
    let btn = sender as!  UIButton
    btn.isSelected = !btn.isSelected
    selectedCellIndex = IndexPath(row: 0, section: 0)
    if btn.isSelected {
      selectedCellIndex = nil
      self.popUpViewLeadingConstraint.constant = 50
      //            self.reloadInstrumentFilters()
    }else{
      self.popUpViewLeadingConstraint.constant = -350
    }
    self.instrumentDict = self.futureSegments
    buttonLeftBorderConstraint.constant = 0
    let currentMonth = Date().currentMonthStr
    
    self.instrumentDict =  self.instrumentDict.sorted {  ((($0 as Dictionary<String,Any>)[CommonValues.tradingSymbol] as! String).contain(currentMonth)) && !((($1 as Dictionary<String,Any>)[CommonValues.tradingSymbol] as! String).contain(currentMonth))}
    
    
    self.instrumentDict =  self.instrumentDict.sorted { (($0 as Dictionary<String,Any>)["isFavourite"] != nil ?(($0 as Dictionary<String,Any>)["isFavourite"] as! Bool) : false) && !(($1 as Dictionary<String,Any>)["isFavourite"] != nil ?(($1 as Dictionary<String,Any>)["isFavourite"] as! Bool) : false) }
    
    
    popUpTableView.setContentOffset(.zero, animated: false)
    
    popUpTableView.reloadData()
  }
  
  
  func resetTrade() -> Void {
    
    UI {
      self.appDelegate.sideMenuVC.tapMainAction()
      
      
      self.selectedCellIndex = IndexPath(row: 0, section: 0)
      self.selCollectionView.reloadData()
      UserDefaults.standard.set(0, forKey: CommonValues.depositAmountValue)
      
      if(self.tabDocumentsDict.count > 0){
        if self.drawChartTimer != nil {
          self.drawChartTimer?.invalidate()
          self.drawChartTimer = nil
        }
        
        for object in self.tabDocumentsDict {
          UserDefaults.standard.setValue(nil, forKey: object[CommonValues.instrumentIdentifier] as! String)
        }
        
        self.currentDocumentsDict = self.tabDocumentsDict[0]
        if self.tabDocumentsDict.count > 1 {
          self.view.bringSubview(toFront: self.activityView)
          self.activityView.isHidden = false
          self.performSelector(onMainThread: #selector(self.updateText(textStr:)), with: "Downloading history...", waitUntilDone: true)
          //        self.getHistoryDataFromGlobalFeedInseconds(isExistingSymbol: false)
        }else{
          self.getfeedBetweenInterval(isExistingSymbol: false)
        }
      }
      self.currentAmountValue = 0
      if ConfigManager.shared.enableFnOServer == false {
        self.currentDepositAmountValue = 10000
        self.depositAmountVlue = self.currentDepositAmountValue
        UserDefaults.standard.set(10000, forKey: CommonValues.depositAmountValue)
      }
      
      
      self.tradePointsDict.removeAll()
      UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
      self.resetViews()
    }
  }
  
  @IBAction func menuButtonTapped(_ sender: Any) {
    
    // For animated side menu
    appDelegate.sideMenuVC.toggleMenu()
  }
  
  //MARK: PopUpVuiew Button Actions
  @IBAction func exchangeBtnAction(_ sender: UIButton) {
    if sender == futuresBtn {
      buttonLeftBorderConstraint.constant = 0
      isFutureOrOption = true
      instrumentDict = self.futureSegments
    }else if sender == optionsBtn {
      isFutureOrOption = false
      buttonLeftBorderConstraint.constant = 45
      instrumentDict  = self.optionSegments
      self.popUpTableView.reloadData()
    }else{
      buttonLeftBorderConstraint.constant = 90
      instrumentDict = []
    }
    popUpTableView.reloadData()
  }
  
  func favButtonTapped(_ sender: AnyObject) {
    let button = sender as? UIButton
    button?.isSelected = !(button?.isSelected)!
    let buttonPosition = sender.convert(CGPoint(), to: self.popUpTableView)
    let index = self.popUpTableView.indexPathForRow(at: buttonPosition)
    
    var currentDict = self.instrumentDict[index!.row]
    if currentDict["isFavourite"] == nil{
      currentDict["isFavourite"] = true
    }else{
      currentDict["isFavourite"] = !(currentDict["isFavourite"]! as! Bool)
    }
    self.instrumentDict[index!.row] = currentDict
    
    
    if isFutureOrOption == true {
      let futureIndex = self.futureSegments.index { (object) -> Bool in
        var segmentStr = object[CommonValues.instrumentIdentifier] as! String
        return segmentStr == currentDict[CommonValues.instrumentIdentifier] as! String
      }
      
      var futureSegmentDict = self.futureSegments[futureIndex!]
      if futureSegmentDict["isFavourite"] == nil{
        futureSegmentDict["isFavourite"] = true
      }else{
        futureSegmentDict["isFavourite"] = !(futureSegmentDict["isFavourite"]! as! Bool)
      }
      self.futureSegments[futureIndex!] = futureSegmentDict
    }
    else{
      let optionIndex = self.optionSegments.index { (object) -> Bool in
        var segmentStr = object[CommonValues.instrumentIdentifier] as! String
        return segmentStr == currentDict[CommonValues.instrumentIdentifier] as! String
      }
      
      var optionSegmentDict = self.optionSegments[optionIndex!]
      if optionSegmentDict["isFavourite"] == nil{
        optionSegmentDict["isFavourite"] = true
      }else{
        optionSegmentDict["isFavourite"] = !(optionSegmentDict["isFavourite"]! as! Bool)
      }
      self.optionSegments[optionIndex!] = optionSegmentDict
    }
  }
  
  
  //MARK: TradeNow View ButtonActions - Right side
  
  @IBAction func amtDecreBtnAction(_ sender: Any) {
    
    
    
    if ConfigManager.shared.enableFnOServer == true {
      // fno api
      ConfigManager.shared.exchange.amountIndex -= 1
      currentAmountValue = ConfigManager.shared.exchange.getAmount()
      
      currentDepositAmountValue = depositAmountVlue - currentAmountValue
      amountValuelbl.text =  "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
      investedValuelbl.text = "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
      amountLabel.text = "\(CommonValues.indianRpsUTF)\(currentDepositAmountValue.rupeesFormatt())"
      UserDefaults.standard.set(currentAmountValue, forKey: "investedAmount")
      //            if amountLabel.text?.characters.count > 5{
      //                self.amountLabel.font = AppFont.getBold(pixels: 25)
      //            }else{
      self.amountValuelbl.font = AppFont.getBold(pixels: 43)
      //            }
      self.showTradeBtn()
      
      
      // if fno server enabled, call calcualte points api
      self.performCalculatePointsAPI()
    }
    else {
      // dummy trade
      // remove point to avoid confusion in drawing chart
      UserDefaults.standard.set(nil, forKey: "serverCalculatedPoint")
      
      if (currentAmountValue > iniitalAmountValue){
        currentAmountValue -= 1000
        currentDepositAmountValue = depositAmountVlue - currentAmountValue
        amountValuelbl.text =  "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
        investedValuelbl.text = "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
        amountLabel.text = "\(CommonValues.indianRpsUTF)\(currentDepositAmountValue.rupeesFormatt())"
        UserDefaults.standard.set(currentAmountValue, forKey: "investedAmount")
        //            if amountLabel.text?.characters.count > 5{
        //                self.amountLabel.font = AppFont.getBold(pixels: 25)
        //            }else{
        self.amountValuelbl.font = AppFont.getBold(pixels: 43)
        //            }
        self.showTradeBtn()
      }
    }
    
    
    
    
    if currentAmountValue <= 0 {
      self.redLineView.isHidden = true
      self.greenLineView.isHidden = true
      self.redLineLabelValue.isHidden = true
      self.greenLineLabelValue.isHidden = true
      self.orangeLineView.isHidden = true
    }
    
    
    if theme == .galaxy || theme == .galaxy2 {
      if currentAmountValue <= 0 {
        
        amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
      }
      else {
        amountDecrementBtn.setTitleColor(UIColor.white, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.white.cgColor
      }
    }
    else if theme == .white {
      if currentAmountValue <= 0 {
        amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
      }
      else {
        amountDecrementBtn.setTitleColor(AppTheme.White.amountButtonColor, for: .normal)
        amountDecrementBtn.layer.borderColor = AppTheme.White.amountButtonColor.cgColor
      }
    }
    
  }
  
  @IBAction func amtIncreBtnAction(_ sender: Any) {
    
    if ConfigManager.shared.enableFnOServer == true {
      // fno api
      ConfigManager.shared.exchange.amountIndex += 1
      currentAmountValue = ConfigManager.shared.exchange.getAmount()
      
      currentDepositAmountValue = depositAmountVlue - currentAmountValue
      
      self.amountValuelbl.text = "\(CommonValues.indianRpsUTF)\(self.currentAmountValue.rupeesFormatt())"
      self.investedValuelbl.text = "\(CommonValues.indianRpsUTF)\(self.currentAmountValue.rupeesFormatt())"
      self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(self.currentDepositAmountValue.rupeesFormatt())"
      
      UserDefaults.standard.set(currentAmountValue, forKey: "investedAmount")
      
      if (amountValuelbl.text?.characters.count)! > 6 {
        self.amountValuelbl.font = AppFont.getBold(pixels: 35)
      }else{
        self.amountValuelbl.font = AppFont.getBold(pixels: 43)
      }
      self.showTradeBtn()
      // if fno server enabled, call calcualte points api
      self.performCalculatePointsAPI()
    }
    else {
      // dummy trade
      
      // remove point to avoid confusion in drawing chart
      UserDefaults.standard.set(nil, forKey: "serverCalculatedPoint")
      if currentAmountValue+1000 <= depositAmountVlue {
        currentAmountValue += 1000
        currentDepositAmountValue = depositAmountVlue - currentAmountValue
        
        self.amountValuelbl.text = "\(CommonValues.indianRpsUTF)\(self.currentAmountValue.rupeesFormatt())"
        self.investedValuelbl.text = "\(CommonValues.indianRpsUTF)\(self.currentAmountValue.rupeesFormatt())"
        self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(self.currentDepositAmountValue.rupeesFormatt())"
        
        UserDefaults.standard.set(currentAmountValue, forKey: "investedAmount")
        
        if (amountValuelbl.text?.characters.count)! > 6 {
          self.amountValuelbl.font = AppFont.getBold(pixels: 35)
        }else{
          self.amountValuelbl.font = AppFont.getBold(pixels: 43)
        }
        self.showTradeBtn()
      }
    }
    
    
    
    
    
    
    
    if theme == .galaxy || theme == .galaxy2 {
      if currentAmountValue <= 0 {
        
        amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
      }
      else {
        amountDecrementBtn.setTitleColor(UIColor.white, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.white.cgColor
      }
    }
    else if theme == .white {
      if currentAmountValue <= 0 {
        amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
      }
      else {
        amountDecrementBtn.setTitleColor(AppTheme.White.amountButtonColor, for: .normal)
        amountDecrementBtn.layer.borderColor = AppTheme.White.amountButtonColor.cgColor
      }
    }
    
    
  }
  
  
  @IBAction func dirUpBtnAction(_ sender: Any) {
    
    if isChartDataAvaible {
      // Top line will be green, and bottom line will bered
      UserDefaults.standard.set(currentAmountValue, forKey: "investedAmount")
      self.showOrHideLineViews(isShow: true, upOrDown: true)
      self.upDirBtn.isSelected = true
      self.downDirBtn.isSelected = false
      if self.upDirBtn.isSelected == true{
        
        if theme == .galaxy || theme == .galaxy2 {
          self.upDirBtn.layer.borderColor = AppTheme.Galaxy2.upDirBGColor.cgColor
          self.upDirBtn.backgroundColor = AppTheme.Galaxy2.upDirBGColor
          self.downDirBtn.backgroundColor = UIColor.clear
        }
        else if theme == .white {
          self.upDirBtn.layer.borderColor = AppTheme.White.upDirBGColor.cgColor
          self.upDirBtn.backgroundColor = AppTheme.White.upDirBGColor
          self.downDirBtn.backgroundColor = UIColor.clear
        }
        
      }
      
      //        self.upDirBtn.isSelected = true//!self.upDirBtn.isSelected
      //        self.downDirBtn.isSelected = false//!self.downDirBtn.isSelected
      
      greenLineLabelValue.text = "+100%"
      redLineLabelValue.text = "-100%"
      if theme == .galaxy || theme == .galaxy2 {
        greenLineView.backgroundColor = AppTheme.Galaxy2.upLineColor
        greenLineLabelValue.textColor = AppTheme.Galaxy2.upLineColor
        
        redLineView.backgroundColor = AppTheme.Galaxy2.downLineColor
        redLineLabelValue.textColor = AppTheme.Galaxy2.downLineColor
        
      }else if theme == .white {
        greenLineView.backgroundColor = AppTheme.White.upLineColor
        greenLineLabelValue.textColor = AppTheme.White.upLineColor
        
        redLineView.backgroundColor = AppTheme.White.downLineColor
        redLineLabelValue.textColor = AppTheme.White.downLineColor
      }
      
      
    }
    
    if currentAmountValue <= 0 {
      self.redLineView.isHidden = true
      self.greenLineView.isHidden = true
      self.redLineLabelValue.isHidden = true
      self.greenLineLabelValue.isHidden = true
      self.orangeLineView.isHidden = true
    }
    
    if theme == .galaxy || theme == .galaxy2 {
      if currentAmountValue <= 0 {
        
        amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
      }
      else {
        amountDecrementBtn.setTitleColor(UIColor.white, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.white.cgColor
      }
    }
    else if theme == .white {
      if currentAmountValue <= 0 {
        amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
      }
      else {
        amountDecrementBtn.setTitleColor(AppTheme.White.amountButtonColor, for: .normal)
        amountDecrementBtn.layer.borderColor = AppTheme.White.amountButtonColor.cgColor
      }
    }
    
    yellowLineView.backgroundColor =  UIColor(patternImage: UIImage.drawDottedImage(width: (yellowLineView?.frame.size.width)!, height: (yellowLineView?.frame.size.height)!, color:UIColor.init(hex: 0x12c233)))
    showTradeBtn()
    
  }
  
  @IBAction func dirDownBtnAction(_ sender: Any) {
    // Top line will be red, and bottom line will be green
    
    if isChartDataAvaible {
      UserDefaults.standard.set(currentAmountValue, forKey: "investedAmount")
      self.showOrHideLineViews(isShow: true, upOrDown: false)
      self.upDirBtn.isSelected = false
      self.downDirBtn.isSelected = true
      if self.downDirBtn.isSelected == true{
        
        if theme == .galaxy || theme == .galaxy2 {
          self.downDirBtn.layer.borderColor = AppTheme.Galaxy2.downDirBGColor.cgColor
          self.downDirBtn.backgroundColor = AppTheme.Galaxy2.downDirBGColor
          self.upDirBtn.backgroundColor = UIColor.clear
        }
        else if theme == .white {
          self.downDirBtn.layer.borderColor = AppTheme.White.downDirBGColor.cgColor
          self.downDirBtn.backgroundColor = AppTheme.White.downDirBGColor
          self.upDirBtn.backgroundColor = UIColor.clear
        }
      }
      
      
      greenLineLabelValue.text = "-100%"
      redLineLabelValue.text = "+100%"
      if theme == .galaxy || theme == .galaxy2 {
        greenLineView.backgroundColor = AppTheme.Galaxy2.downLineColor
        greenLineLabelValue.textColor = AppTheme.Galaxy2.downLineColor
        
        redLineView.backgroundColor = AppTheme.Galaxy2.upLineColor
        redLineLabelValue.textColor = AppTheme.Galaxy2.upLineColor
        
      }else if theme == .white {
        greenLineView.backgroundColor = AppTheme.White.downLineColor
        greenLineLabelValue.textColor = AppTheme.White.downLineColor
        
        redLineView.backgroundColor = AppTheme.White.upLineColor
        redLineLabelValue.textColor = AppTheme.White.upLineColor
      }
      
      //      greenLineView.backgroundColor = UIColor.init(hex: 0xda4930)//UIColor(colorLiteralRed: 209.0/255.0, green: 85.0/255.0, blue: 59.0/255.0, alpha: 1.0)
      //      greenLineLabelValue.textColor = UIColor.init(hex: 0xda4930)//UIColor(colorLiteralRed: 209.0/255.0, green: 85.0/255.0, blue: 59.0/255.0, alpha: 1.0)
      //      greenLineLabelValue.text = "-100%"
      //
      //      redLineView.backgroundColor = UIColor.init(hex: 0x28e028)//UIColor(colorLiteralRed: 113.0/255.0, green: 232.0/255.0, blue: 80.0/255.0, alpha: 1.0)
      //      redLineLabelValue.textColor = UIColor.init(hex: 0x28e028) //UIColor(colorLiteralRed: 113.0/255.0, green: 232.0/255.0, blue: 80.0/255.0, alpha: 1.0)
      //      redLineLabelValue.text = "+100%"
      
      
      showTradeBtn()
    }
    
    if currentAmountValue <= 0 {
      self.redLineView.isHidden = true
      self.greenLineView.isHidden = true
      self.redLineLabelValue.isHidden = true
      self.greenLineLabelValue.isHidden = true
      self.orangeLineView.isHidden = true
    }
    if theme == .galaxy || theme == .galaxy2 {
      if currentAmountValue <= 0 {
        
        amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
      }
      else {
        amountDecrementBtn.setTitleColor(UIColor.white, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.white.cgColor
      }
    }
    else if theme == .white {
      if currentAmountValue <= 0 {
        amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
      }
      else {
        amountDecrementBtn.setTitleColor(AppTheme.White.amountButtonColor, for: .normal)
        amountDecrementBtn.layer.borderColor = AppTheme.White.amountButtonColor.cgColor
      }
    }
    
    yellowLineView.backgroundColor =  UIColor(patternImage: UIImage.drawDottedImage(width: (yellowLineView?.frame.size.width)!, height: (yellowLineView?.frame.size.height)!, color:UIColor.init(hex: 0xf49000)))
    
  }
  
  
  func showOrHideLineViews(isShow : Bool , upOrDown : Bool) -> Void {
    // isShow = If yes show lines , No = hide
    // upOrDown = If yes Up Button action , no = down button action
    greenLineView.isHidden = !isShow
    redLineView.isHidden = !isShow
    greenLineLabelValue.isHidden = !isShow
    redLineLabelValue.isHidden = !isShow
    
    //Constant value need to be changed
    if isShow {
      if upOrDown {
        // Up button action
        isUporDown  = 1
      }else{
        // Down button action
        isUporDown = 2
      }
    }
  }
  
  @IBAction func tradeBtnAction(_ sender: Any) {
    //        self.loadingIndicator(show: true)
    
    
    
    
    if ConfigManager.shared.enableFnOServer == true {//} && self.isMarketTimeClosed == false {
      // connect to FnO backend server
      self.performPlaceOrderAPI()
    }
    else {
      
      self.yellowLineView.isHidden = false
      
      self.isResetTrade = true
      
      self.amountLabel.textAlignment = .right
      UserDefaults.standard.set(true, forKey: "TradePoint")
      UserDefaults.standard.set(true, forKey: "IsTradeBtnEnabled")
      
      UserDefaults.standard.set(currentDepositAmountValue, forKey: CommonValues.depositAmountValue)
      
      lastTradePrice = currentPrice
      self.tradedAmountLabel.text = "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
      self.showTradeAlertView(isShow: true)
      self.perform(#selector(self.showTradeAlertView(isShow:)), with: false, afterDelay: 2.0)
      if self.upDirBtn.isSelected{
        self.takeProfitDirectionBtn.borderColor = UIColor.init(hex: 0x30ab49)
        self.takeProfitDirectionBtn.setImage(UIImage(named:"ArrUp_UnSel"), for: .normal)
        //            self.calculateTradePrice(isUpBtnSelected: true)
        
      }else{
        self.takeProfitDirectionBtn.borderColor = UIColor.init(hex: 0xda4930)
        self.takeProfitDirectionBtn.setImage(UIImage(named:"ArrDown_UnSel"), for: .normal)
        //            self.calculateTradePrice(isUpBtnSelected: false)
      }
      var key : String = ""
      if currentDocumentsDict["PRODUCT"] != nil {
        key = currentDocumentsDict["PRODUCT"] as! String
      }else{
        key = "NIFTY"
        currentDocumentsDict["PRODUCT"] = key
      }
      
      var set2: LineChartDataSet? = nil
      var values2 = [ChartDataEntry]()
      if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 1 {
        set2 = (chartView.data?.dataSets[1] as? LineChartDataSet)!
        values2 = (set2?.values)!
        if values2.count > 0{
          values2.removeLast()
          values2.append(ChartDataEntry(x: Double((set2?.entryCount)! + 1), y: lastTradePrice, data: UIImage(named: "icon")))
          set2?.values = values2
        }
      }
      
      var set3: LineChartDataSet? = nil
      var values3 = [ChartDataEntry]()
      if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 2 {
        set3 = (chartView.data?.dataSets[2] as? LineChartDataSet)!
        values3 = (set3?.values)!
        if values3.count > 0{
          values3.removeLast()
          values3.append(ChartDataEntry(x: Double((set3?.entryCount)! + 1), y: lastTradePrice, data: UIImage(named: "icon")))
          set3?.values = values3
        }
      }
      
      var set4: LineChartDataSet? = nil
      var values4 = [ChartDataEntry]()
      if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 3 {
        set4 = (chartView.data?.dataSets[3] as? LineChartDataSet)!
        values4 = (set4?.values)!
        if values4.count > 0 {
          values4.removeLast()
          values4.append(ChartDataEntry(x: Double((set4?.entryCount)! + 1), y: lastTradePrice, data: UIImage(named: "icon")))
          set4?.values = values4
        }
      }
      
      
      var set5: LineChartDataSet? = nil
      var values5 = [ChartDataEntry]()
      if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 4 {
        set5 = (chartView.data?.dataSets[4] as? LineChartDataSet)!
        values5 = (set5?.values)!
        if values5.count > 0 {
          values5.removeLast()
          values5.append(ChartDataEntry(x: Double((set5?.entryCount)! + 1), y: lastTradePrice, data: UIImage(named: "icon")))
          set5?.values = values5
        }
      }
      
      var set6: LineChartDataSet? = nil
      var fiveMinCount = 0
      var values6 = [ChartDataEntry]()
      if chartView.data != nil && chartView.data?.dataSetCount != nil && (chartView.data?.dataSetCount)! > 5 {
        set6 = (chartView.data?.dataSets[5] as? LineChartDataSet)!
        values6 = (set6?.values)!
        //            if values6.count > 0 && values6.count < self.fiveSecondseHistoricalYvaluesArray.count {
        //                let remainingCount = self.fiveSecondseHistoricalYvaluesArray.count - values6.count
        //                let remainigObjectes = Array(self.fiveSecondseHistoricalYvaluesArray.suffix(remainingCount))
        ////                for l in 0..<remainigObjectes.count {
        ////                    print(l)
        ////                    if l < remainigObjectes.count {
        ////                        values6.append(ChartDataEntry(x: Double(l), y: remainigObjectes[l], data: UIImage(named: "icon")))
        ////                    }
        ////                }
        //            }
        if values6.count > 0 {
          values6.removeLast()
          values6.append(ChartDataEntry(x: Double((set6?.entryCount)! + 1), y: lastTradePrice, data: UIImage(named: "icon")))
          set6?.values = values6
        }
        fiveMinCount = values6.count
      }
      
      //        let key = currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
      tradePoints["\(key)_sec"] = "\(self.secondHistoricalYvaluesArray.count-1)"
      self.minuteHistoricalYvaluesArray[self.minuteHistoricalYvaluesArray.count - 1] = lastTradePrice
      
      self.hoursHistoricalYvaluesArray[self.hoursHistoricalYvaluesArray.count - 1] = lastTradePrice
      self.tenSecondseHistoricalYvaluesArray[self.tenSecondseHistoricalYvaluesArray.count - 1] = lastTradePrice
      self.tenMinutesHistoricalYvaluesArray[self.tenMinutesHistoricalYvaluesArray.count - 1] = lastTradePrice
      self.fiveSecondseHistoricalYvaluesArray[self.fiveSecondseHistoricalYvaluesArray.count - 1] = lastTradePrice
      
      tradePoints["\(key)_min"] = "\(self.minuteHistoricalYvaluesArray.count-1)"
      tradePoints["\(key)_tenSec"] = "\(self.tenSecondseHistoricalYvaluesArray.count-1)"
      tradePoints["\(key)_hour"] = "\(self.hoursHistoricalYvaluesArray.count-1)"
      tradePoints["\(key)_tenMin"] = "\(self.tenMinutesHistoricalYvaluesArray.count-1)"
      tradePoints["\(key)_fiveSec"] = "\(fiveMinCount-1)"
      
      
      if self.upDirBtn.isSelected {
        self.calculateTradePrice(isUpBtnSelected: true)
      }else{
        self.calculateTradePrice(isUpBtnSelected: false)
      }
      
      
      //        tradeTimer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(tradeTimerClculate(timer:)), userInfo: self.upDirBtn.isSelected, repeats: true)
      
      //##---- SAVE Current dict object with selected dict *****
      if currentDepositAmountValue == 10000{
        currentDepositAmountValue = 0
      }
      UserDefaults.standard.set(currentDepositAmountValue, forKey: CommonValues.depositAmountValue)
      let lastTradedDict = [CommonValues.instrumentIdentifier:currentDocumentsDict[CommonValues.instrumentIdentifier] ,"lastTradePrice":lastTradePrice,"upSelected":self.upDirBtn.isSelected,"downselected":self.downDirBtn.isSelected,"currentAmountValue":currentAmountValue,"tradePoints":tradePoints,"Product":currentDocumentsDict["PRODUCT"]]
      UserDefaults.standard.setValue(lastTradedDict, forKey: currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)
      UserDefaults.standard.synchronize()
    }
  }
  
  //MARK: TakeProfit View ButtonActions
  
  func tradeTimerClculate(timer : Timer) -> Void {
    if self.upDirBtn.isSelected || self.downDirBtn.isSelected{
      let userInfo = timer.userInfo as! Bool
      self.calculateTradePrice(isUpBtnSelected: userInfo)
    }else{
      tradeTimer.invalidate()
    }
  }
  
  func calculateTradePrice(isUpBtnSelected : Bool) -> Void {
    
    
    
    var lotSize : Double = 75.0
    if UserDefaults.standard.value(forKey: "lot_size") != nil{
      lotSize  = UserDefaults.standard.value(forKey: "lot_size")! as! Double
    }
    
    var profitorloss : Double = 0.0
    
    if isUpBtnSelected {
      profitorloss = (currentPrice - lastTradePrice) * lotSize
      
    }else{
      profitorloss = (lastTradePrice - currentPrice) * lotSize
    }
    
    let currentValue = Double(currentAmountValue) + profitorloss
    var revisedPercentage : Double  = 0
    let percentage = (currentValue / Double(currentAmountValue)) * 100
    revisedPercentage = percentage - 100
    
    print("last Traded price ----- \(lastTradePrice) and currnet price----- \(currentPrice)")
    print("profit or loss \(profitorloss)")
    print("---- Revised Percentage \(revisedPercentage)")
    
    self.takeProfitBtn.setTitleColor(UIColor.white, for: .normal)
    if revisedPercentage.isNaN || revisedPercentage.isInfinite{
      profitPercentValuelbl.text = "\(0)%"
      revisedPercentage = 0
    }else{
      
      profitPercentValuelbl.text = "\(Int(revisedPercentage))%"
    }
    
    self.takeProfitBtn.layer.removeAllAnimations()
    if revisedPercentage > 0 {
      profitPercentValuelbl.text = "+\(Int(revisedPercentage))%"
      
      
      self.takeProfitBtn.setTitle("TAKE PROFIT", for: .normal)
      
      if theme == .galaxy || theme == .galaxy2 {
        self.takeProfitBtn.backgroundColor = UIColor.init(hex: 0x67cd74)
        profitPercentValuelbl.textColor = AppTheme.Galaxy2.profitValueColor
        totalValuelbl.textColor = AppTheme.Galaxy2.totalValueColor
      }
      else if theme == .white {
        self.takeProfitBtn.backgroundColor = UIColor.init(hex: 0x56d066)
        profitPercentValuelbl.textColor = AppTheme.White.profitValueColor
        totalValuelbl.textColor = AppTheme.White.totalValueColor
      }
      
    }else{
      
      self.takeProfitBtn.setTitle("STOP LOSS", for: .normal)
      
      
      if theme == .galaxy || theme == .galaxy2 {
        self.takeProfitBtn.backgroundColor = UIColor.init(hex: 0xa50a4b)
        profitPercentValuelbl.textColor = AppTheme.Galaxy2.lossValueColor
        totalValuelbl.textColor = AppTheme.Galaxy2.totalValueColor
      }
      else if theme == .white {
        self.takeProfitBtn.backgroundColor = UIColor.init(hex: 0xaf1857)
        profitPercentValuelbl.textColor = AppTheme.White.lossValueColor
        totalValuelbl.textColor = AppTheme.White.totalValueColor
      }
    }
    
    
      self.takeProfitBtn.layoutIfNeeded()
    
    
    
    let totalValue = Int(currentValue)
    let totalValueText =  totalValue.rupeesFormatt()
    
    totalValuelbl.text = "\(CommonValues.indianRpsUTF)\(totalValueText)"
    
    if (totalValuelbl.text?.characters.count)! > 6{
      self.profitPercentValuelbl.font = AppFont.getBold(pixels: 47)
      self.totalValuelbl.font = AppFont.getBold(pixels: 50)
    }else if (totalValuelbl.text?.characters.count)! > 4
    {
      self.profitPercentValuelbl.font = AppFont.getBold(pixels: 57)
      self.totalValuelbl.font = AppFont.getBold(pixels: 59)
    }
    else{
      self.profitPercentValuelbl.font = AppFont.getBold(pixels: 58)
      self.totalValuelbl.font = AppFont.getBold(pixels: 65)
    }
    
    if abs(Int(revisedPercentage)) >= 100 {
      print("Revised percentage \(abs(Int(revisedPercentage))) --- Original Percentage \(revisedPercentage)")
      tradeTimer.invalidate()
      if self.takeProfitBtn.titleLabel?.text == "STOP LOSS" {
        let totalValue = Int(0)
        let totalValueText =  totalValue.rupeesFormatt()
        totalValuelbl.text = "\(CommonValues.indianRpsUTF)\(totalValueText)"
        
      }else{
        let currentValue12 = Double(currentAmountValue) * 2
        let totalValueText =  currentValue12.rupeesFormatt()
        totalValuelbl.text = "\(CommonValues.indianRpsUTF)\(totalValueText)"
      }
      self.takeProfitBtnAction(self.takeProfitBtn)
    }
  }
  
  @IBAction func takeAgainBtnAction(_ sender: Any) {
    
    if theme == .galaxy || theme == .galaxy2 {
      self.upDirBtn.layer.borderColor = AppTheme.Galaxy2.upDirBGColor.cgColor
      self.downDirBtn.layer.borderColor = AppTheme.Galaxy2.downDirBGColor.cgColor
    }
    else if theme == .white {
      self.upDirBtn.layer.borderColor = AppTheme.White.upDirBGColor.cgColor
      self.downDirBtn.layer.borderColor = AppTheme.White.downDirBGColor.cgColor
    }
    self.showProfitAlertview(isShow: false)
  }
  
  // Take profit button actions
  @IBAction func takeProfitBtnAction(_ sender: Any) {
    //        self.amountLabel.textAlignment = .center
    
    if ConfigManager.shared.enableFnOServer == true {
      self.performCloseOrderAPI()
    }
    else{
      self.yellowLineView.isHidden = true
      
      self.upDirBtn.isSelected  = false
      self.downDirBtn.isSelected = false
      UserDefaults.standard.set(false, forKey: "IsTradeBtnEnabled")
      
      
      
      
      self.showProfitAlertview(isShow: true)
      
      //self.perform(#selector(self.showProfitAlertview(isShow:)), with: false, afterDelay: 2.0)
      
      var key : String = ""
      if currentDocumentsDict["PRODUCT"] != nil {
        key = currentDocumentsDict["PRODUCT"] as! String
      }else{
        key = "NIFTY"
      }
      tradePoints.removeValue(forKey: "\(key)_sec")
      tradePoints.removeValue(forKey: "\(key)_min")
      tradePoints.removeValue(forKey: "\(key)_tenSec")
      tradePoints.removeValue(forKey: "\(key)_hour")
      tradePoints.removeValue(forKey: "\(key)_tenMin")
      tradePoints.removeValue(forKey: "\(key)_fiveSec")
      UserDefaults.standard.set(false, forKey: "TradePoint")
      UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeNowMarkerPoint)
      UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
      
      self.redLineView.isHidden = true
      self.greenLineView.isHidden = true
      self.redLineLabelValue.isHidden = true
      self.greenLineLabelValue.isHidden = true
      self.orangeLineView.isHidden = true
      
      self.chartView.notifyDataSetChanged()
      
      
      tradeTimer.invalidate()
      UserDefaults.standard.setValue(nil, forKey: currentDocumentsDict[CommonValues.instrumentIdentifier] as! String )
      
      
      var profitOrLossAmount = 0
      if(totalValuelbl.text?.contain(","))!{
        profitOrLossAmount = (totalValuelbl.text?.replace("₹", with: "").replace(",", with: "").toInt)!
      }else{
        profitOrLossAmount = (totalValuelbl.text?.replace("₹", with: "").toInt)!
      }
      
      currentDepositAmountValue = currentDepositAmountValue + profitOrLossAmount
      
      UserDefaults.standard.set(currentDepositAmountValue, forKey: CommonValues.depositAmountValue)
      self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(currentDepositAmountValue.rupeesFormatt())"
    }
    
    
    
  }
  
  //MARK : - Right side reset
  func initialSetUpOfTradeNowView() -> Void{
    self.takeProfitBtn.backgroundColor = UIColor.init(hex: 0x67cd74)//UIColor.green
    self.takeProfitBtn.setTitle("TAKE PROFIT", for: .normal)
    
    self.upDirBtn.isSelected = false
    self.downDirBtn.isSelected = false
    self.upDirBtn.backgroundColor = UIColor.clear
    self.downDirBtn.backgroundColor = UIColor.clear
    
    
    if ConfigManager.shared.enableFnOServer == false {
      if UserDefaults.standard.integer(forKey: CommonValues.depositAmountValue) != 0 {
        depositAmountVlue = UserDefaults.standard.integer(forKey: CommonValues.depositAmountValue)
      }else{
        depositAmountVlue = 10000
      }
      
      currentDepositAmountValue = depositAmountVlue
    }
    else {
      if UserDefaults.standard.integer(forKey: CommonValues.depositFnOAmountValue) != 0 {
        depositAmountVlue = UserDefaults.standard.integer(forKey: CommonValues.depositFnOAmountValue)
      }else{
        depositAmountVlue = 10000
      }
      
      currentDepositAmountValue = depositAmountVlue
    }
    
    
    
    self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(currentDepositAmountValue.rupeesFormatt())"
    currentAmountValue = iniitalAmountValue  // Reset the value 0
    self.amountValuelbl.text = "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
    
    self.greenLineView.isHidden = true
    self.greenLineLabelValue.isHidden = true
    self.redLineView.isHidden = true
    self.redLineLabelValue.isHidden = true
    self.orangeLineView.isHidden = true
    
    self.tradeAlertView.isHidden = true
    self.takeProfitAlertView.isHidden = true
    
    showTradeBtn()
  }
  
  func resetInitialRightSide() -> Void {
    // Check wheather Selected instrument was traded
    // 1. Howmuch amount invested
    // 2. whether he take profit or not
    // 3. if profit or loss taken no need to show invested amount. just put a last traded point(yellow)
    // 4. If not, calculate the points how much he invested based on that we show the process
    // 5. if market closed just reset the view
    if (tradePointsDict.has(key: currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)){
      let key = currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
      //                UserDefaults.standard.setValue(tradePointsDict[key], forKey: CommonValues.tradeCGPoint)
      UserDefaults.standard.setValue("", forKey: CommonValues.tradeCGPoint)
      UserDefaults.standard.setValue(tradePointsDict[key], forKey: "TradeTime")
      if UserDefaults.standard.value(forKey: "TradeTime") != nil{
        self.getIndexOfTradePoint(withStr: UserDefaults.standard.value(forKey: "TradeTime") as! String)
      }
      if olderEntries.has(key: key) && (olderEntries[key]?.count)! > 0 {
        UserDefaults.standard.set(olderEntries[key], forKey: "olderEntries")
      }
    }
    
    let existingTradePoints = UserDefaults.standard.value(forKey: currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)
    if existingTradePoints != nil {
      if isMarketTimeClosed {
        self.resetViews()
      }
      else{
        self.tradeView.isHidden = true
        self.takeProfitView.isHidden = false
        self.notSureView.isHidden = true
        
        let lastTradedDict = existingTradePoints as! [String:Any]
        
        let upSelected = lastTradedDict["upSelected"] as! Bool
        currentAmountValue = lastTradedDict["currentAmountValue"] as! Int
        if upSelected {
          self.isChartDataAvaible = true
          self.dirUpBtnAction(upDirBtn)
          self.takeProfitDirectionBtn.borderColor = UIColor.init(hex: 0x30ab49)
          self.takeProfitDirectionBtn.setImage(UIImage(named:"ArrUp_UnSel"), for: .normal)
          
        }else{
          self.isChartDataAvaible = true
          self.dirDownBtnAction(downDirBtn)
          self.takeProfitDirectionBtn.borderColor = UIColor.init(hex: 0xda4930)
          self.takeProfitDirectionBtn.setImage(UIImage(named:"ArrDown_UnSel"), for: .normal)
          
        }
        
        self.showTradeBtn()
        self.greenLineLabelValue.isHidden = false
        self.greenLineView.isHidden = false
        self.redLineView.isHidden = false
        self.redLineLabelValue.isHidden = false
        self.yellowLineView.isHidden = false
        self.orangeLineView.isHidden = false
        self.view.bringSubview(toFront: self.greenLineView)
        self.view.bringSubview(toFront: self.greenLineLabelValue)
        self.view.bringSubview(toFront: self.redLineLabelValue)
        self.view.bringSubview(toFront: self.redLineView)
        self.view.bringSubview(toFront: self.orangeLineView)
        
        lastTradePrice = lastTradedDict["lastTradePrice"] as! Double
        currentPrice = lastTradedDict["lastTradePrice"] as! Double
        self.calculateTradePrice(isUpBtnSelected: upSelected)
        tradeTimer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(tradeTimerClculate(timer:)), userInfo: upSelected, repeats: true)
        if ConfigManager.shared.enableFnOServer == false {
          depositAmountVlue = UserDefaults.standard.integer(forKey: CommonValues.depositAmountValue)
        }else {
          depositAmountVlue = UserDefaults.standard.integer(forKey: CommonValues.depositFnOAmountValue)
        }
        currentDepositAmountValue = depositAmountVlue
        self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(currentDepositAmountValue.rupeesFormatt())"
        self.amountValuelbl.text = "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
        self.investedValuelbl.text = "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
        //                    self.tradePoints = lastTradedDict["tradePoints"] as! Dictionary<String, String>
        
        self.isResetTrade = true
      }
      
    }else{
      // here just reset the view
      self.resetViews()
    }
  }
  
  
  func resetViews() -> Void {
    self.tradeView.isHidden = false
    self.takeProfitView.isHidden = true
    self.notSureView.isHidden = true
    self.tradeAlertView.isHidden = true
    self.takeProfitAlertView.isHidden = true
    self.yellowLineView.isHidden = true
    
    self.upDirBtn.isSelected = false
    self.downDirBtn.isSelected = false
    
    self.upDirBtn.backgroundColor = UIColor.clear
    self.downDirBtn.backgroundColor = UIColor.clear
    
    
    
    
    if ConfigManager.shared.enableFnOServer == false {
      var depostVal = UserDefaults.standard.integer(forKey: CommonValues.depositAmountValue)
      if depostVal == 0 && currentDepositAmountValue != 0{
        depostVal = 10000
      }
      depositAmountVlue = depostVal
      currentDepositAmountValue = depostVal
      
      self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(currentDepositAmountValue.rupeesFormatt())"
      currentAmountValue = iniitalAmountValue  // Reset the value 0
      self.amountValuelbl.text = "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
    }
    else {
      var depostVal = UserDefaults.standard.integer(forKey: CommonValues.depositFnOAmountValue)
      if depostVal == 0 && currentDepositAmountValue != 0{
        depostVal = 10000
      }
      depositAmountVlue = depostVal
      currentDepositAmountValue = depostVal
      
      self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(currentDepositAmountValue.rupeesFormatt())"
      currentAmountValue = iniitalAmountValue  // Reset the value 0
      self.amountValuelbl.text = "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
    }
    
    
    
    
    self.greenLineView.isHidden = true
    self.greenLineLabelValue.isHidden = true
    self.redLineView.isHidden = true
    self.redLineLabelValue.isHidden = true
    self.orangeLineView.isHidden = true
    
    self.tradeTimer.invalidate()
    self.showTradeBtn()
    
  }
  
  
  @IBAction func exitBtnAction(_ sender: Any) {
    self.tradeBtn.isHidden = false
    //self.confirmView.isHidden = true
  }
  
  //MARK: Not sure View ButtonActions
  @IBAction func notSureBtnAction(_ sender: Any) {
    self.tradeView.isHidden = true
    self.notSureView.isHidden = false
    self.takeProfitView.isHidden = true
    
  }
  
  @IBAction func notSureCancelBtnAction(_ sender: Any) {
    self.notSureButtonAction()
  }
  
  @IBAction func adviseMeBtnAction(_ sender: Any) {
    //self.notSureButtonAction()
    self.view.bringSubview(toFront: self.notsureAdviceMeView)
    self.view.sendSubview(toBack: self.notsureFollowTheCrowdView)
    self.view.sendSubview(toBack: self.notsureFlipACoinView)
  }
  
  @IBAction func adviseMeBackBtnAction(_ sender: Any) {
    //self.notSureButtonAction()
    self.view.sendSubview(toBack: self.notsureAdviceMeView)
    
    self.tradeView.isHidden = true
    self.notSureView.isHidden = false
    self.takeProfitView.isHidden = true
  }
  
  @IBAction func adviseMeCloseBtnAction(_ sender: Any) {
    //self.notSureButtonAction()
    self.view.sendSubview(toBack: self.notsureAdviceMeView)
    self.tradeView.isHidden = false
    self.notSureView.isHidden = true
    self.takeProfitView.isHidden = true
  }
  
  @IBAction func followCrowdBtnAction(_ sender: Any) {
    //self.notSureButtonAction()
    self.leftChartBarClicked = false
    
    self.view.bringSubview(toFront: self.notsureFollowTheCrowdView)
    self.view.sendSubview(toBack: self.notsureAdviceMeView)
    self.view.sendSubview(toBack: self.notsureFlipACoinView)
    
    notSureGradientTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateNotSureSlider), userInfo: nil, repeats: true)
  }
  
  @IBAction func followCrowdBackBtnAction(_ sender: Any) {
    //self.notSureButtonAction()
    
    
    self.view.sendSubview(toBack: self.notsureFollowTheCrowdView)
    
    if leftChartBarClicked == true {
      leftBarChartButton.isSelected = false
      leftChartBarClicked = false
      self.tradeView.isHidden = false
      self.notSureView.isHidden = true
      self.takeProfitView.isHidden = true
    }
    else {
      self.tradeView.isHidden = true
      self.notSureView.isHidden = false
      self.takeProfitView.isHidden = true
    }
    
    
    self.notSureGradientTimer.invalidate()
  }
  
  @IBAction func followCrowdCloseBtnAction(_ sender: Any) {
    //self.notSureButtonAction()
    if leftChartBarClicked == true {
      leftBarChartButton.isSelected = false
    }
    self.view.sendSubview(toBack: self.notsureFollowTheCrowdView)
    self.tradeView.isHidden = false
    self.notSureView.isHidden = true
    self.takeProfitView.isHidden = true
    
    self.notSureGradientTimer.invalidate()
  }
  
  @IBAction func flipCoinBtnAction(_ sender: Any) {
    //self.notSureButtonAction()
    self.view.bringSubview(toFront: self.notsureFlipACoinView)
    self.view.sendSubview(toBack: self.notsureAdviceMeView)
    self.view.sendSubview(toBack: self.notsureFollowTheCrowdView)
  }
  
  @IBAction func flipCoinBackBtnAction(_ sender: Any) {
    //self.notSureButtonAction()
    self.view.sendSubview(toBack: self.notsureFlipACoinView)
    
    self.tradeView.isHidden = true
    self.notSureView.isHidden = false
    self.takeProfitView.isHidden = true
  }
  
  @IBAction func flipCoinCloseBtnAction(_ sender: Any) {
    //self.notSureButtonAction()
    self.view.sendSubview(toBack: self.notsureFlipACoinView)
    self.tradeView.isHidden = false
    self.notSureView.isHidden = true
    self.takeProfitView.isHidden = true
  }
  
  func notSureButtonAction()->Void{
    self.tradeView.isHidden = false
    self.notSureView.isHidden = true
    self.takeProfitView.isHidden = true
  }
  
  @IBAction func flipCoinDirectionBtnAction(_ sender: Any) {
    //self.notSureButtonAction()
    notsureFlipACoinDirectionLabel.text = ""
    notsureFlipACoinDirectionBtn.isSelected = !notsureFlipACoinDirectionBtn.isSelected
    
    let t = CATransition.init()
    t.startProgress = 0
    t.endProgress = 1.0
    t.type = "flip"
    t.subtype = "fromRight"
    t.duration = 0.5
    t.repeatCount = 4
    self.notsureFlipACoinDirectionBtn.layer.add(t, forKey: "transition")
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { // change 2 to desired number of seconds
      // Your code with delay
      if self.notsureFlipACoinDirectionBtn.isSelected == false {
        // direction up
        
        let image = UIImage.init(named: "notSureBGUP")
        
        self.notsureFlipACoinDirectionBtn.setImage(image, for: .normal)
        
        self.notsureFlipACoinDirectionLabel.text = "UP"
        self.notsureFlipACoinDirectionLabel.textColor = UIColor.init(hex: 0x30ab49)
      }
      else {
        // direction down
        let image = UIImage.init(named: "notSureBGDOWN")
        
        self.notsureFlipACoinDirectionBtn.setImage(image, for: .normal)
        
        self.notsureFlipACoinDirectionLabel.text = "DOWN"
        self.notsureFlipACoinDirectionLabel.textColor = UIColor.init(hex: 0xda4930)
      }
    }

    
    
  }
  
  //MARK: Collection view TabItem Close btn
  func cancelButtonTapped(_ sender: AnyObject) {
    let button = sender as? UIButton
    let cell = button?.superview?.superview as? TabCollectionViewCell
    tabCount = tabCount - 1
    self.tabDocumentsDict.remove(at: sender.tag)//self.documents.remove(at: sender.tag)
    if tabCount < 4{
      // collectionViewLeadingConstraint.constant = 0
    }
    selectedCellIndex = IndexPath(row: 0, section: 0)
    selCollectionView.reloadData()
    
    if(self.tabDocumentsDict.count > 0){
      currentDocumentsDict = self.tabDocumentsDict[0]
    }
    
    
    if ConfigManager.shared.enableFnOServer == false {
      UserDefaults.standard.set(currentDepositAmountValue, forKey: CommonValues.depositAmountValue)
      self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(currentDepositAmountValue.rupeesFormatt())"
    }
    else {
      UserDefaults.standard.set(currentDepositAmountValue, forKey: CommonValues.depositFnOAmountValue)
      self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(currentDepositAmountValue.rupeesFormatt())"
      
    }
    
    
    
    self.getLastQuoteOfInstrumentIdentifier(instrumentToken: self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)
    UserDefaults.standard.set(currentDocumentsDict["lot_size"], forKey: "lot_size")
    
    UI {
      self.activityView.isHidden = false
      self.view.bringSubview(toFront: self.activityView)
      self.performSelector(onMainThread: #selector(self.updateText(textStr:)), with: "  ", waitUntilDone: true)
    }
    
    self.resetInitialRightSide()
    self.fetchFromSingleton()
  }
  
  //#- Search Button
  @IBAction func searchBtnAction(_ sender: UIButton) {
    
    sender.isSelected = !sender.isSelected
    if sender.isSelected {
      searchbar.isHidden = false
      searchViewHeightConstraint.constant = 80
      filterBtnTopConstraint.constant = 50
    }
    else{
      searchbar.isHidden = true
      searchViewHeightConstraint.constant = 40
      filterBtnTopConstraint.constant = 7
    }
  }
  
  //MARK: HELPER METHODS
  func showTradeBtn() -> Void{
    if ((self.upDirBtn.isSelected || self.downDirBtn.isSelected) && currentAmountValue > 0 && isChartDataAvaible == true) {
      
      if self.upDirBtn.isSelected {
        UserDefaults.standard.setValue("up", forKey: CommonValues.tradeDirection)
      }
      else if self.downDirBtn.isSelected {
        UserDefaults.standard.setValue("down", forKey: CommonValues.tradeDirection)
      }
      
      
      self.tradeBtn.isUserInteractionEnabled = true
      self.tradeBtn.alpha = 1
      self.tradeBtn.setTitleColor(UIColor.white, for: .normal)
      //self.tradeBtn.backgroundColor = UIColor.init(hex: 0x1f69d7)
      
      if theme == .galaxy || theme == .galaxy2 {
        tradeBtn.setTitleColor(AppTheme.Galaxy2.actionButtonNormalTextColor, for: .normal)
        tradeBtn.setBackgroundImage(UIImage(named: AppTheme.Galaxy2.tradeBtnNormalBGImage), for: .normal)
      }
      else if theme == .white {
        tradeBtn.setTitleColor(AppTheme.White.actionButtonNormalTextColor, for: .normal)
        tradeBtn.setBackgroundImage(UIImage(named: AppTheme.White.tradeBtnNormalBGImage), for: .normal)
      }
      
    }else{
      self.tradeBtn.isUserInteractionEnabled = false
      self.tradeBtn.alpha = 0.5
      self.tradeBtn.setTitleColor(UIColor(colorLiteralRed: 61.0/255.0, green: 71.0/255.0, blue: 92.0/255.0, alpha: 1.0), for: .normal)
      //self.tradeBtn.backgroundColor = UIColor.init(hex: 0x192231)
      self.tradeView.bringSubview(toFront: self.tradeBtn)
      UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeDirection)
      
      if theme == .galaxy || theme == .galaxy2 {
        tradeBtn.setTitleColor(AppTheme.Galaxy2.actionButtonDisabledTextColor, for: .normal)
        tradeBtn.setBackgroundImage(UIImage(named: AppTheme.Galaxy2.tradeBtnDisabledBGImage), for: .normal)
      }
      else if theme == .white {
        tradeBtn.setTitleColor(AppTheme.White.actionButtonDisabledTextColor, for: .normal)
        let image = UIImage(named: AppTheme.White.tradeBtnDisabledBGImage)
        tradeBtn.setBackgroundImage(UIImage(named: AppTheme.White.tradeBtnDisabledBGImage), for: .normal)
      }
      
    }
    
    
  }
  
  func noChartDataAvailable() -> Void {
    
    print("####### ---- noChartDataAvailable \(Date()) ------ #######  ")
    self.performSelector(onMainThread: #selector(self.hideActivityIndicator), with: nil, waitUntilDone: true)
    if self.isChartDataAvaible == true{
      self.reloadBottomScrollView()
      self.chartView.notifyDataSetChanged()
      self.chartView.data?.notifyDataChanged()
    }
    self.isChartDataAvaible = false
    self.chartView.reloadInputViews()
    self.hideActivityIndicator()
    self.chartView.data = nil
    self.chartView.noDataText = "No chart data available"
    self.chartView.noDataTextColor = UIColor.white

      self.showOrHidePointValues(isHide: true)
    
    self.redFlagView.isHidden = true
    self.outerView.isHidden = true
    self.tagDataValue.isHidden = true
    self.pointView.isHidden = true
    self.dataPointView.isHidden = true
  }
  
  func marketClosedToOpenView() -> Void{
    
    let currentIdentifer = currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
    UserDefaults.standard.setValue(nil, forKey: "\(currentIdentifer)-\(self.getCurrentMarketDate().currentDateStr)-dataHistory")
    selectedCellIndex = IndexPath(row: 0, section: 0)
    selCollectionView.reloadData()
    UserDefaults.standard.set(0, forKey: CommonValues.depositAmountValue)
    
    if(self.tabDocumentsDict.count > 0){
      if drawChartTimer != nil {
        self.drawChartTimer?.invalidate()
        self.drawChartTimer = nil
      }
      
      for object in self.tabDocumentsDict {
        UserDefaults.standard.setValue(nil, forKey: object[CommonValues.instrumentIdentifier] as! String)
      }
      
      currentDocumentsDict = self.tabDocumentsDict[0]
      //            if self.tabDocumentsDict.count > 1 {
    
        self.view.bringSubview(toFront: self.activityView)
        self.activityView.isHidden = false
        self.performSelector(onMainThread: #selector(self.updateText(textStr:)), with: "Downloading history...", waitUntilDone: true)
      
      self.islast10Obj = true
      //      self.downloadHistory(isExistingSymbol: false)
      //            }
    }
  }
  
  /// Check market is open or close.
  func compareCloseTime() -> Void {
    setFlagView()
    let currentDate = Date()
    if currentDate.checkIsMarketOpen {
      if  Date().dateAtOnlyMonthandday().isTodayHoliday{
        print("market is closed")
        self.marketClosdView()
      }else{
        let isMarketTime = Date().isMarketTime
        if  isMarketTime {
          print("market is open")
          if self.isMarketClosd == true {
            self.isMarketClosd = false
            self.isMarketTimeClosed = false
            self.perform(#selector(self.marketClosedToOpenView), with: nil, afterDelay: 180)
            //                    self.marketClosedToOpenView()
          }
          self.isMarketTimeClosed = false
          self.enableOrDisableBtns(isEnable: true)
          
        }else{
          print("market is closed")
          self.marketClosdView()
        }
      }
    }else{
      self.marketClosdView()
    }
  }
  
  
  /// Market closed view . 1. Disable drawchart timer, 2. show red flag view 3. Disable to trade
  func marketClosdView(){
    self.enableOrDisableBtns(isEnable: false)
    self.isMarketTimeClosed = true
    self.isMarketClosd = true
    self.updateRedFlagView()
    if self.upDirBtn.isSelected || self.downDirBtn.isSelected {
      self.takeProfitBtnAction(self.takeProfitBtn)
    }
    //    if self.drawChartTimer != nil{
    //      self.drawChartTimer?.invalidate()
    //      self.drawChartTimer = nil
    //    }
  }
  
  /// Update the red flag view based on market time and duration period
  func setFlagView() -> Void {
    
    if self.isMarketTimeClosed{
      redFlagView.isHidden = dataPointView.isHidden
      return
    }
    let currentDate = Date()
    let marketClosedDate = Date().getMarketClosedTime
    let calender:Calendar = Calendar.current
    let components: DateComponents = calender.dateComponents([.minute], from: currentDate, to: marketClosedDate)
    let intervalMinute = components.minute
    
    var isWeekend = false
    
    if currentDate.weekday == 1 || currentDate.weekday == 7 {
      isWeekend = true
    }
    
    switch timeIntervalForFlag {
    case 60:
      if intervalMinute! <= 60 || isWeekend{
        redFlagView.isHidden = dataPointView.isHidden
      }else{
        redFlagView.isHidden = true
      }
      break
    case 15:
      if intervalMinute! <= 15 || isWeekend{
        redFlagView.isHidden = dataPointView.isHidden
        
      }else{
        redFlagView.isHidden = true
      }
      break
    case 5:
      if intervalMinute! <= 5 || isWeekend{
        redFlagView.isHidden = dataPointView.isHidden
      }else{
        redFlagView.isHidden = true
      }
      break
    default:
      redFlagView.isHidden = dataPointView.isHidden
      
      if selectedSegmentIndex == 5 || selectedSegmentIndex == 6 || selectedSegmentIndex == 7 {
        redFlagView.isHidden = true
      }
    }
    
  }
  
  /// Enable or disable trade now button based on market cloesed time comparsion
  ///
  /// - Parameter isEnable: true - Market is open and able to trade , false - Market closed
  func enableOrDisableBtns(isEnable : Bool) -> Void {
    if isEnable {
      tradeBtn.setTitle("TRADE NOW", for: .normal)
    }else{
      if self.takeProfitView.isHidden == false {
        self.takeProfitBtnAction(self.takeProfitBtn)
      }
      upDirBtn.isSelected = isEnable
      downDirBtn.isSelected = isEnable
      tradeBtn.setTitle("MARKET CLOSED", for: .normal)
      tradeTimer.invalidate()
      self.resetViews()
    }
    upDirBtn.isEnabled = isEnable
    downDirBtn.isEnabled = isEnable
    tradeBtn.isSelected = isEnable
  }
  
  /// Update loading activity text
  ///
  /// - Parameter textStr: displayed text
  func updateText(textStr : String) -> Void {
    activityLabeltext.text = textStr
    activityLabeltext.adjustsFontSizeToFitWidth = true
    self.activityLabeltext.textAlignment = .center
    
  }
  
  
  
  
  /// Process and get the default month segment from the list of symbols
  ///
  /// - Parameter isCurrentMonth: True - Currentmonth, False -  Next month
  func getDefaultMonthSegment(isCurrentMonth : Bool) -> Void {
    
    
    
    //Fetch default month
    print("Get default month process starts \(Date())")
    var  selectedMonth = Date().dateStringFromDate(toFormat: "MMM")
    if isCurrentMonth == false {
      selectedMonth = Date().nextMonth.dateStringFromDate(toFormat: "MMM")
    }
    var isInstrumentExpired: Bool = false
    var defaultMonthFuture = "NIFTY\(Date().currentYear)\(selectedMonth.uppercased())FUT"
    
    
    
    let isCurrentMonthNiftyExists =  self.futureSegments.contains { (object) -> Bool in
      let segmentStr = object[CommonValues.tradingSymbol] as! String
      return segmentStr == defaultMonthFuture
    }
    
    if isCurrentMonthNiftyExists == false {
      
      // add '1' to month unit and get MM/YY values from same date object. When new year started, this will be handled automatically
      selectedMonth = Date().nextMonth.dateStringFromDate(toFormat: "MMM")
      defaultMonthFuture = "NIFTY\(Date().nextMonth.nextYearString)\(selectedMonth.uppercased())FUT"
    }
    var dummyDict = [Dictionary<String,Any>]()
    if tabDocumentsDict.count > 1 {
      self.tabDocumentsDict.remove(at: 0)
      dummyDict = self.tabDocumentsDict
    }
    
    tabDocumentsDict = self.futureSegments.filter { (object) -> Bool in
      let tradingSymbol = object[ CommonValues.tradingSymbol]
      if tradingSymbol as? String != nil{
        let segmentStr = tradingSymbol as! String
        //print("segment Str \(segmentStr) and default \(defaultMonthFuture)")
        if segmentStr == defaultMonthFuture {
          let expiryDateStr = object[ CommonValues.expiryStr] as! String
          let expiryDate = Date().stringToDate(dateStr: expiryDateStr , format: CommonValues.dateFormat).dateAtOnlyMonthandday()
          let currentDate = Date().dateAtOnlyMonthandday()
          let result = currentDate.compare(expiryDate)
          if result == ComparisonResult.orderedDescending{
            isInstrumentExpired = true
          }else{
            return segmentStr == defaultMonthFuture
          }
        }else{
          isInstrumentExpired = true
          return false
        }
      }
      return false
    }
    
    if dummyDict.count > 0 {
      for existingDict in dummyDict {
        tabDocumentsDict.append(existingDict)
      }
    }
    
    print("Get default month process ends \(Date())")
    
    if tabDocumentsDict.count > 0{
      currentDocumentsDict = tabDocumentsDict[0]
      DispatchQueue.global(qos: .background).async {
        self.getLastQuoteOfInstrumentIdentifier(instrumentToken: self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)
        print("API Call process ends at \(Date())")
        
      }
      UserDefaults.standard.set(currentDocumentsDict, forKey: CommonValues.currentNifty)
      
      UserDefaults.standard.setValue(nil, forKey: currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)
      UI {
        self.selCollectionView.reloadData()
      }
      
      self.reloadInstrumentFilters()
      
      UI {
        if self.activityView.isHidden && self.isCurrentNiftyLoaded == false{
          self.view.bringSubview(toFront: self.activityView)
          self.activityView.isHidden = false
          self.performSelector(onMainThread: #selector(self.updateText(textStr:)), with: "Downloading history...", waitUntilDone: true)
        }
      }
    }else{
      if isInstrumentExpired == true{
        self.getDefaultMonthSegment(isCurrentMonth: false)
        return
      }
    }
    print("********************* Process completed \(Date())*************************")
    if isCurrentNiftyLoaded == false {
      //        self.downloadHistory(isExistingSymbol: false)
    }else{
      if self.isMarketTimeClosed{
      }
    }
    
  }
  
  
  
  func hideActivityPopUp() -> Void {
    UI {
      self.activityView.isHidden = true
    }
  }
  
  //MARK: Show alertviews
  // Reload bottom scroll View
  func reloadBottomScrollView(selecetdIndex : Int = 7) -> Void{
    
    let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    btn.tag = selecetdIndex
    self.segmentButtonAction(sender: btn)
  }
  
  /// Show the profit or loss alert view , after take profit button clickes
  ///
  /// - Parameter isShow: true - show , false - hide
  func showProfitAlertview(isShow : Bool) -> Void
  {
    if ConfigManager.shared.enableFnOServer == true {
      if ConfigManager.shared.orderInfo.loss > 0.0 {
        self.takeProfitCongratsLabel.text = "Tough luck!"
        self.takeProfitViewLblLeadingConstraint.constant = 20
        self.takeProfitRedBG.isHidden = true
        self.takeProfitRedMini.isHidden = true
        self.takeProfitBlueBG.isHidden = true
        self.takeProfitYellowBG.isHidden = true
      }else{
        self.takeProfitCongratsLabel.text = "Congrats!"
        self.takeProfitViewLblLeadingConstraint.constant = 35
        self.takeProfitRedBG.isHidden = false
        self.takeProfitRedMini.isHidden = false
        self.takeProfitBlueBG.isHidden = false
        self.takeProfitYellowBG.isHidden = false
      }
    }
    else {
      if takeProfitBtn.titleLabel?.text == "STOP LOSS" {
        self.takeProfitCongratsLabel.text = "Tough luck!"
        self.takeProfitViewLblLeadingConstraint.constant = 20
        self.takeProfitRedBG.isHidden = true
        self.takeProfitRedMini.isHidden = true
        self.takeProfitBlueBG.isHidden = true
        self.takeProfitYellowBG.isHidden = true
      }else{
        self.takeProfitCongratsLabel.text = "Congrats!"
        self.takeProfitViewLblLeadingConstraint.constant = 35
        self.takeProfitRedBG.isHidden = false
        self.takeProfitRedMini.isHidden = false
        self.takeProfitBlueBG.isHidden = false
        self.takeProfitYellowBG.isHidden = false
      }
    }
    
    self.takeProfitAlertView.isHidden = !isShow
    self.takeProfitviewAmountLabel.text = self.totalValuelbl.text
    self.view.bringSubview(toFront: self.takeProfitAlertView)
    if isShow == false {
      self.tradeView.isHidden = false
      self.notSureView.isHidden = true
      self.takeProfitView.isHidden = true
      self.resetInitialRightSide()
    }
    
    selCollectionView.isUserInteractionEnabled = !isShow
    
  }
  
  
  /// Show the Trade alert view , after tradenow button clickes
  ///
  /// - Parameter isShow:  true - show , false - hide
  func showTradeAlertView(isShow : Bool) -> Void {
    
      self.tradeAlertView.isHidden = !isShow
      self.view.bringSubview(toFront: self.tradeAlertView)
      if isShow == false {
        self.tradeView.isHidden = true
        self.notSureView.isHidden = true
        self.takeProfitView.isHidden = false
      }
      self.selCollectionView.isUserInteractionEnabled = !isShow
    
  }
  
  
  func getIndexOfTradePoint(withStr:String) -> Void{
    var indexes = self.xAxisDateArray.enumerated().filter {
      $0.element.contains(withStr)
      }.map{$0.offset}
    
    
    if indexes.count == 0{
      if self.xAxisDateArray.count > 0 {
        indexes.append(xAxisDateArray.count-1)
        UserDefaults.standard.setValue("\n\(xAxisDateArray.last!)", forKey: "TradeTime")
      }
    }else{
    }
  }
  
  
  
  //MARK: Global Data Feed Methods
  
  /// Fetch the last quote from the server
  ///
  /// - Parameter instrumentToken: Current selected token
  func getLastQuoteOfInstrumentIdentifier(instrumentToken : String) -> Void {
    /*print("---- getLastQuoteOfInstrumentIdentifier-----")
     let urlString = "\(CommonValues.serverIp)/\(CommonValues.getLastQuote)?accessKey=\(CommonValues.accessKey)&exchange=\(CommonValues.exchange)&instrumentIdentifier=\(instrumentToken)"
     print("---- getLastQuoteOfInstrumentIdentifier----- \(urlString)")
     NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "GET", requestBody: nil, contentType: "application/json") { (object) in
     
     if (object as? NSDictionary != nil){
     //print("getLastQuote response \(object)")
     if self.currentDocumentsDict["lot_size"] == nil{
     self.currentDocumentsDict["lot_size"] = object["QUOTATIONLOT"]!
     }
     if self.currentDocumentsDict["last_price"] == nil {
     self.currentDocumentsDict["last_price"] = object["LASTTRADEPRICE"] as! Double
     }
     self.lastClosedPrice = object["LASTTRADEPRICE"] as! Double
     }else{
     
     if (object as! String) == CommonValues.authRequestReceivedMsg{
     self.getLastQuoteOfInstrumentIdentifier(instrumentToken: self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)
     }
     else if (object as! String) == CommonValues.dataUnavalibaleMsg{
     self.getLastQuoteOfInstrumentIdentifier(instrumentToken: self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String)
     }
     else{
     if self.currentDocumentsDict["lot_size"] == nil{
     self.currentDocumentsDict["lot_size"] = 75
     }
     }
     }
     }*/
  }
  
  
  /// Calculate the change percentage either up or down
  ///
  /// - Parameters:
  ///   - accessToken: From the global data feed
  ///   - instrumentToken: Symbol [Nifty, BankNifty, etc...]
  ///   - exchange: Exchanges [NSE, NFO,etc ...]
  ///   - completionHandler: completion if success returns percentage, else error
  func calculateChangePercentage(accessToken : String , instrumentToken : String , exchange: String ,completionHandler: @escaping ((_ obj: String)->()))
  {
    let urlString = "\(CommonValues.serverIp)/\(CommonValues.getLastQuote)?accessKey=\(CommonValues.accessKey)&exchange=\(CommonValues.exchange)&instrumentIdentifier=\(instrumentToken)"
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "GET", requestBody: nil, contentType: CommonValues.jsonApplication) { (obj) in
      //            print(obj)
      if (obj as? NSDictionary != nil){
        let lastPrice = obj["LASTTRADEPRICE"] as! Double
        let closePrice = obj["CLOSE"] as! Double
        if lastPrice > 0 {
          let percentage = (lastPrice - closePrice)*100 / closePrice
          completionHandler(String(format: "%.2f", percentage))
        }else{
          completionHandler("0.0")
        }
        //Push to view c
      }else if (obj as? String != nil){
        completionHandler("0.0")
      }
    }
  }
  
  ///  Load data from the local database with selected symbol
  func loadDataFromLocalDatabase() -> Void {
    print("Load data from local starts \(Date())")
    self.clearAllArrays()
    let currentIdentifer = currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
    let historyObjectsdef = UserDefaults.standard.value(forKey: "\(currentIdentifer)-\(self.getCurrentMarketDate().currentDateStr)-dataHistory")
    if historyObjectsdef != nil{
      
      let historyObjects = historyObjectsdef as! [String : Any]
      let secondsObj = historyObjects["Seconds"] as! [String : Any]
      self.secondXAxisDateArray = secondsObj["xAxis"] as! [String]
      self.secondxAxisDateTimeArray = secondsObj["xAxisDateTime"] as! [Date]
      self.secondHistoricalYvaluesArray = secondsObj["historyVal"] as! [Double]
      self.xAxisDateTimeArray = self.secondxAxisDateTimeArray
      
      let minuteObj = historyObjects["Minutes"] as! [String : Any]
      self.minuteXAxisDateArray = minuteObj["xAxis"] as! [String]
      self.minutexAxisDateTimeArray = minuteObj["xAxisDateTime"] as! [Date]
      self.minuteHistoricalYvaluesArray = minuteObj["historyVal"] as! [Double]
      
      
      let hourObj = historyObjects["Hours"] as! [String : Any]
      self.hoursXAxisDateArray = hourObj["xAxis"] as! [String]
      self.hoursxAxisDateTimeArray = hourObj["xAxisDateTime"] as! [Date]
      self.hoursHistoricalYvaluesArray = hourObj["historyVal"] as! [Double]
      
      
      let tenSecondObj = historyObjects["tenSeconds"] as! [String : Any]
      self.tenSecondsXAxisDateArray = tenSecondObj["xAxis"] as! [String]
      self.tenSecondsxAxisDateTimeArray = tenSecondObj["xAxisDateTime"] as! [Date]
      self.tenSecondseHistoricalYvaluesArray = tenSecondObj["historyVal"] as! [Double]
      
      let tenMinuteObj = historyObjects["tenMinutes"] as! [String : Any]
      self.tenMinutesXAxisDateArray = tenMinuteObj["xAxis"] as! [String]
      self.tenMinutesxAxisDateTimeArray = tenMinuteObj["xAxisDateTime"] as! [Date]
      self.tenMinutesHistoricalYvaluesArray = tenMinuteObj["historyVal"] as! [Double]
      
      
      let fiveSecondObj = historyObjects["fiveSeconds"] as! [String : Any]
      self.fiveSecondsXAxisDateArray = fiveSecondObj["xAxis"] as! [String]
      self.fiveSecondsxAxisDateTimeArray = fiveSecondObj["xAxisDateTime"] as! [Date]
      self.fiveSecondseHistoricalYvaluesArray = fiveSecondObj["historyVal"] as! [Double]
      
      
      
      print("Load data from local completed \(Date())")
      
      UI {
        self.isChartDataAvaible = true
        self.processHistoryData(isExistigSymbol: false)
        self.reloadBottomScrollView()
        self.chartView.noDataText = ""
        self.chartView.reloadInputViews()
        self.updateYaxisValue()
      }
    }
  }
  
  
  
  
  
  
  /// Download 60 minutes data asynchronously while 5m chart rendered
  ///
  /// - Parameters:
  ///   - toDate: current date
  ///   - fromDate: last date market open time
  ///   - isRedownload: true - if server returns auth request received msg, false - initial download
  func download60min(toDate : Double , fromDate : Double , isRedownload : Bool) -> Void {
    print("##### getHistoryDataFromGlobalFeedIn60seconds process starts ##### --- \(Date())")
    var fromdateStamp : Double
    var toDateStamp :  Double
    if isRedownload == false {
      fromdateStamp = Date(timeIntervalSince1970: fromDate).lastDayCloseTime.dateAtStartingofminutes().unixTimestamp
      toDateStamp = Date(timeIntervalSince1970: toDate).unixTimestamp
      
    }else{
      fromdateStamp = fromDate
      toDateStamp = toDate
    }
    
    if isMarketTimeClosed{
      var lastMarketOpenDate = Date().yesterday
      let result = lastMarketOpenDate.compare(lastMarketOpenDate.marketOpenTime)
      
      if result == .orderedAscending || result == .orderedSame{
        lastMarketOpenDate = lastMarketOpenDate.yesterday
      }
      else{
        lastMarketOpenDate = Date()
      }
      
      if lastMarketOpenDate.weekday == 1  {
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -2)
      }
      else if lastMarketOpenDate.weekday == 7{
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -1)
      }
      
      if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
        lastMarketOpenDate = lastMarketOpenDate.yesterday
        if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
          lastMarketOpenDate = lastMarketOpenDate.yesterday
        }
      }else{
        
        
      }
      fromdateStamp =  lastMarketOpenDate.marketOpenTime.unixTimestamp
      //                todate   =  lastMarketOpenDate.marketCloseTime.unixTimestamp
      
      toDateStamp = Date().marketCloseTime.unixTimestamp
    }
    
    let instrumentToken = self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
    let urlString = "\(CommonValues.serverIp)/\(CommonValues.gethistory)/?accessKey=\(CommonValues.accessKey)&xml=false&exchange=NFO&instrumentIdentifier=\(instrumentToken)&periodicity=\(CommonValues.tick)&from=\(NSNumber(value: fromdateStamp).int64Value)&to=\(NSNumber(value: toDateStamp).int64Value)"
    
    
    NetworkUtilities.sendAsynchronousRequestToServerWithURLCompletion(actionName: urlString, httpMethod: "GET", requestBody: nil, contentType: CommonValues.jsonApplication) { (obj, url) in
      let token = self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
      if url.absoluteString.range(of: token) != nil {
        print("Current instrument token oprocess")
        self.is60MinProcessing = true
        if (obj as? NSDictionary != nil){
          self.tenSecondsXAxisDateArray.removeAll()
          self.tenSecondsxAxisDateTimeArray.removeAll()
          self.tenSecondseHistoricalYvaluesArray.removeAll()
          
          self.fiveSecondsXAxisDateArray.removeAll()
          self.fiveSecondsxAxisDateTimeArray.removeAll()
          self.fiveSecondseHistoricalYvaluesArray.removeAll()
          
          
          self.isChartDataAvaible = true
          let candleObj = (obj as? NSDictionary)?.value(forKey: "TICK") as! NSArray
          if candleObj.count > 0{
            let reversedObj = candleObj.reversed()
//            let reversedObj = candleObj
            let sequence = stride(from: 0, to: reversedObj.count , by: 10)
            
            for element in sequence {
              // do stuff
              let tempObj3 = reversedObj[element] as! NSDictionary
              
              self.tenSecondseHistoricalYvaluesArray.append(tempObj3.value(forKey: "LASTTRADEPRICE")! as! Double)
              let tickDate = Date(timeIntervalSince1970: (tempObj3.value(forKey: "LASTTRADETIME")! as! Double)/1000)
              self.tenSecondsXAxisDateArray.append(tickDate.xAxisDateStr)
              self.tenSecondsxAxisDateTimeArray.append(tickDate)
              
            }
            
            let sequence12 = stride(from: 0, to: reversedObj.count , by: 2)
            
            for element1 in sequence12 {
              // do stuff
              let tempObj3 = reversedObj[element1] as! NSDictionary
              
              self.fiveSecondseHistoricalYvaluesArray.append(tempObj3.value(forKey: "LASTTRADEPRICE")! as! Double)
              let tickDate = Date(timeIntervalSince1970: (tempObj3.value(forKey: "LASTTRADETIME")! as! Double)/1000)
              self.fiveSecondsXAxisDateArray.append(tickDate.xAxisDateStr)
              self.fiveSecondsxAxisDateTimeArray.append(tickDate)
              
            }
            
            print("##### getHistoryDataFromGlobalFeedIn60seconds process completed ##### --- \(Date())")
            self.process60MinHistoryDataInAsync()
            self.performGetBalanceAPI()
          }
        }else{
          if (obj as! String) == CommonValues.authRequestReceivedMsg{
            self.download60min(toDate: toDateStamp, fromDate: fromdateStamp, isRedownload: true)
          }  else if (obj as! String) == CommonValues.dataUnavalibaleMsg{
            self.download60min(toDate: toDateStamp, fromDate: fromdateStamp, isRedownload: true)
          }
          else{
            UI{
              self.showAlert(title: "Error", contentText: (obj as! String) , actions: nil)
              self.noChartDataAvailable()
            }
          }
        }
      }else{
        print("Some other segment")
      }
    }
  }
  
  
  /// Download history data by incrementally
  ///
  /// - Parameters:
  ///   - toDate: last dowanloaddate
  ///   - fromDate: time duration before downloaded date
  ///   - isRedownload: true - if server returns auth request received msg, false - initial download
  func incrementalDownloadHistory(toDate : Double , fromDate : Double , isRedownload : Bool) -> Void {
    
    print("##### getincrementalDownloadHistoryDataFromGlobalFeedInseconds process starts ##### --- \(Date())")
    var fromdateStamp : Double
    var toDateStamp :  Double
    if isRedownload == false {
      fromdateStamp = Date(timeIntervalSince1970: fromDate).previousFiveMinutes.dateAtStartingofminutes().unixTimestamp
      toDateStamp = Date(timeIntervalSince1970: toDate).unixTimestamp
      
    }else{
      fromdateStamp = fromDate
      toDateStamp = toDate
    }
    
    let instrumentToken = ConfigManager.shared.exchange.identifierId
    
    let urlString = "\(ConfigManager.shared.apiURL.feedServiceURL)?identifierId=\(instrumentToken)&periodicity=\(CommonValues.tick)&from=\(Int(fromdateStamp))&to=\(Int(toDateStamp))"
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "GET", requestBody: nil, contentType: CommonValues.jsonApplication) { (obj) in
      if (obj as? NSDictionary != nil){
        if obj.allKeys.count <= 0 {
          return
        }
        let candleObj = (obj as? NSDictionary)?.value(forKey: "tick") as! NSArray
        if candleObj.count > 0{
            let reversedObj = candleObj.reversed()
          self.isChartDataAvaible = true
          
          for element in candleObj {
            // do stuff
            let tempObj3 = element as! NSDictionary
            self.secondHistoricalYvaluesArray.insert(tempObj3.value(forKey: "lastTradePrice")! as! Double, at: 0)
            let tickDate = Date(timeIntervalSince1970: (tempObj3.value(forKey: "lastTradeTime")! as! Double)/1000)
            self.secondXAxisDateArray.insert(tickDate.xAxisDateStr, at: 0)
            self.secondxAxisDateTimeArray.insert(tickDate, at: 0)
          }
          
          self.chartView.data?.notifyDataChanged()
          self.chartView.notifyDataSetChanged()
          print("##### getincrementalDownloadHistoryDataFromGlobalFeedInseconds process completed ##### --- \(Date())")
        }
      }else{
        if (obj as! String) == CommonValues.authRequestReceivedMsg{
          self.incrementalDownloadHistory(toDate: toDateStamp, fromDate: fromdateStamp, isRedownload: true)
        } else if (obj as! String) == CommonValues.dataUnavalibaleMsg{
          self.incrementalDownloadHistory(toDate: toDateStamp, fromDate: fromdateStamp, isRedownload: true)
        }
        else{
          UI{
            self.showAlert(title: "Error", contentText: (obj as! String) , actions: nil)
            self.noChartDataAvailable()
          }
        }
      }
    }
  }
  
  
  
  
  /// Process the saved history data and render the graph
  func processHistoryData(isExistigSymbol :Bool) -> Void {
    
    
    DispatchQueue.main.sync {
      
      self.reloadChartViewUpdate()
      
      //    if !isExistigSymbol{
      //      self.performSelector(onMainThread: #selector(self.hideActivityIndicator), with: nil, waitUntilDone: true)
      //    }
      
      
      self.reloadBottomScrollView()
      self.chartView.noDataText = ""
      self.chartView.reloadInputViews()
      
      if self.isMarketTimeClosed{
        self.saveHistoryDatas()
      }
      
      self.hideActivityIndicator()
      
      self.getfeedBetweenInterval(isExistingSymbol:  false)
      
      if self.loadingXAxisDateArray.count > 0 {
        self.currentDataCount = 0
        
        if self.drawChartTimer == nil{
          
          self.drawChartTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.drawChartWithTimer), userInfo: nil, repeats: true)
        }
      }else if isExistigSymbol == false {
        //self.performSelector(inBackground: #selector(self.getfeedBetweenInterval(isExistingSymbol:)), with: false)
        self.perform(#selector(self.hideActivityIndicator), with: nil, afterDelay: 1.0)
      }else if isExistigSymbol == true {
        self.isCurrentExistingSymbol = true
        self.updateXAxisMaxValues()
        //self.performSelector(inBackground: #selector(self.getfeedBetweenInterval(isExistingSymbol:)), with: true)
        self.perform(#selector(self.hideActivityIndicator), with: nil, afterDelay: 1.0)
      }
      
      
      
    }
    
    
  }
  
  //MARK: Live stream data from GDF
  
  /// Getting the last minute interval(Last timestamp to current timestamp)
  func getfeedBetweenInterval(isExistingSymbol : Bool) -> Void{
    var isExists = isExistingSymbol
    //    if self.isCurrentExistingSymbol {
    //      print("\n\n\nLoading current symbol with selected period")
    //      UI {
    //        self.updateXAxisMaxValues()
    //        self.updateYaxisValue()
    //        self.chartView.data?.notifyDataChanged()
    //        self.chartView.notifyDataSetChanged()
    //        self.chartView.setNeedsDisplay()
    //      }
    //      isExists = self.isCurrentExistingSymbol
    //    }
    // From server
    //    print("##### getfeedBetweenInterval ####  --- \(Date())")
    
    
    
    if self.isMarketTimeClosed {
        //      print("##### getfeedBetweenInterval ####  Market closed. trying to invalidate timer--- \(Date())")
        if self.drawChartTimer != nil {
            //        print("##### getfeedBetweenInterval #### drawChartTimer is not nil. clearing now --- \(Date())")
            self.drawChartTimer!.invalidate()
            self.drawChartTimer = nil
        }
        //      print("##### getfeedBetweenInterval #### return control now --- \(Date())")
        return
    }

    
    
    var fromDate : Double
    
    var todate = Date().dateAtStartingofminutes().unixTimestamp
    
    if todate == self.lastTimeStamp {
      todate = Date().adding(.second, value: 10).unixTimestamp
    }
    
    fromDate = self.lastTimeStamp
    if isExists{
      todate = Date().adding(.second, value: 10).dateAtStartingofminutes().unixTimestamp
      fromDate = (self.secondxAxisDateTimeArray.last?.unixTimestamp)!
    }
    
    
    if self.isMarketTimeClosed {
      var lastMarketOpenDate = Date()
      if Date().weekday == 1  {
        lastMarketOpenDate = Date().adding(.day, value: -2)
      }
      else if Date().weekday == 7{
        lastMarketOpenDate = Date().adding(.day, value: -1)
      }
      else{
        lastMarketOpenDate = Date()
      }
      todate   =  lastMarketOpenDate.marketCloseTime.unixTimestamp
    }
    else {
      
      
      fromDate = self.lastTimeStamp
      todate = Date().unixTimestamp
      print("from date \(Date(timeIntervalSince1970: self.lastTimeStamp))")
      print("to date \(Date())")
    }
    //let instrumentToken = self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
    
    //    print("getfeedBetweenInterval from date \(fromDate) and last time stamp \(self.lastTimeStamp)")
    //    print("getfeedBetweenInterval to date \(todate)")
    
    //let urlString = "\(CommonValues.serverIp)/\(CommonValues.gethistory)/?accessKey=\(CommonValues.accessKey)&xml=false&exchange=NFO&instrumentIdentifier=\(instrumentToken)&periodicity=\(CommonValues.tick)&from=\(NSNumber(value: fromDate).int64Value)&to=\(NSNumber(value: todate).int64Value)"
    
    
    let instrumentToken = ConfigManager.shared.exchange.identifierId
    
    let urlString = "\(ConfigManager.shared.apiURL.feedServiceURL)?identifierId=\(instrumentToken)&periodicity=\(CommonValues.tick)&from=\(Int(fromDate))&to=\(Int(todate))"
    
    NetworkUtilities.sendAsynchronousRequestToServerWithURLCompletion(actionName: urlString, httpMethod: "GET", requestBody: nil, contentType: CommonValues.jsonApplication) { (obj, url) in
      let token = self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
      //      if url.absoluteString.range(of: token) != nil {
        
        UI {
            if (obj as? NSDictionary != nil){
                guard obj.allKeys != nil else {
                    
                    return
                }
                guard obj.allKeys.count > 0 else {
                    
                    return
                }
                let array = (obj as? NSDictionary)?.value(forKey: "tick") as! NSArray
                
                guard array != nil else {
                    
                    return
                }
                
                if array.count > 0{
                    
                    self.isCurrentExistingSymbol = false
                    
                    //          print("get feed between interval count \(array.count)")
                    let candleObj = (obj as? NSDictionary)?.value(forKey: "tick") as! NSArray
                    if candleObj.count > 0 {
                        
                        
                        
                        self.lastTimeStamp = (candleObj[0] as! NSDictionary).value(forKey: "lastTradeTime")! as! Double/1000
                        //            self.lastTimeStamp = (candleObj.lastObject as! NSDictionary).value(forKey: "lastTradeTime")! as! Double/1000
                        if self.loadingHistoricalYvaluesArray.count > 0 {
                            self.loadingHistoricalYvaluesArray.removeAll()
                        }
                        
                        if self.loadingXAxisDateArray.count > 0 {
                            self.loadingXAxisDateArray.removeAll()
                        }
                        
                        if self.loadingXAxisDateTimeArray.count > 0 {
                            self.loadingXAxisDateTimeArray.removeAll()
                        }
                        
                        self.currentDataCount = 0
                        
                        for tempObj in candleObj.reversed(){
                            //            for tempObj in candleObj {
                            let tempObj1 = tempObj as! NSDictionary
                            self.loadingHistoricalYvaluesArray.append(tempObj1.value(forKey: "lastTradePrice")! as! Double)
                            let tickDate = Date(timeIntervalSince1970: (tempObj1.value(forKey: "lastTradeTime")! as! Double)/1000)
                            //print("loadingXAxisDateTimeArray tickDate \(tickDate)")
                            self.loadingXAxisDateArray.append(tickDate.xAxisDateStr)
                            self.loadingXAxisDateTimeArray.append(tickDate)
                        }
                        self.xAxisDateTimeArray = self.loadingXAxisDateTimeArray
                        
                        
                        if self.drawChartTimer == nil{
                            self.drawChartTimer =  Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.drawChartWithTimer), userInfo: nil, repeats: true)
                        }
                        
                        
                        //            self.getfeedBetweenInterval(isExistingSymbol : isExistingSymbol)
                        
                        //            print("###### getfeedBetweenInterval process completed \(Date())")
                    }
                }
            }

      }
      
      
    }
  }
  
  
  /// Draw the chart with every seconds
  func drawChartWithTimer() -> Void{
    if self.drawChartTimer == nil   {
      return
    }
    
    if xAxisDateTimeArray.count > 0 {
      
      if  currentDataCount < self.loadingHistoricalYvaluesArray.count {
        self.chartView.setDragOffsetX(150)
        
        let startDate = self.xAxisDateTimeArray.last!
        let currentDate = self.loadingXAxisDateTimeArray[currentDataCount]
        let interval = currentDate.timeIntervalSince(startDate)
        
        if interval <= 5 {
            if self.loadingHistoricalYvaluesArray.count > 0{
                for i in 1..<Int(self.loadingHistoricalYvaluesArray.count){
                    if i < self.loadingHistoricalYvaluesArray.count {
                        self.loadIncreMethod(yVal: self.loadingHistoricalYvaluesArray[i], xVal: (startDate.adding(.second, value: i).xAxisDateStr), xValTime: (startDate.adding(.second, value: i)))
                    }
                }
                
                //currentDataCount = currentDataCount + Int(interval)
            }

        }
        else {
            for i in 1..<Int(interval){
                self.loadIncreMethod(yVal: self.loadingHistoricalYvaluesArray.last!, xVal: (startDate.adding(.second, value: i).xAxisDateStr), xValTime: (startDate.adding(.second, value: i)))
            }
            self.loadIncreMethod(yVal: self.loadingHistoricalYvaluesArray[currentDataCount], xVal: self.loadingXAxisDateArray[currentDataCount], xValTime: self.loadingXAxisDateTimeArray[currentDataCount])
        }
        
        
        self.currentPrice = self.loadingHistoricalYvaluesArray[currentDataCount]
        self.updateTagViewDataPoints(dataPoint: self.loadingHistoricalYvaluesArray[currentDataCount])
        self.updateYaxisValue()
        
        self.chartView.notifyDataSetChanged()
        
        currentDataCount = currentDataCount + self.loadingHistoricalYvaluesArray.count
        
        //self.loadingHistoricalYvaluesArray.removeAll()
        
        if  self.loadingHistoricalYvaluesArray.count == 0{
          BG {
            self.getfeedBetweenInterval(isExistingSymbol:  false)
          }
          
        }
      }else{
        BG {
          self.getfeedBetweenInterval(isExistingSymbol:  false)
        }
      }
    }
    else{
      BG {
        self.getfeedBetweenInterval(isExistingSymbol:  false)
      }
    }
  }
  
  //MARK:  Save and clear history datas
  
  /// Clear all the arrays
  func clearAllArrays() -> Void {
    secondsLimit = 0
    minutesLimit = 0
    tenSecondsLimt = 0
    tenMinutesLimit = 0
    fiveSecondsLimt = 0
    hoursLimit = 0
    
    self.historicalYvaluesArray.removeAll()
    self.xAxisDateArray.removeAll()
    self.xAxisDateTimeArray.removeAll()
    
    self.minuteXAxisDateArray.removeAll()
    self.minutexAxisDateTimeArray.removeAll()
    self.minuteHistoricalYvaluesArray.removeAll()
    
    self.tenSecondsXAxisDateArray.removeAll()
    self.tenSecondseHistoricalYvaluesArray.removeAll()
    self.tenSecondsxAxisDateTimeArray.removeAll()
    
    
    self.fiveSecondsXAxisDateArray.removeAll()
    self.fiveSecondseHistoricalYvaluesArray.removeAll()
    self.fiveSecondsxAxisDateTimeArray.removeAll()
    
    
    self.secondXAxisDateArray.removeAll()
    self.secondxAxisDateTimeArray.removeAll()
    self.secondHistoricalYvaluesArray.removeAll()
    
    self.hoursXAxisDateArray.removeAll()
    self.hoursxAxisDateTimeArray.removeAll()
    self.hoursHistoricalYvaluesArray.removeAll()
    
    
    self.tenMinutesXAxisDateArray.removeAll()
    self.tenMinutesxAxisDateTimeArray.removeAll()
    self.tenMinutesHistoricalYvaluesArray.removeAll()
    
    self.loadingXAxisDateArray.removeAll()
    self.loadingXAxisDateTimeArray.removeAll()
    self.loadingHistoricalYvaluesArray.removeAll()
    
    if self.drawChartTimer != nil {
      self.drawChartTimer?.invalidate()
      self.drawChartTimer = nil
    }
  }
  
  
  
  /// Save the history objects to the local database for the selected symbol while switch the next symbol in market closed time
  func saveHistoryDatas() -> Void{
    
    var currentIdentifer = ""
    #if ENABLE_DB
      currentIdentifer = currentDocumentsDictDB.identifier!
    #else
      currentIdentifer = currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
    #endif
    
    if UserDefaults.standard.value(forKey: "\(currentIdentifer)-\(self.getCurrentMarketDate().currentDateStr)-dataHistory") == nil{
      let secondsHistoryDict = ["xAxis":self.secondXAxisDateArray,"xAxisDateTime":self.secondxAxisDateTimeArray,"historyVal":self.secondHistoricalYvaluesArray] as [String : Any]
      let minuteHistoryDict = ["xAxis":self.minuteXAxisDateArray,"xAxisDateTime":self.minutexAxisDateTimeArray,"historyVal":self.minuteHistoricalYvaluesArray] as [String : Any]
      let hoursHistoryDict = ["xAxis":self.hoursXAxisDateArray,"xAxisDateTime":self.hoursxAxisDateTimeArray,"historyVal":self.hoursHistoricalYvaluesArray] as [String : Any]
      let tenSecondsHistoryDict = ["xAxis":self.tenSecondsXAxisDateArray,"xAxisDateTime":self.tenSecondsxAxisDateTimeArray,"historyVal":self.tenSecondseHistoricalYvaluesArray] as [String : Any]
      let tenMinutesHistoryDict = ["xAxis":self.tenSecondsXAxisDateArray,"xAxisDateTime":self.tenSecondsxAxisDateTimeArray,"historyVal":self.tenSecondseHistoricalYvaluesArray] as [String : Any]
      let fiveSecondsHistoryDict = ["xAxis":self.fiveSecondsXAxisDateArray,"xAxisDateTime":self.fiveSecondsxAxisDateTimeArray,"historyVal":self.fiveSecondseHistoricalYvaluesArray] as [String : Any]
      
      
      let totalObject = ["Seconds":secondsHistoryDict,"Minutes":minuteHistoryDict,"Hours":hoursHistoryDict,"tenSeconds":tenSecondsHistoryDict,"tenMinutes":tenMinutesHistoryDict,"fiveSeconds":fiveSecondsHistoryDict]
      
      let currentIdentifer = currentIdentifer //currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
      UserDefaults.standard.setValue(totalObject, forKey: "\(currentIdentifer)-\(self.getCurrentMarketDate().currentDateStr)-dataHistory" )
      UserDefaults.standard.synchronize()
    }
    
  }
  
  
  /// Save the history objects in singleton shared instance while switch the other symbols
  ///
  /// - Parameter key: Current selected identifier(Symbol)
  func saveHistoryInSingleton(key : String) -> Void {
    
    if chartView.data?.dataSets[0].entryCount == nil {
      return
    }
    
    
    if (chartView.data?.dataSets[0].entryCount)! < 1{
      return
    }
    HistoryFile.shared.secondXAxis["\(key)"] = self.secondXAxisDateArray
    HistoryFile.shared.secondHistory["\(key)"] = self.secondHistoricalYvaluesArray
    HistoryFile.shared.secondXAxisDateTime["\(key)"] = self.secondxAxisDateTimeArray
    
    HistoryFile.shared.minuteXAxis["\(key)"] = self.minuteXAxisDateArray
    HistoryFile.shared.minuteHistory["\(key)"] = self.minuteHistoricalYvaluesArray
    HistoryFile.shared.minuteXAxisDateTime["\(key)"] = self.minutexAxisDateTimeArray
    
    HistoryFile.shared.hourXAxis["\(key)"] = self.hoursXAxisDateArray
    HistoryFile.shared.hourHistory["\(key)"] = self.hoursHistoricalYvaluesArray
    HistoryFile.shared.hourXAxisDateTime["\(key)"] = self.hoursxAxisDateTimeArray
    
    HistoryFile.shared.tenSecXAxis["\(key)"] = self.tenSecondsXAxisDateArray
    HistoryFile.shared.tenSecHistory["\(key)"] = self.tenSecondseHistoricalYvaluesArray
    HistoryFile.shared.tenSecXAxisDateTime["\(key)"] = self.tenSecondsxAxisDateTimeArray
    
    HistoryFile.shared.tenMinXAxis["\(key)"] = self.tenMinutesXAxisDateArray
    HistoryFile.shared.tenMinHistory["\(key)"] = self.tenMinutesHistoricalYvaluesArray
    HistoryFile.shared.tenMinXAxisDateTime["\(key)"] = self.tenMinutesxAxisDateTimeArray
    
    HistoryFile.shared.fiveSecXAxis["\(key)"] = self.fiveSecondsXAxisDateArray
    HistoryFile.shared.fiveSecHistory["\(key)"] = self.fiveSecondseHistoricalYvaluesArray
    HistoryFile.shared.fiveSecXAxisDateTime["\(key)"] = self.fiveSecondsxAxisDateTimeArray
    
  }
  
  
  /// Fetch the data from the singleton shared instance class
  func fetchFromSingleton() -> Void {
    
    let key = currentDocumentsDict["PRODUCT"] as! String
    self.clearAllArrays()
    if HistoryFile.shared.secondXAxis["\(key)"] != nil {
      self.secondXAxisDateArray = HistoryFile.shared.secondXAxis["\(key)"] as! [String]
      self.secondHistoricalYvaluesArray = HistoryFile.shared.secondHistory["\(key)"] as! [Double]
      self.secondxAxisDateTimeArray = HistoryFile.shared.secondXAxisDateTime["\(key)"] as! [Date]
      
      self.minuteXAxisDateArray = HistoryFile.shared.minuteXAxis["\(key)"] as! [String]
      self.minuteHistoricalYvaluesArray = HistoryFile.shared.minuteHistory["\(key)"] as! [Double]
      self.minutexAxisDateTimeArray = HistoryFile.shared.minuteXAxisDateTime["\(key)"] as! [Date]
      
      self.hoursXAxisDateArray = HistoryFile.shared.hourXAxis["\(key)"] as! [String]
      self.hoursHistoricalYvaluesArray = HistoryFile.shared.hourHistory["\(key)"] as! [Double]
      self.hoursxAxisDateTimeArray = HistoryFile.shared.hourXAxisDateTime["\(key)"] as! [Date]
      
      self.tenSecondsXAxisDateArray = HistoryFile.shared.tenSecXAxis["\(key)"] as! [String]
      self.tenSecondseHistoricalYvaluesArray = HistoryFile.shared.tenSecHistory["\(key)"] as! [Double]
      self.tenSecondsxAxisDateTimeArray = HistoryFile.shared.tenSecXAxisDateTime["\(key)"] as! [Date]
      
      
      self.tenMinutesXAxisDateArray = HistoryFile.shared.tenMinXAxis["\(key)"] as! [String]
      self.tenMinutesHistoricalYvaluesArray = HistoryFile.shared.tenMinHistory["\(key)"] as! [Double]
      self.tenMinutesxAxisDateTimeArray = HistoryFile.shared.tenMinXAxisDateTime["\(key)"] as! [Date]
      
      self.fiveSecondsXAxisDateArray = HistoryFile.shared.fiveSecXAxis["\(key)"] as! [String]
      self.fiveSecondseHistoricalYvaluesArray = HistoryFile.shared.fiveSecHistory["\(key)"] as! [Double]
      self.fiveSecondsxAxisDateTimeArray = HistoryFile.shared.fiveSecXAxisDateTime["\(key)"] as! [Date]
      
      if isMarketTimeClosed {
        UI {
          self.reloadChartViewUpdate()
          self.reloadBottomScrollView()
          self.chartView.noDataText = ""
          self.chartView.reloadInputViews()
          self.performSelector(onMainThread: #selector(self.hideActivityIndicator), with: nil, waitUntilDone: true)
        }
      }
      else{
        //        self.downloadHistory(isExistingSymbol: true)
        self.performSelector(onMainThread: #selector(self.hideActivityIndicator), with: nil, waitUntilDone: true)
      }
    }else{
      //      self.downloadHistory(isExistingSymbol: false)
    }
  }
  
  ///Check current date is market date. If date is in holiday return last market opened date
  func getCurrentMarketDate() -> Date {
    var lastMarketOpenDate = Date()
    let result = lastMarketOpenDate.compare(lastMarketOpenDate.marketOpenTime)
    
    if result == .orderedAscending || result == .orderedSame{
      lastMarketOpenDate = lastMarketOpenDate.yesterday
    }
    else{
      lastMarketOpenDate = Date()
    }
    
    if lastMarketOpenDate.weekday == 1  {
      lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -2)
    }
    else if lastMarketOpenDate.weekday == 7{
      lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -1)
    }
    
    if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
      lastMarketOpenDate = lastMarketOpenDate.yesterday
      if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
        lastMarketOpenDate = lastMarketOpenDate.yesterday
      }
    }
    return lastMarketOpenDate
  }
  
  @IBAction func switchTheme (_ sender : UIButton) {
    
    self.gradientTimer.invalidate()
    self.dataFeedTimer.invalidate()
    self.notSureGradientTimer.invalidate()
    appThemeUpdating = true
    
    if switchBtn.isSelected == true {
      switchBtn.isSelected = false
      // galaxy theme selected
      theme = .galaxy
      
      self.topImageView.image = UIImage.init(named: AppTheme.Galaxy.gradientTopImage)
      self.bottomImageView.image = UIImage.init(named: AppTheme.Galaxy.gradientBottomImage)
      
      callLabel.textColor = AppTheme.Galaxy.gradientDownColor
      putLabel.textColor = AppTheme.Galaxy.gradientUpColor
      topPercentlbl.textColor = AppTheme.Galaxy.percentTopTextColor
      botPercentLbl.textColor = AppTheme.Galaxy.percentBottomTextColor
      
      bgImageView.image = UIImage.init(named: AppTheme.Galaxy.bgImage)
      notSureRightBG.image = UIImage.init(named: AppTheme.Galaxy.rightBGImage)
      tradeViewRightBG.image = UIImage.init(named: AppTheme.Galaxy.rightBGImage)
      tradeAlertViewRightBG.image = UIImage.init(named: AppTheme.Galaxy.rightBGImage)
      takeProfitViewRightBG.image = UIImage.init(named: AppTheme.Galaxy.rightBGImage)
      popupViewRightBG.image = UIImage.init(named: AppTheme.Galaxy.rightBGImage)
      takeProfitAlertViewRightBG.image = UIImage.init(named: AppTheme.Galaxy.rightBGImage)
      
      
      if currentAmountValue <= 0 {
        amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
      }
      else {
        amountDecrementBtn.setTitleColor(AppTheme.Galaxy.amountButtonColor, for: .normal)
        amountDecrementBtn.layer.borderColor = AppTheme.Galaxy.amountButtonColor.cgColor
      }
      
      amountIncrementBtn.setTitleColor(AppTheme.Galaxy.amountButtonColor, for: .normal)
      amountIncrementBtn.layer.borderColor = AppTheme.Galaxy.amountButtonColor.cgColor
      
      
      self.amountLabel.textColor = AppTheme.Galaxy.amountValueColor
      self.amountValuelbl.textColor = AppTheme.Galaxy.amountValueColor
      //      self.topPercentlbl.textColor = AppTheme.Galaxy.gradientUpColor
      //      self.botPercentLbl.textColor = AppTheme.Galaxy.gradientDownColor
      self.dateLbl.textColor = AppTheme.Galaxy.textColor
      
      self.separatorView.backgroundColor = AppTheme.Galaxy.separatorView
      self.tabViewSeparatorLbl.backgroundColor = AppTheme.Galaxy.tabViewSeparatorColor
      notSureBtn.setTitleColor(AppTheme.Galaxy.notSureColor, for: .normal)
      notSureBtn.layer.borderColor = AppTheme.Galaxy.notSureColor.cgColor
      
      menuBtn.setImage(UIImage.init(named: AppTheme.Galaxy.menuButtonImage), for: .normal)
      
      upDirBtn.setImage(UIImage.init(named: AppTheme.Galaxy.upDirectionNormalBGImage), for: .normal)
      upDirBtn.setImage(UIImage.init(named: AppTheme.Galaxy.upDirectionSelecgedBGImage), for: .selected)
      downDirBtn.setImage(UIImage.init(named: AppTheme.Galaxy.downDirectionNormalBGImage), for: .normal)
      downDirBtn.setImage(UIImage.init(named: AppTheme.Galaxy.downDirectionSelectedBGImage), for: .selected)
      
      //      tradeBtn.setTitleColor(AppTheme.Galaxy.actionButtonDisabledTextColor, for: .disabled)
      //      tradeBtn.setTitleColor(AppTheme.Galaxy.actionButtonNormalTextColor, for: .normal)
      //      tradeBtn.setTitleColor(AppTheme.Galaxy.actionButtonNormalTextColor, for: .selected)
      //      tradeBtn.setBackgroundColor(color: AppTheme.Galaxy.tradeNowButtonDisabledBGColor, forState: .disabled)
      //      tradeBtn.setBackgroundColor(color: AppTheme.Galaxy.actionButtonSelectedBGColor, forState: .normal)
      //      tradeBtn.setBackgroundColor(color: AppTheme.Galaxy.tradeNowButtonSelectedBGColor, forState: .selected)
      
      tradeAgainBtn.setTitleColor(AppTheme.Galaxy.actionButtonDisabledTextColor, for: .disabled)
      tradeAgainBtn.setTitleColor(AppTheme.Galaxy.actionButtonNormalTextColor, for: .normal)
      tradeAgainBtn.setTitleColor(AppTheme.Galaxy.actionButtonNormalTextColor, for: .selected)
      tradeAgainBtn.setBackgroundColor(color: AppTheme.Galaxy.actionButtonDisabledBGColor, forState: .disabled)
      tradeAgainBtn.setBackgroundColor(color: AppTheme.Galaxy.actionButtonSelectedBGColor, forState: .normal)
      tradeAgainBtn.setBackgroundColor(color: AppTheme.Galaxy.actionButtonSelectedBGColor, forState: .selected)
      
      pointView.backgroundColor = AppTheme.Galaxy.pointViewBGColor
      
      flagView.backgroundColor = AppTheme.Galaxy.flagBGColor
      flagImageView.image = UIImage.init(named: AppTheme.Galaxy.flagImage)
      
      investedValuelbl.textColor = AppTheme.Galaxy.investAmountValueColor
      totalValuelbl.textColor = AppTheme.Galaxy.totalAmountTextColor
      
      self.chartView.xAxis.labelTextColor = AppTheme.Galaxy.chartAxisTextColor
      self.chartView.leftAxis.labelTextColor = AppTheme.Galaxy.chartAxisTextColor
      
      takeProfitSuccessLabel.textColor = AppTheme.Galaxy.textColor
      
      futuresBtn.setTitleColor(AppTheme.Galaxy.textColor, for: .normal)
      optionsBtn.setTitleColor(AppTheme.Galaxy.textColor, for: .normal)
      
      filterTitleLabel.textColor = AppTheme.Galaxy.textColor
      
      tradeExcuteLabel.textColor = AppTheme.Galaxy.textColor
      
      adviseMeBtn.setImage(UIImage.init(named: AppTheme.Galaxy.adviceMeImage), for: .normal)
      flipCoinBtn.setImage(UIImage.init(named: AppTheme.Galaxy.flipACoinImage), for: .normal)
      followCrowdBtn.setImage(UIImage.init(named: AppTheme.Galaxy.followCrowdImage), for: .normal)
      notsureTitleLabel.textColor = AppTheme.Galaxy.notSureTitleTextColor
      adviseMeTitleLabel.textColor = AppTheme.Galaxy.notSureTextColor
      flipACoinTitleLabel.textColor = AppTheme.Galaxy.notSureTextColor
      followTheCrowdTitleLabel.textColor = AppTheme.Galaxy.notSureTextColor
      cancelBtn.setImage(UIImage.init(named: AppTheme.Galaxy.notSureCancelImage), for: .normal)
      
      for view in buttonsScrollView.subviews {
        if view .isKind(of: UIButton.self) {
          let btn = view as! UIButton
          btn.setTitleColor(AppTheme.Galaxy.segmentNormalTextColor, for: .normal)
          btn.setTitleColor(AppTheme.Galaxy.segmentSelectedTextColor, for: .selected)
          btn.setBackgroundImage(UIImage.init(named: AppTheme.Galaxy.segmentSelectedImage), for: .selected)
        }
      }
      
      let selCollectionViewCount = selCollectionView.numberOfItems(inSection: 0)
      if selCollectionViewCount > 0 {
        for i in 0...selCollectionViewCount-1 {
          let myCell = selCollectionView.cellForItem(at: IndexPath.init(row: i, section: 0)) as! TabCollectionViewCell
          myCell.tabTitleLbl.textColor = AppTheme.Galaxy.textColor
        }
      }
      
      if self.upDirBtn.isSelected == true {
        
        self.upDirBtn.layer.borderColor = AppTheme.Galaxy.upDirBGColor.cgColor
        self.upDirBtn.backgroundColor = AppTheme.Galaxy.upDirBGColor
        self.downDirBtn.backgroundColor = UIColor.clear
        
        greenLineView.backgroundColor = AppTheme.Galaxy.upLineColor
        greenLineLabelValue.textColor = AppTheme.Galaxy.upLineColor
        
        redLineView.backgroundColor = AppTheme.Galaxy.downLineColor
        redLineLabelValue.textColor = AppTheme.Galaxy.downLineColor
      }
      else if self.downDirBtn.isSelected == true {
        
        self.downDirBtn.layer.borderColor = AppTheme.Galaxy.downDirBGColor.cgColor
        self.downDirBtn.backgroundColor = AppTheme.Galaxy.downDirBGColor
        self.upDirBtn.backgroundColor = UIColor.clear
        
        greenLineView.backgroundColor = AppTheme.Galaxy.downLineColor
        greenLineLabelValue.textColor = AppTheme.Galaxy.downLineColor
        
        redLineView.backgroundColor = AppTheme.Galaxy.upLineColor
        redLineLabelValue.textColor = AppTheme.Galaxy.upLineColor
      }
      
      self.takeProfitRedBG.image = UIImage.init(named: AppTheme.Galaxy.fireworkRedBG)
      self.takeProfitRedMini.image = UIImage.init(named: AppTheme.Galaxy.fireworkRedMiniBG)
      self.takeProfitBlueBG.image = UIImage.init(named: AppTheme.Galaxy.fireworkBlueBG)
      self.takeProfitYellowBG.image = UIImage.init(named: AppTheme.Galaxy.fireworkYelloBG)
      self.takeProfitRedMini.isHidden = false
      
      //      if takeProfitBtn.titleLabel?.text != "STOP LOSS" {
      //        self.takeProfitCongratsLabel.backgroundColor = UIColor.clear
      //      }
      self.takeProfitCongratsLabel.backgroundColor = UIColor.clear
      
      self.thinLineView.backgroundColor = AppTheme.Galaxy.thinLineBGColor
      
      self.leftMenuView.backgroundColor = AppTheme.Galaxy.leftBGColor
      
    }
    else {
      // white theme selected
      theme = .white
      
      switchBtn.isSelected = true
      
      
      self.topImageView.image = UIImage.init(named: AppTheme.White.gradientTopImage)
      self.bottomImageView.image = UIImage.init(named: AppTheme.White.gradientBottomImage)
      
      topPercentlbl.textColor = AppTheme.White.percentTopTextColor
      botPercentLbl.textColor = AppTheme.White.percentBottomTextColor
      callLabel.textColor = AppTheme.White.gradientDownColor
      putLabel.textColor = AppTheme.White.gradientUpColor
      
      bgImageView.image = UIImage.init(named: AppTheme.White.bgImage)
      notSureRightBG.image = UIImage.init(named: AppTheme.White.rightBGImage)
      tradeViewRightBG.image = UIImage.init(named: AppTheme.White.rightBGImage)
      tradeAlertViewRightBG.image = UIImage.init(named: AppTheme.White.rightBGImage)
      takeProfitViewRightBG.image = UIImage.init(named: AppTheme.White.rightBGImage)
      popupViewRightBG.image = UIImage.init(named: AppTheme.White.rightBGImage)
      takeProfitAlertViewRightBG.image = UIImage.init(named: AppTheme.White.rightBGImage)
      
      if currentAmountValue <= 0 {
        amountDecrementBtn.setTitleColor(UIColor.darkGray, for: .normal)
        amountDecrementBtn.layer.borderColor = UIColor.darkGray.cgColor
      }
      else {
        amountDecrementBtn.setTitleColor(AppTheme.White.amountButtonColor, for: .normal)
        amountDecrementBtn.layer.borderColor = AppTheme.White.amountButtonColor.cgColor
      }
      
      amountIncrementBtn.setTitleColor(AppTheme.White.amountButtonColor, for: .normal)
      amountIncrementBtn.layer.borderColor = AppTheme.White.amountButtonColor.cgColor
      
      upDirBtn.setImage(UIImage.init(named: AppTheme.White.upDirectionNormalBGImage), for: .normal)
      upDirBtn.setImage(UIImage.init(named: AppTheme.White.upDirectionSelecgedBGImage), for: .selected)
      downDirBtn.setImage(UIImage.init(named: AppTheme.White.downDirectionNormalBGImage), for: .normal)
      downDirBtn.setImage(UIImage.init(named: AppTheme.White.downDirectionSelectedBGImage), for: .selected)
      
      //      tradeBtn.setTitleColor(AppTheme.White.actionButtonDisabledTextColor, for: .disabled)
      //      tradeBtn.setTitleColor(AppTheme.White.actionButtonNormalTextColor, for: .normal)
      //      tradeBtn.setTitleColor(AppTheme.White.actionButtonNormalTextColor, for: .selected)
      //      tradeBtn.setBackgroundColor(color: AppTheme.White.tradeNowButtonDisabledBGColor, forState: .disabled)
      //      tradeBtn.setBackgroundColor(color: AppTheme.White.actionButtonSelectedBGColor, forState: .normal)
      //      tradeBtn.setBackgroundColor(color: AppTheme.White.tradeNowButtonSelectedBGColor, forState: .selected)
      
      tradeAgainBtn.setTitleColor(AppTheme.White.actionButtonDisabledTextColor, for: .disabled)
      tradeAgainBtn.setTitleColor(AppTheme.White.actionButtonNormalTextColor, for: .normal)
      tradeAgainBtn.setTitleColor(AppTheme.White.actionButtonNormalTextColor, for: .selected)
      tradeAgainBtn.setBackgroundColor(color: AppTheme.White.actionButtonDisabledBGColor, forState: .disabled)
      tradeAgainBtn.setBackgroundColor(color: AppTheme.White.actionButtonSelectedBGColor, forState: .normal)
      tradeAgainBtn.setBackgroundColor(color: AppTheme.White.actionButtonSelectedBGColor, forState: .selected)
      
      self.amountLabel.textColor = AppTheme.White.accountValueColor
      self.amountValuelbl.textColor = AppTheme.White.accountValueColor
      //      self.topPercentlbl.textColor = AppTheme.White.gradientUpColor
      //      self.botPercentLbl.textColor = AppTheme.White.gradientDownColor
      
      self.dateLbl.textColor = AppTheme.White.textColor
      
      self.separatorView.backgroundColor = AppTheme.White.separatorView
      self.tabViewSeparatorLbl.backgroundColor = AppTheme.White.tabViewSeparatorColor
      notSureBtn.setTitleColor(AppTheme.White.notSureColor, for: .normal)
      notSureBtn.layer.borderColor = AppTheme.White.notSureColor.cgColor
      
      menuBtn.setImage(UIImage.init(named: AppTheme.White.menuButtonImage), for: .normal)
      
      pointView.backgroundColor = AppTheme.White.pointViewBGColor
      
      flagView.backgroundColor = AppTheme.White.flagBGColor
      flagImageView.image = UIImage.init(named: AppTheme.White.flagImage)
      
      investedValuelbl.textColor = AppTheme.White.investAmountValueColor
      totalValuelbl.textColor = AppTheme.White.totalAmountTextColor
      
      self.chartView.xAxis.labelTextColor = AppTheme.White.chartAxisTextColor
      self.chartView.leftAxis.labelTextColor = AppTheme.White.chartAxisTextColor
      
      takeProfitSuccessLabel.textColor = AppTheme.White.textColor
      
      futuresBtn.setTitleColor(AppTheme.White.textColor, for: .normal)
      optionsBtn.setTitleColor(AppTheme.White.textColor, for: .normal)
      
      filterTitleLabel.textColor = AppTheme.White.textColor
      
      tradeExcuteLabel.textColor = AppTheme.White.textColor
      
      adviseMeBtn.setImage(UIImage.init(named: AppTheme.White.adviceMeImage), for: .normal)
      flipCoinBtn.setImage(UIImage.init(named: AppTheme.White.flipACoinImage), for: .normal)
      followCrowdBtn.setImage(UIImage.init(named: AppTheme.White.followCrowdImage), for: .normal)
      notsureTitleLabel.textColor = AppTheme.White.notSureTitleTextColor
      adviseMeTitleLabel.textColor = AppTheme.White.notSureTextColor
      flipACoinTitleLabel.textColor = AppTheme.White.notSureTextColor
      followTheCrowdTitleLabel.textColor = AppTheme.White.notSureTextColor
      cancelBtn.setImage(UIImage.init(named: AppTheme.White.notSureCancelImage), for: .normal)
      
      greenLineView.backgroundColor = AppTheme.White.gradientDownColor
      greenLineView.backgroundColor = AppTheme.White.gradientDownColor
      
      
      if self.upDirBtn.isSelected == true {
        
        self.upDirBtn.layer.borderColor = AppTheme.White.upDirBGColor.cgColor
        self.upDirBtn.backgroundColor = AppTheme.White.upDirBGColor
        self.downDirBtn.backgroundColor = UIColor.clear
        
        greenLineView.backgroundColor = AppTheme.White.upLineColor
        greenLineLabelValue.textColor = AppTheme.White.upLineColor
        
        redLineView.backgroundColor = AppTheme.White.downLineColor
        redLineLabelValue.textColor = AppTheme.White.downLineColor
      }
      else if self.downDirBtn.isSelected == true {
        
        self.downDirBtn.layer.borderColor = AppTheme.White.downDirBGColor.cgColor
        self.downDirBtn.backgroundColor = AppTheme.White.downDirBGColor
        self.upDirBtn.backgroundColor = UIColor.clear
        
        greenLineView.backgroundColor = AppTheme.White.downLineColor
        greenLineLabelValue.textColor = AppTheme.White.downLineColor
        
        redLineView.backgroundColor = AppTheme.White.upLineColor
        redLineLabelValue.textColor = AppTheme.White.upLineColor
      }
      
      self.takeProfitRedBG.image = UIImage.init(named: AppTheme.White.fireworkRedBG)
      self.takeProfitRedMini.image = UIImage.init(named: AppTheme.White.fireworkRedMiniBG)
      self.takeProfitBlueBG.image = UIImage.init(named: AppTheme.White.fireworkBlueBG)
      self.takeProfitYellowBG.image = UIImage.init(named: AppTheme.White.fireworkYelloBG)
      self.takeProfitRedMini.isHidden = true
      
      if takeProfitBtn.titleLabel?.text != "STOP LOSS" {
        self.takeProfitCongratsLabel.backgroundColor = UIColor.white
      }
      
      
      for view in buttonsScrollView.subviews {
        if view .isKind(of: UIButton.self) {
          let btn = view as! UIButton
          btn.setTitleColor(AppTheme.White.segmentNormalTextColor, for: .normal)
          btn.setTitleColor(AppTheme.White.segmentSelectedTextColor, for: .selected)
          btn.setBackgroundImage(UIImage.init(named: AppTheme.White.segmentSelectedImage), for: .selected)
        }
      }
      
      let selCollectionViewCount = selCollectionView.numberOfItems(inSection: 0)
      if selCollectionViewCount > 0 {
        for i in 0...selCollectionViewCount-1 {
          var myCell = selCollectionView.cellForItem(at: IndexPath.init(row: i, section: 0)) as! TabCollectionViewCell
          myCell.tabTitleLbl.textColor = AppTheme.White.textColor
        }
      }
      
      self.takeProfitCongratsLabel.backgroundColor = UIColor.white
      self.thinLineView.backgroundColor = AppTheme.White.thinLineBGColor
      
      self.leftMenuView.backgroundColor = AppTheme.White.leftBGColor
    }
    
    if takeProfitBtn.titleLabel?.text == "STOP LOSS" {
      self.takeProfitCongratsLabel.text = "Tough luck!"
      self.takeProfitRedBG.isHidden = true
      self.takeProfitRedMini.isHidden = true
      self.takeProfitBlueBG.isHidden = true
      self.takeProfitYellowBG.isHidden = true
    }else{
      self.takeProfitCongratsLabel.text = "Congrats!"
      self.takeProfitRedBG.isHidden = false
      self.takeProfitRedMini.isHidden = false
      self.takeProfitBlueBG.isHidden = false
      self.takeProfitYellowBG.isHidden = false
    }
    
    self.showTradeBtn()
    print("bg image frame size \(bgImageView.frame)")
    print("view frame size \(self.view.frame)")
    
    
    self.gradientTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
    
    self.dataFeedTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.animatePoint), userInfo: nil, repeats: true)
    self.notSureGradientTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.updateNotSureSlider), userInfo: nil, repeats: true)
    appThemeUpdating = false
  }
  
  
  func downloadConfig() {
    let urlString =  "\(CommonValues.serverFnOIp)/\(CommonValues.configAPI)"
    NetworkUtilities.downloadConfigFromServer(url: urlString, completionHandler: { (object) in
      
      
      print(object)
      let response = object as? [String : Any]
      guard response!["apiUrls"] != nil else {
        return
      }
      
      // API URLs
      let apiurlDict = response!["apiUrls"] as! [String : Any]
      guard apiurlDict["feedServiceUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.feedServiceURL = apiurlDict["feedServiceUrl"] as! String
      
      guard apiurlDict["calculatePointsUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.calcualtePointsURL = apiurlDict["calculatePointsUrl"] as! String
      
      guard apiurlDict["placeOrderUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.placeOrderURL = apiurlDict["placeOrderUrl"] as! String
      
      guard apiurlDict["orderDetailUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.orderDetailURL = apiurlDict["orderDetailUrl"] as! String
      
      guard apiurlDict["closeOrderUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.closeOrderURL = apiurlDict["closeOrderUrl"] as! String
      
      guard apiurlDict["loginUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.loginURL = apiurlDict["loginUrl"] as! String
      
      guard apiurlDict["balanceUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.getBalanceURL = apiurlDict["balanceUrl"] as! String
      
      
      // App Version
      guard response!["updateAppVersion"] != nil else {
        return
      }
      let appversionDict = response!["updateAppVersion"] as! [String : Any]
      guard appversionDict["appVersion"] != nil else {
        return
      }
      ConfigManager.shared.appVersion.appVersion = Double(appversionDict["appVersion"] as! String)!
      
      guard appversionDict["cache"] != nil else {
        return
      }
      ConfigManager.shared.appVersion.cache = Int(appversionDict["cache"] as! String)!
      
      guard appversionDict["forceUpdate"] != nil else {
        return
      }
      ConfigManager.shared.appVersion.forceUpdate = (appversionDict["forceUpdate"] as! String)
      
      guard appversionDict["maintainanceMessage"] != nil else {
        return
      }
      ConfigManager.shared.appVersion.maintainanceMessage = appversionDict["maintainanceMessage"] as! String
      
      guard response!["exchange"] != nil else {
        return
      }
      let exchangeDict = response!["exchange"] as! [String : Any]
      
      guard exchangeDict["currency"] != nil else {
        return
      }
      ConfigManager.shared.exchange.currency = exchangeDict["currency"] as! String
      
      guard exchangeDict["identifierId"] != nil else {
        return
      }
      ConfigManager.shared.exchange.identifierId = exchangeDict["identifierId"] as! String
      
      self.currentDocumentsDict[CommonValues.instrumentIdentifier] = ConfigManager.shared.exchange.identifierId
      
      guard exchangeDict["name"] != nil else {
        return
      }
      ConfigManager.shared.exchange.name = exchangeDict["name"] as! String
      
      guard exchangeDict["amount"] != nil else {
        return
      }
      ConfigManager.shared.exchange.amount = exchangeDict["amount"] as! [Int]
      
      
      // config download finished.. continue downloading history data
      //self.getDefaultMonthSegment(isCurrentMonth: true)
      self.getHistoryDataFromFnOServerInseconds(isExistingSymbol: false)
      
    })
  }
  
  
  
  
  
}

extension ViewController {
  func getHistoryDataFromFnOServerInseconds(isExistingSymbol : Bool) -> Void{
    
    
    
    // reset trade - clear old values
    
    
    
    // From server
    var fromDate : Double
    var todate : Double
    print("##### getHistoryDataFromFnOServerInseconds process Starts #### ----- \(Date())")
    
    let currentIdentifer = ConfigManager.shared.exchange.identifierId
    
    
    UserDefaults.standard.setValue(nil, forKey: "\(currentIdentifer)-\(self.getCurrentMarketDate().currentDateStr)-dataHistory")
    fromDate = Date().previousFiveMinutes.dateAtStartingofminutes().unixTimestamp //Date().lastDayCloseTime.unixTimestamp
    todate = Date().dateAtStartingofminutes().unixTimestamp // \(NSNumber(value: todate).int64Value)
    
    let currentDate = Date()
    let marketOpenTime = Date().marketOpenTime
    let calender:Calendar = Calendar.current
    let components: DateComponents = calender.dateComponents([.minute], from: marketOpenTime, to: currentDate)
    let intervalMinute = components.minute
    //Fetch last 30 min data
    if intervalMinute! < 30{
      var lastMarketOpenDate = Date().yesterday
      if lastMarketOpenDate.weekday == 1  {
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -2)
      }
      else if lastMarketOpenDate.weekday == 7{
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -1)
      }
      if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
        lastMarketOpenDate = lastMarketOpenDate.yesterday
        if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
          lastMarketOpenDate = lastMarketOpenDate.yesterday
        }
      }
      lastMarketOpenDate = lastMarketOpenDate.dateAt(hours: 15, minutes: 00)
      fromDate = lastMarketOpenDate.dateAtStartingofminutes().unixTimestamp
    }
    
    // Check if curent time is market open or closed.
    if isMarketTimeClosed {
      print("history seconds")
      var lastMarketOpenDate = Date()
      let result = lastMarketOpenDate.compare(lastMarketOpenDate.marketOpenTime)
      
      if result == .orderedAscending || result == .orderedSame{
        lastMarketOpenDate = lastMarketOpenDate.yesterday
      }
      else{
        lastMarketOpenDate = Date()
      }
      // Check current day is sunday or saturday
      if lastMarketOpenDate.weekday == 1  {
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -2)
      }
      else if lastMarketOpenDate.weekday == 7{
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -1)
      }
      // Check current day is public holiday
      if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
        lastMarketOpenDate = lastMarketOpenDate.yesterday
        if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
          lastMarketOpenDate = lastMarketOpenDate.yesterday
        }
      }
      
      fromDate =  lastMarketOpenDate.marketCloseTime.previousFiveMinutes.unixTimestamp  //lastMarketOpenDate.marketOpenTime.unixTimestamp
      todate   =  lastMarketOpenDate.marketCloseTime.unixTimestamp
    }
    
    
    let instrumentToken = currentIdentifer
    let urlString = "\(ConfigManager.shared.apiURL.feedServiceURL)?identifierId=\(instrumentToken)&periodicity=\(CommonValues.tick)&from=\(NSNumber(value: fromDate).int64Value)&to=\(NSNumber(value: todate).int64Value)"
    //history? identifierId =FUTIDX_NIFTY_22FEB2018_XX_0&periodicity=TICK&max=100
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "GET", requestBody: nil, contentType: CommonValues.jsonApplication) { (obj) in
      self.historicalYvaluesArray.removeAll()
      self.xAxisDateArray.removeAll()
      self.xAxisDateTimeArray.removeAll()
      if (obj as? NSDictionary != nil){
        
        guard obj.allKeys != nil else {
          self.getHistoryDataFromFnOServerInseconds(isExistingSymbol: isExistingSymbol)
          return
        }
        guard obj.allKeys.count > 0 else {
          self.getHistoryDataFromFnOServerInseconds(isExistingSymbol: isExistingSymbol)
          return
        }
        let array = (obj as? NSDictionary)?.value(forKey: "tick") as! NSArray
        
        guard array != nil else {
          self.getHistoryDataFromFnOServerInseconds(isExistingSymbol: isExistingSymbol)
          return
        }
        
        if array.count > 0{
          let candleObj = array.subarray(with: NSMakeRange(0, array.count)) as NSArray
          print("response count \(candleObj.count)")
          self.isChartDataAvaible = true
          
          self.lastTimeStamp = (candleObj[0] as! NSDictionary).value(forKey: "lastTradeTime")! as! Double/1000
//          self.lastTimeStamp = (candleObj.lastObject as! NSDictionary).value(forKey: "lastTradeTime")! as! Double/1000
          var remainingObj : NSArray = candleObj
          
          if !self.isMarketTimeClosed && isExistingSymbol == false{
            var last60count = 60
            if self.islast10Obj == true {
              last60count = 10
              self.islast10Obj = false
            }else{
              last60count = 60
            }
            if candleObj.count > last60count {
              let first60Obj : NSArray = candleObj.subarray(with: NSMakeRange(0, last60count)) as NSArray
              remainingObj = candleObj.subarray(with: NSMakeRange(last60count, candleObj.count - last60count)) as NSArray
              self.loadingXAxisDateArray.removeAll()
              self.loadingHistoricalYvaluesArray.removeAll()
              self.loadingXAxisDateTimeArray.removeAll()
              
              
              if self.drawChartTimer != nil {
                self.drawChartTimer?.invalidate()
                self.drawChartTimer = nil
              }
              
              
              for tempObj1 in first60Obj.reversed(){
//              for tempObj1 in first60Obj{
                let tempObj2 = tempObj1 as! NSDictionary
                self.loadingHistoricalYvaluesArray.append(tempObj2.value(forKey: "lastTradePrice")! as! Double)
                let tickDate = Date(timeIntervalSince1970: (tempObj2.value(forKey: "lastTradeTime")! as! Double)/1000)
                self.loadingXAxisDateArray.append(tickDate.xAxisDateStr)
                self.loadingXAxisDateTimeArray.append(tickDate)
              }
            }
          }
          
          
          remainingObj = candleObj as NSArray
          
          for tempObj2 in remainingObj.reversed(){
//          for tempObj2 in remainingObj{
            let tempObj3 = tempObj2 as! NSDictionary
            
            self.secondHistoricalYvaluesArray.append(tempObj3.value(forKey: "lastTradePrice")! as! Double)
            
            let tickDate = Date(timeIntervalSince1970: (tempObj3.value(forKey: "lastTradeTime")! as! Double)/1000)
            self.secondXAxisDateArray.append(tickDate.xAxisDateStr)
            // print("secondXAxisDateArray  \(tickDate)")
            self.secondxAxisDateTimeArray.append(tickDate)
          }
          
          let reversedObj = remainingObj.reversed()
//          let reversedObj = remainingObj
          let sequence = stride(from: 0, to: reversedObj.count , by: 10)
          
          for element in sequence {
            // do stuff
            let tempObj3 = reversedObj[element] as! NSDictionary
            
            self.tenSecondseHistoricalYvaluesArray.append(tempObj3.value(forKey: "lastTradePrice")! as! Double)
            let tickDate = Date(timeIntervalSince1970: (tempObj3.value(forKey: "lastTradeTime")! as! Double)/1000)
            self.tenSecondsXAxisDateArray.append(tickDate.xAxisDateStr)
            self.tenSecondsxAxisDateTimeArray.append(tickDate)
            
          }
          
          
          let sequence12 = stride(from: 0, to: reversedObj.count , by: 2)
          
          for element1 in sequence12 {
            // do stuff
            let tempObj3 = reversedObj[element1] as! NSDictionary
            
            self.fiveSecondseHistoricalYvaluesArray.append(tempObj3.value(forKey: "lastTradePrice")! as! Double)
            let tickDate = Date(timeIntervalSince1970: (tempObj3.value(forKey: "lastTradeTime")! as! Double)/1000)
            self.fiveSecondsXAxisDateArray.append(tickDate.xAxisDateStr)
            self.fiveSecondsxAxisDateTimeArray.append(tickDate)
            
          }
          
          //self.xAxisDateTimeArray = self.secondxAxisDateTimeArray
          print("##### getHistoryDataFromGlobalFeedInseconds process completed ##### --- \(Date())")
          self.isSecondsCompleted = true
          self.processHistoryData(isExistigSymbol: isExistingSymbol)
          
          if isExistingSymbol == false && self.isMarketTimeClosed == false {
            // self.download60minFromFnOServer(toDate: todate ,fromDate: fromDate , isRedownload: false)
          }
          
          // start minutes download
          // self.getFnOFeedInMinutes(isExistingSymbol: false)
          
        }
        else{
          print("##### getHistoryDataFromGlobalFeedInSeconds process Completed - With No data #### ----- \(Date())")
          UI {
            self.noChartDataAvailable()
          }
          return
        }
      }
      else{
        if (obj as! String) == CommonValues.dataUnavalibaleMsg{
          self.getHistoryDataFromFnOServerInseconds(isExistingSymbol: isExistingSymbol)
        }
        else {
          UI{
            self.showAlert(title: "Error", contentText: (obj as! String) , actions: nil)
            self.noChartDataAvailable()
          }
        }
      }
    }
  }
  
  
  func download60minFromFnOServer(toDate : Double , fromDate : Double , isRedownload : Bool) -> Void {
    print("##### getHistoryDataFrom FnO In60seconds process starts ##### --- \(Date())")
    var fromdateStamp : Double
    var toDateStamp :  Double
    if isRedownload == false {
      fromdateStamp = Date(timeIntervalSince1970: fromDate).lastDayCloseTime.dateAtStartingofminutes().unixTimestamp
      toDateStamp = Date(timeIntervalSince1970: toDate).unixTimestamp
      
    }else{
      fromdateStamp = fromDate
      toDateStamp = toDate
    }
    
    
    // less time duration
    let earlyDate = Calendar.current.date(
      byAdding: .hour,
      value: -1,
      to: Date())
    
    fromdateStamp = (earlyDate?.unixTimestamp)!
    
    if isMarketTimeClosed{
      var lastMarketOpenDate = Date().yesterday
      let result = lastMarketOpenDate.compare(lastMarketOpenDate.marketOpenTime)
      
      if result == .orderedAscending || result == .orderedSame{
        lastMarketOpenDate = lastMarketOpenDate.yesterday
      }
      else{
        lastMarketOpenDate = Date()
      }
      
      if lastMarketOpenDate.weekday == 1  {
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -2)
      }
      else if lastMarketOpenDate.weekday == 7{
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -1)
      }
      
      if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
        lastMarketOpenDate = lastMarketOpenDate.yesterday
        if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
          lastMarketOpenDate = lastMarketOpenDate.yesterday
        }
      }else{
        
        
      }
      fromdateStamp =  lastMarketOpenDate.marketOpenTime.unixTimestamp
      //                todate   =  lastMarketOpenDate.marketCloseTime.unixTimestamp
      
      toDateStamp = Date().marketCloseTime.unixTimestamp
    }
    
    let instrumentToken = ConfigManager.shared.exchange.identifierId  //self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
    let urlString = "\(ConfigManager.shared.apiURL.feedServiceURL)?identifierId=\(instrumentToken)&periodicity=\(CommonValues.tick)&from=\(NSNumber(value: fromdateStamp).int64Value)&to=\(NSNumber(value: toDateStamp).int64Value)"
    
    // "\(ConfigManager.shared.apiURL.feedServiceURL)?
    NetworkUtilities.sendAsynchronousRequestToServerWithURLCompletion(actionName: urlString, httpMethod: "GET", requestBody: nil, contentType: CommonValues.jsonApplication) { (obj, url) in
      let token = self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
      if url.absoluteString.range(of: token) != nil {
        print("Current instrument token oprocess")
        self.is60MinProcessing = true
        if (obj as? NSDictionary != nil){
          self.tenSecondsXAxisDateArray.removeAll()
          self.tenSecondsxAxisDateTimeArray.removeAll()
          self.tenSecondseHistoricalYvaluesArray.removeAll()
          
          self.fiveSecondsXAxisDateArray.removeAll()
          self.fiveSecondsxAxisDateTimeArray.removeAll()
          self.fiveSecondseHistoricalYvaluesArray.removeAll()
          
          
          if (obj as? NSDictionary != nil){
            
            guard obj.allKeys != nil else {
              return
            }
            guard obj.allKeys.count > 0 else {
              return
            }
            let array = (obj as? NSDictionary)?.value(forKey: "tick") as! NSArray
            
            guard array != nil else {
              return
            }
            
            self.isChartDataAvaible = true
            let candleObj = (obj as? NSDictionary)?.value(forKey: "tick") as! NSArray
            if candleObj.count > 0{
              let reversedObj = candleObj.reversed()
//              let reversedObj = candleObj
              let sequence = stride(from: 0, to: reversedObj.count , by: 10)
              
              for element in sequence {
                // do stuff
                let tempObj3 = reversedObj[element] as! NSDictionary
                
                self.tenSecondseHistoricalYvaluesArray.append(tempObj3.value(forKey: "lastTradePrice")! as! Double)
                let tickDate = Date(timeIntervalSince1970: (tempObj3.value(forKey: "lastTradeTime")! as! Double)/1000)
                self.tenSecondsXAxisDateArray.append(tickDate.xAxisDateStr)
                self.tenSecondsxAxisDateTimeArray.append(tickDate)
                
              }
              
              let sequence12 = stride(from: 0, to: reversedObj.count , by: 2)
              
              for element1 in sequence12 {
                // do stuff
                let tempObj3 = reversedObj[element1] as! NSDictionary
                
                self.fiveSecondseHistoricalYvaluesArray.append(tempObj3.value(forKey: "lastTradePrice")! as! Double)
                let tickDate = Date(timeIntervalSince1970: (tempObj3.value(forKey: "lastTradeTime")! as! Double)/1000)
                self.fiveSecondsXAxisDateArray.append(tickDate.xAxisDateStr)
                self.fiveSecondsxAxisDateTimeArray.append(tickDate)
                
              }
              
              print("##### getHistoryDataFromGlobalFeedIn60seconds process completed ##### --- \(Date())")
              self.process60MinHistoryDataInAsync()
            }
          }
        }else{
          if (obj as! String) == CommonValues.authRequestReceivedMsg{
            self.download60minFromFnOServer(toDate: toDateStamp, fromDate: fromdateStamp, isRedownload: true)
          }  else if (obj as! String) == CommonValues.dataUnavalibaleMsg{
            self.download60minFromFnOServer(toDate: toDateStamp, fromDate: fromdateStamp, isRedownload: true)
          }
          else{
            UI{
              self.showAlert(title: "Error", contentText: (obj as! String) , actions: nil)
              self.noChartDataAvailable()
            }
          }
        }
      }else{
        print("Some other segment")
      }
    }
  }
  
  func getFnOFeedInMinutes(isExistingSymbol:Bool) -> Void
  {
    var fromDate : Double
    var todate : Double
    
    self.minuteHistoricalYvaluesArray.removeAll()
    self.minuteXAxisDateArray.removeAll()
    
    
    
    fromDate = Date().last28Days.marketOpenTime.dateAtStartingofminutes().unixTimestamp //Date().previousMonth.unixTimestamp
    if isMarketTimeClosed {
      var lastMarketOpenDate = Date()
      let result = lastMarketOpenDate.compare(lastMarketOpenDate.marketOpenTime)
      
      if result == .orderedAscending || result == .orderedSame{
        lastMarketOpenDate = lastMarketOpenDate.yesterday
      }
      else{
        lastMarketOpenDate = Date()
      }
      
      if lastMarketOpenDate.weekday == 1  {
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -2)
      }
      else if lastMarketOpenDate.weekday == 7{
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -1)
      }
      
      if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
        lastMarketOpenDate = lastMarketOpenDate.yesterday
        if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
          lastMarketOpenDate = lastMarketOpenDate.yesterday
        }
      }
      
      todate   =  lastMarketOpenDate.marketCloseTime.unixTimestamp
    }
    else{
      todate = Date().unixTimestamp // \(NSNumber(value: todate).int64Value)
    }
    
    print("##### getHistoryDataFrom FnO FeedInMinutes process Starts #### ----- \(Date())")
    
    let instrumentToken = ConfigManager.shared.exchange.identifierId
    
    
    var urlString = "\(ConfigManager.shared.apiURL.feedServiceURL)?identifierId=\(instrumentToken)&periodicity=\(CommonValues.minute)&from=\(NSNumber(value: fromDate).int64Value)&to=\(NSNumber(value: todate).int64Value)"
    
    
    //let urlString = "\(CommonValues.serverIp)/\(CommonValues.gethistory)/?accessKey=\(CommonValues.accessKey)&xml=false&exchange=NFO&instrumentIdentifier=\(instrumentToken)&periodicity=\(CommonValues.minute)&from=\(NSNumber(value: fromDate).int64Value)&to=\(NSNumber(value: todate).int64Value)"
    //http://34.212.33.97:8080/history?identifierId=FUTIDX_NIFTY_28MAR2018_XX_0&periodicity=TICK&from=1519628280&to=1519630080
    
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "GET", requestBody: nil, contentType: CommonValues.jsonApplication) { (obj) in
      if (obj as? NSDictionary != nil){
        
        guard obj.allKeys != nil else {
          self.getFnOFeedInMinutes(isExistingSymbol: isExistingSymbol)
          return
        }
        guard obj.allKeys.count > 0 else {
          self.getFnOFeedInMinutes(isExistingSymbol: isExistingSymbol)
          return
        }
        let array = (obj as? NSDictionary)?.value(forKey: "tick") as! NSArray
        
        guard array != nil else {
          self.getFnOFeedInMinutes(isExistingSymbol: isExistingSymbol)
          return
        }
        
        let candleObj = (obj as? NSDictionary)?.value(forKey: "tick") as! NSArray
        if candleObj.count > 0 {
          self.isChartDataAvaible = true
          
          for tempObj in candleObj.reversed(){
//          for tempObj in candleObj {
            
            let tempObj1 = tempObj as! NSDictionary
            
            self.minuteHistoricalYvaluesArray.append(tempObj1.value(forKey: "lastTradePrice")! as! Double)
            let tickDate = Date(timeIntervalSince1970: (tempObj1.value(forKey: "lastTradeTime")! as! Double)/1000)
            self.minuteXAxisDateArray.append(tickDate.xAxisDateStr)
            self.minutexAxisDateTimeArray.append(tickDate)
            // print("minutes date \(tickDate)")
            
          }
          
          print("minutes array cont \(self.minuteXAxisDateArray.count)")
          let tenminObject = candleObj.reversed()
//          let tenminObject = candleObj
          let sequence = stride(from: 0, to: tenminObject.count , by: 10)
          
          for element in sequence {
            // do stuff
            let tempObj3 = tenminObject[element] as! NSDictionary
            
            self.tenMinutesHistoricalYvaluesArray.append(tempObj3.value(forKey: "lastTradePrice")! as! Double)
            let tickDate = Date(timeIntervalSince1970: (tempObj3.value(forKey: "lastTradeTime")! as! Double)/1000)
            self.tenMinutesXAxisDateArray.append(tickDate.xAxisDateStr)
            self.tenMinutesxAxisDateTimeArray.append(tickDate)
            
          }
          print("ten minutes array cont \(self.tenMinutesXAxisDateArray.count)")
        }
        else{
          print("##### getHistoryDataFrom FnO FeedInMinutes process Completed - With No data #### ----- \(Date())")
          
          self.getFnOFeedInMinutes(isExistingSymbol: isExistingSymbol)
          
          return
        }
        print("##### getHistoryDataFrom FnO FeedInMinutes process Completed #### ----- \(Date())")
        
        if self.isSecondsCompleted == true{
          self.minitesProcessDataInAsnyc()
        }
        
        // download hour feed in background
        self.getFnOFeedInHour(isExistingSymbol: false)
        
      }
      else{
        self.getFnOFeedInMinutes(isExistingSymbol: isExistingSymbol)
        
        return
      }
    }
  }
  
  func getFnOFeedInHour(isExistingSymbol : Bool) -> Void{
    print("##### getFnOFeedInHour process Starts #### ----- \(Date())")
    
    let fromDate = Date().previousMonth.unixTimestamp
    //let fromDate = Date().last7Days.unixTimestamp
    var todate = Date().unixTimestamp
    if isMarketTimeClosed {
      var lastMarketOpenDate = Date()
      let result = lastMarketOpenDate.compare(lastMarketOpenDate.marketOpenTime)
      
      if result == .orderedAscending || result == .orderedSame{
        lastMarketOpenDate = lastMarketOpenDate.yesterday
      }
      else{
        lastMarketOpenDate = Date()
      }
      
      if lastMarketOpenDate.weekday == 1  {
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -2)
      }
      else if lastMarketOpenDate.weekday == 7{
        lastMarketOpenDate = lastMarketOpenDate.adding(.day, value: -1)
      }
      
      if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
        lastMarketOpenDate = lastMarketOpenDate.yesterday
        if lastMarketOpenDate.dateAtOnlyMonthandday().isTodayHoliday {
          lastMarketOpenDate = lastMarketOpenDate.yesterday
        }
      }
      todate   =  lastMarketOpenDate.marketCloseTime.unixTimestamp
    }
    
    //let instrumentToken = self.currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
    
    let instrumentToken = ConfigManager.shared.exchange.identifierId
    var urlString = "\(ConfigManager.shared.apiURL.feedServiceURL)?identifierId=\(instrumentToken)&periodicity=\(CommonValues.hour)&from=\(NSNumber(value: fromDate).int64Value)&to=\(NSNumber(value: todate).int64Value)"
    
    
    //let urlString = "\(CommonValues.serverIp)/\(CommonValues.gethistory)/?accessKey=\(CommonValues.accessKey)&xml=false&exchange=NFO&instrumentIdentifier=\(instrumentToken)&periodicity=\(CommonValues.hour)&from=\(NSNumber(value: fromDate).int64Value)&to=\(NSNumber(value: todate).int64Value)"
    
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "GET", requestBody: nil, contentType: CommonValues.jsonApplication) { (obj) in
      if (obj as? NSDictionary != nil){
        self.hoursHistoricalYvaluesArray.removeAll()
        self.hoursXAxisDateArray.removeAll()
        
        if (obj as? NSDictionary != nil){
          
          guard obj.allKeys != nil else {
            self.getFnOFeedInHour(isExistingSymbol: isExistingSymbol)
            return
          }
          guard obj.allKeys.count > 0 else {
            self.getFnOFeedInHour(isExistingSymbol: isExistingSymbol)
            return
          }
          let array = (obj as? NSDictionary)?.value(forKey: "tick") as! NSArray
          
          guard array != nil else {
            self.getFnOFeedInHour(isExistingSymbol: isExistingSymbol)
            return
          }
          let candleObj = (obj as? NSDictionary)?.value(forKey: "tick") as! NSArray
          if candleObj.count > 0 {
            self.isChartDataAvaible = true
//            for tempObj in candleObj.reversed(){   // data is shown wrong.. so removing reevrse
            for tempObj in candleObj {
              let tempObj1 = tempObj as! NSDictionary
              
              self.hoursHistoricalYvaluesArray.append(tempObj1.value(forKey: "lastTradePrice")! as! Double)
              let tickDate = Date(timeIntervalSince1970: (tempObj1.value(forKey: "lastTradeTime")! as! Double)/1000)
              self.hoursXAxisDateArray.append(tickDate.xAxisDateStr)
              self.hoursxAxisDateTimeArray.append(tickDate)
            }
            print("hours array count \(self.hoursXAxisDateArray.count)")
          }
          else{
            print("##### getHistoryDataFrom FnO FeedInMinutes process Completed - With No data #### ----- \(Date())")
            
            self.getFnOFeedInHour(isExistingSymbol: isExistingSymbol)
            
            return
          }
        }
        else{
          print("##### getFnOFeedInHour FnO FeedInHours process Completed - With No data #### ----- \(Date())")
          
          self.getFnOFeedInHour(isExistingSymbol: isExistingSymbol)
          
          //          UI {
          //            self.noChartDataAvailable()
          //          }
          return
        }
        print("##### getFnOFeedInHour process Completed #### ----- \(Date())")
        
        if self.isSecondsCompleted  == true {
          self.hoursProcessDataInAsnyc()
        }
        
        //self.isSecondsCompleted = true
        //self.processHistoryData(isExistigSymbol: isExistingSymbol)
        self.getHistoryDataFromFnOServerInseconds(isExistingSymbol: isExistingSymbol)
        //        UI {
        //          //if self.isMarketTimeClosed {
        //            let button = UIButton.init()
        //            button.tag = 7
        //            self.segmentButtonAction(sender: button)
        //          //}
        
        //        }
      }
      else{
        
        UI{
          self.showAlert(title: "Error", contentText: (obj as! String) , actions: nil)
          self.noChartDataAvailable()
        }
      }
    }
  }
  
  func performCalculatePointsAPI() {
    let urlString = ConfigManager.shared.apiURL.calcualtePointsURL
    //history? identifierId =FUTIDX_NIFTY_22FEB2018_XX_0&periodicity=TICK&max=100
    
    var mainDict = [String : Any]()
    mainDict["deviceInfo"] = self.getDeviceInfo()
    mainDict["userInfo"] = self.getUserInfo()
    mainDict["transactionInfo"] = self.getCalculatePointsTransactionInfo()
    
    var requestDict = [String : Any]()
    requestDict["serviceRequest"] = mainDict
    print("request body \(requestDict)")
    
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "POST", requestBody: requestDict as AnyObject, contentType: CommonValues.jsonApplication) { (obj) in
      if (obj as? NSDictionary != nil){
        print("calculate points response \(obj)")
        guard obj.allKeys != nil else {
          return
        }
        guard obj.allKeys.count > 0 else {
          return
        }
        let serviceResponse = (obj as? NSDictionary)?.value(forKey: "serviceResponse") as! [String : Any]
        
        guard serviceResponse != nil else {
          return
        }
        
        guard serviceResponse["pointsInfo"] != nil else {
          return
        }
        
        let pointsInfo = serviceResponse["pointsInfo"] as! [String : Any]
        
        guard pointsInfo["points"] != nil else {
          return
        }
        
        UserDefaults.standard.set(Double(pointsInfo["points"] as! Double), forKey: "serverCalculatedPoint")
        UserDefaults.standard.synchronize()
        
        
      }
      
    }
  }
  
  func performPlaceOrderAPI() {
    
    //    ConfigManager.shared.orderInfo = OrderInfo()
    //    ConfigManager.shared.orderInfo.tradeInititationPrice = 10349.4
    //    self.lastTradePrice = 10349.4
    //    ConfigManager.shared.orderInfo.status = "open"
    //    ConfigManager.shared.orderInfo.totalPoints = 10345.0
    //
    //    UI{
    //      self.performFnOTrade()
    //    }
    //
    //    return
    
    self.rightActivityLabeltext.text = "Placing order..."
    self.showRightActivityIndicatory()
    
    // reset order values
    ConfigManager.shared.orderInfo = OrderInfo()
    self.performCloseOrder = false
    
    
    
    let urlString = ConfigManager.shared.apiURL.placeOrderURL
    //history? identifierId =FUTIDX_NIFTY_22FEB2018_XX_0&periodicity=TICK&max=100
    
    var mainDict = [String : Any]()
    mainDict["deviceInfo"] = self.getDeviceInfo()
    mainDict["userInfo"] = self.getUserInfo()
    mainDict["transactionInfo"] = self.getPlaceOrderTransactionInfo()
    
    
    
    var requestDict = [String : Any]()
    requestDict["serviceRequest"] = mainDict
    print("request body \(requestDict)")
    
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "POST", requestBody: requestDict as AnyObject, contentType: CommonValues.jsonApplication) { (obj) in
      print("obj \(obj)")
      if (obj as? NSDictionary != nil){
        
        guard obj.allKeys != nil else {
          return
        }
        guard obj.allKeys.count > 0 else {
          return
        }
        
        let serviceResponse = (obj as? NSDictionary)?.value(forKey: "serviceResponse") as! [String : Any]
        
        guard serviceResponse != nil else {
          return
        }
        
        guard serviceResponse["orderInfo"] != nil else {
          return
        }
        
        let orderInfo = serviceResponse["orderInfo"] as! [String : Any]
        
        
        guard orderInfo["orderId"] != nil else {
          return
        }
        
        ConfigManager.shared.orderInfo.orderId = orderInfo["orderId"] as! String
        
        self.performCloseOrder = false
        self.performOrderDetailApi()
        
      }
      
    }
  }
  
  
  func performOrderDetailApi() {
    
    UI {
      if self.performCloseOrder == false {
        self.rightActivityLabeltext.text = "Executing trade"
      }
      else {
        self.rightActivityLabeltext.text = "Updating order details..."
      }
    }
    
    
    let urlString = ConfigManager.shared.apiURL.orderDetailURL
    //history? identifierId =FUTIDX_NIFTY_22FEB2018_XX_0&periodicity=TICK&max=100
    
    var mainDict = [String : Any]()
    mainDict["deviceInfo"] = self.getDeviceInfo()
    mainDict["userInfo"] = self.getUserInfo()
    mainDict["orderInfo"] = self.getOrderDetailInfo()
    
    var requestDict = [String : Any]()
    requestDict["serviceRequest"] = mainDict
    print("request body \(requestDict)")
    
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "POST", requestBody: requestDict as AnyObject, contentType: CommonValues.jsonApplication) { (obj) in
      print("obj \(obj)")
      if (obj as? NSDictionary != nil){
        
        guard obj.allKeys != nil else {
          
          DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
            // your code here
            self.performOrderDetailApi()
          }
          return
        }
        guard obj.allKeys.count > 0 else {
          DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
            // your code here
            self.performOrderDetailApi()
          }
          return
        }
        
        
        
        let serviceResponse = (obj as? NSDictionary)?.value(forKey: "serviceResponse") as! [String : Any]
        
        guard serviceResponse != nil else {
          DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
            // your code here
            self.performOrderDetailApi()
          }
          return
        }
        
        
        if self.performCloseOrder == false {
          let responseInfo = serviceResponse["responseInfo"] as! [String : Any]
          
          guard responseInfo["responseCode"] != nil else {
            DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
              // your code here
              self.performOrderDetailApi()
            }
            return
          }
          
          if (responseInfo["responseCode"] as AnyObject).intValue  == 1 {
            // call order detail again
            DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
              // your code here
              self.performOrderDetailApi()
            }
            return
          }
          else {
            
            guard serviceResponse["orderInfo"] != nil else {
              return
            }
            
            let orderInfo = serviceResponse["orderInfo"] as! [String : Any]
            
            
            guard orderInfo["orderId"] != nil else {
              DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
                // your code here
                self.performOrderDetailApi()
              }
              return
            }
            
            
            
            ConfigManager.shared.orderInfo.tradeInititationPrice = (orderInfo["tradeInititationPrice"]  as AnyObject).doubleValue
            self.lastTradePrice = ConfigManager.shared.orderInfo.tradeInititationPrice
            ConfigManager.shared.orderInfo.status = orderInfo["status"] as! String
            ConfigManager.shared.orderInfo.totalPoints = (orderInfo["totalPoints"] as AnyObject).doubleValue
            
            
            // update the limit lines with new total points value
            UserDefaults.standard.set(ConfigManager.shared.orderInfo.totalPoints, forKey: "serverCalculatedPoint")
            UserDefaults.standard.set(ConfigManager.shared.orderInfo.tradeInititationPrice, forKey: "tradeInititationPrice")
            UserDefaults.standard.synchronize()
            //          }
            //
            UI{
              self.performFnOTrade()
            }
            
          }
          
          
        }
        else {
          // performing close order
          
          guard serviceResponse["orderInfo"] != nil else {
            DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
              // your code here
              self.performOrderDetailApi()
            }
            return
          }
          
          let orderInfo = serviceResponse["orderInfo"] as! [String : Any]
          
          
          guard orderInfo["status"] != nil else {
            DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
              // your code here
              self.performOrderDetailApi()
            }
            return
          }
          
          let status = (orderInfo["status"] as! String ?? "")
          
          if status == "open" {
            DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
              // your code here
              self.performOrderDetailApi()
            }
            return
            // call order detail again
          }
          else if status == "close" {
            // perfrom close order
            guard orderInfo["orderId"] != nil else {
              return
            }
            
            guard orderInfo["profit"] != nil else {
              return
            }
            
            guard orderInfo["loss"] != nil else {
              return
            }
            
            
            ConfigManager.shared.orderInfo.orderId = orderInfo["orderId"] as! String
            ConfigManager.shared.orderInfo.profit = (orderInfo["profit"]  as AnyObject).doubleValue
            ConfigManager.shared.orderInfo.status = orderInfo["status"] as! String
            ConfigManager.shared.orderInfo.loss = (orderInfo["loss"] as AnyObject).doubleValue
            
            
            UI{
              self.performFnOCloseOrder()
            }
            // close order action
            
          }
          else {
            // call order detail again
            DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
              // your code here
              self.performOrderDetailApi()
            }
            return
          }
        }
      }
      else {
        DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
          // your code here
          self.performOrderDetailApi()
        }
      }
      
    }
    
  }
  
  
  func performCloseOrderAPI() {
    
    self.rightActivityLabeltext.text = "Closing order..."
    self.showRightActivityIndicatory()
    
    self.performCloseOrder = true
    
    let urlString = ConfigManager.shared.apiURL.closeOrderURL
    //history? identifierId =FUTIDX_NIFTY_22FEB2018_XX_0&periodicity=TICK&max=100
    
    var mainDict = [String : Any]()
    mainDict["deviceInfo"] = self.getDeviceInfo()
    mainDict["userInfo"] = self.getUserInfo()
    mainDict["transactionInfo"] = self.getCloseOrderTransactionInfo()
    mainDict["orderInfo"] = self.getOrderDetailInfo()
    
    var requestDict = [String : Any]()
    requestDict["serviceRequest"] = mainDict
    print("request body \(requestDict)")
    
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "POST", requestBody: requestDict as AnyObject, contentType: CommonValues.jsonApplication) { (obj) in
      print("obj \(obj)")
      if (obj as? NSDictionary != nil){
        
        guard obj.allKeys != nil else {
          DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
            // your code here
            self.performCloseOrderAPI()
          }
          return
        }
        guard obj.allKeys.count > 0 else {
          DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
            // your code here
            self.performCloseOrderAPI()
          }
          return
        }
        
        let serviceResponse = (obj as? NSDictionary)?.value(forKey: "serviceResponse") as! [String : Any]
        
        guard serviceResponse != nil else {
          DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
            // your code here
            self.performCloseOrderAPI()
          }
          return
        }
        
        guard serviceResponse["orderInfo"] != nil else {
          DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
            // your code here
            self.performCloseOrderAPI()
          }
          return
        }
        
        let orderInfo = serviceResponse["orderInfo"] as! [String : Any]
        
        
        guard orderInfo["orderId"] != nil else {
          DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
            // your code here
            self.performCloseOrderAPI()
          }
          return
        }
        
        self.performOrderDetailApi()
        
      }
      else {
        DispatchQueue.main.asyncAfter(deadline: .now() + self.orderDetailRetryDuration) {
          // your code here
          self.performCloseOrderAPI()
        }
      }
      
    }
  }
  
  
  func performGetBalanceAPI() {
    let urlString = ConfigManager.shared.apiURL.getBalanceURL
    //history? identifierId =FUTIDX_NIFTY_22FEB2018_XX_0&periodicity=TICK&max=100
    
    var mainDict = [String : Any]()
    mainDict["deviceInfo"] = self.getDeviceInfo()
    mainDict["userInfo"] = self.getUserInfo()
    
    
    var requestDict = [String : Any]()
    requestDict["serviceRequest"] = mainDict
    print("request body \(requestDict)")
    
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "POST", requestBody: requestDict as AnyObject, contentType: CommonValues.jsonApplication) { (obj) in
      print("obj \(obj)")
      if (obj as? NSDictionary != nil){
        
        guard obj.allKeys != nil else {
          return
        }
        guard obj.allKeys.count > 0 else {
          return
        }
        
        let serviceResponse = (obj as? NSDictionary)?.value(forKey: "serviceResponse") as! [String : Any]
        
        guard serviceResponse != nil else {
          return
        }
        
        // set user balance amount from server
        
        guard serviceResponse["userInfo"] != nil else {
          return
        }
        
        let userInfo = serviceResponse["userInfo"] as! [String : Any]
        
        guard userInfo["balance"] != nil else {
          return
        }
        
        self.currentDepositAmountValue = (userInfo["balance"] as AnyObject).intValue
        self.depositAmountVlue = self.currentDepositAmountValue
        UserDefaults.standard.set(self.currentDepositAmountValue, forKey: CommonValues.depositFnOAmountValue)
        UserDefaults.standard.synchronize()
        
        UI {
          self.currentDepositAmountValue = UserDefaults.standard.value(forKey: CommonValues.depositFnOAmountValue) as! Int
          self.amountLabel.text = "\(CommonValues.indianRpsUTF)\(self.currentDepositAmountValue.rupeesFormatt())"
        }
        
      }
      
    }
  }
  
  func performLoginAPI() {
    let urlString = ConfigManager.shared.apiURL.loginURL
    //history? identifierId =FUTIDX_NIFTY_22FEB2018_XX_0&periodicity=TICK&max=100
    
    var mainDict = [String : Any]()
    mainDict["deviceInfo"] = self.getDeviceInfo()
    mainDict["userInfo"] = self.getLoginUserInfo()
    
    
    var requestDict = [String : Any]()
    requestDict["serviceRequest"] = mainDict
    print("request body \(requestDict)")
    
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "POST", requestBody: requestDict as AnyObject, contentType: CommonValues.jsonApplication) { (obj) in
      print("obj \(obj)")
      if (obj as? NSDictionary != nil){
        
        guard obj.allKeys != nil else {
          return
        }
        guard obj.allKeys.count > 0 else {
          return
        }
        
        let serviceResponse = (obj as? NSDictionary)?.value(forKey: "serviceResponse") as! [String : Any]
        
        guard serviceResponse != nil else {
          return
        }
        
        guard serviceResponse["orderInfo"] != nil else {
          return
        }
        
        let orderInfo = serviceResponse["orderInfo"] as! [String : Any]
        
        
        guard orderInfo["orderId"] != nil else {
          return
        }
      }
      
    }
  }
  
  func getDeviceInfo() -> [String: Any] {
    var deviceInfo = [String : Any]()
    deviceInfo["ip"] = ""
    deviceInfo["deviceId"] = ""
    deviceInfo["os"] = "ios"
    deviceInfo["geoCode"] = ""
    return deviceInfo
  }
  
  func getUserInfo() -> [String: Any] {
    var userInfo = [String : Any]()
    userInfo["userId"] = ConfigManager.shared.userInfo.userId
    return userInfo
  }
  
  func getLoginUserInfo() -> [String: Any] {
    var userInfo = [String : Any]()
    userInfo["userName"] = ""
    userInfo["Password"] = ""
    return userInfo
  }
  
  func getOrderDetailInfo() -> [String: Any] {
    var userInfo = [String : Any]()
    userInfo["orderId"] = ConfigManager.shared.orderInfo.orderId
    return userInfo
  }
  
  func getCalculatePointsTransactionInfo() -> [String: Any] {
    var transactionInfo = [String : Any]()
    transactionInfo["amount"] = String(currentAmountValue)
    transactionInfo["exchange"] = ConfigManager.shared.exchange.name
    return transactionInfo
  }
  
  func getPlaceOrderTransactionInfo() -> [String: Any] {
    var transactionInfo = [String : Any]()
    transactionInfo["amount"] = String(currentAmountValue)
    transactionInfo["exchange"] = ConfigManager.shared.exchange.name
    if self.upDirBtn.isSelected {
      transactionInfo["tradeType"] = "up"
    }
    else if self.downDirBtn.isSelected {
      transactionInfo["tradeType"] = "down"
    }
    
    return transactionInfo
  }
  
  func getCloseOrderTransactionInfo() -> [String: Any] {
    var transactionInfo = [String : Any]()
    transactionInfo["exchange"] = ConfigManager.shared.exchange.name
    transactionInfo["type"] = "close"
    return transactionInfo
  }
  
  
  
  
  func performFnOTrade () {
    
    self.yellowLineView.isHidden = false
    
    self.isResetTrade = true
    
    self.amountLabel.textAlignment = .right
    UserDefaults.standard.set(true, forKey: "TradePoint")
    UserDefaults.standard.set(true, forKey: "IsTradeBtnEnabled")
    
    UserDefaults.standard.set(self.currentDepositAmountValue, forKey: CommonValues.depositFnOAmountValue)
    
    
    
    //    self.lastTradePrice = 10319.4 //ConfigManager.shared.orderInfo.tradeInititationPrice
    
    UserDefaults.standard.set(self.lastTradePrice, forKey: CommonValues.tradePriceYAxis)
    
    self.tradedAmountLabel.text = "\(CommonValues.indianRpsUTF)\(self.currentAmountValue.rupeesFormatt())"
    self.showTradeAlertView(isShow: true)
    self.perform(#selector(self.showTradeAlertView(isShow:)), with: false, afterDelay: 2.0)
    if self.upDirBtn.isSelected{
      self.takeProfitDirectionBtn.borderColor = UIColor.init(hex: 0x30ab49)
      self.takeProfitDirectionBtn.setImage(UIImage(named:"ArrUp_UnSel"), for: .normal)
      //            self.calculateTradePrice(isUpBtnSelected: true)
      
    }else{
      self.takeProfitDirectionBtn.borderColor = UIColor.init(hex: 0xda4930)
      self.takeProfitDirectionBtn.setImage(UIImage(named:"ArrDown_UnSel"), for: .normal)
      //            self.calculateTradePrice(isUpBtnSelected: false)
    }
    
    
    let key = ConfigManager.shared.exchange.name
    
    //        let key = currentDocumentsDict[CommonValues.instrumentIdentifier] as! String
    tradePoints["\(key)_sec"] = "\(self.secondHistoricalYvaluesArray.count-1)"
    self.minuteHistoricalYvaluesArray[self.minuteHistoricalYvaluesArray.count - 1] = lastTradePrice
    //    if self.hoursHistoricalYvaluesArray.count <= 0 {
    //      self.restartAppLoad()
    //      return
    //    }
    self.hoursHistoricalYvaluesArray[self.hoursHistoricalYvaluesArray.count - 1] = lastTradePrice
    self.tenSecondseHistoricalYvaluesArray[self.tenSecondseHistoricalYvaluesArray.count - 1] = lastTradePrice
    self.tenMinutesHistoricalYvaluesArray[self.tenMinutesHistoricalYvaluesArray.count - 1] = lastTradePrice
    self.fiveSecondseHistoricalYvaluesArray[self.fiveSecondseHistoricalYvaluesArray.count - 1] = lastTradePrice
    
    tradePoints["\(key)_min"] = "\(self.minuteHistoricalYvaluesArray.count-1)"
    tradePoints["\(key)_tenSec"] = "\(self.tenSecondseHistoricalYvaluesArray.count-1)"
    tradePoints["\(key)_hour"] = "\(self.hoursHistoricalYvaluesArray.count-1)"
    tradePoints["\(key)_tenMin"] = "\(self.tenMinutesHistoricalYvaluesArray.count-1)"
    tradePoints["\(key)_fiveSec"] = "\(self.secondHistoricalYvaluesArray.count-1)"
    
    if self.upDirBtn.isSelected {
      self.calculateTradePrice(isUpBtnSelected: true)
    }else{
      self.calculateTradePrice(isUpBtnSelected: false)
    }
    
    
    //        tradeTimer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(tradeTimerClculate(timer:)), userInfo: self.upDirBtn.isSelected, repeats: true)
    
    //##---- SAVE Current dict object with selected dict *****
    if self.currentDepositAmountValue == 10000{
      self.currentDepositAmountValue = 0
    }
    UserDefaults.standard.set(self.currentDepositAmountValue, forKey: CommonValues.depositFnOAmountValue)
    let lastTradedDict = [CommonValues.instrumentIdentifier:self.currentDocumentsDict[CommonValues.instrumentIdentifier] ,"lastTradePrice":lastTradePrice,"upSelected":self.upDirBtn.isSelected,"downselected":self.downDirBtn.isSelected,"currentAmountValue":currentAmountValue,"tradePoints":tradePoints,"Product":ConfigManager.shared.exchange.name]
    UserDefaults.standard.setValue(lastTradedDict, forKey: ConfigManager.shared.exchange.identifierId)
    UserDefaults.standard.synchronize()
    
    self.hideRightActivityIndicatory()
  }
  
  func performFnOCloseOrder () {
    self.yellowLineView.isHidden = true
    
    self.upDirBtn.isSelected  = false
    self.downDirBtn.isSelected = false
    UserDefaults.standard.set(false, forKey: "IsTradeBtnEnabled")
    
    self.showProfitAlertview(isShow: true)
    
    //self.perform(#selector(self.showProfitAlertview(isShow:)), with: false, afterDelay: 2.0)
    
    var key : String = ConfigManager.shared.exchange.name
    
    tradePoints.removeValue(forKey: "\(key)_sec")
    tradePoints.removeValue(forKey: "\(key)_min")
    tradePoints.removeValue(forKey: "\(key)_tenSec")
    tradePoints.removeValue(forKey: "\(key)_hour")
    tradePoints.removeValue(forKey: "\(key)_tenMin")
    tradePoints.removeValue(forKey: "\(key)_fiveSec")
    UserDefaults.standard.set(false, forKey: "TradePoint")
    UserDefaults.standard.set(nil, forKey: CommonValues.tradePriceYAxis)
    UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeNowMarkerPoint)
    UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
    UserDefaults.standard.set(nil, forKey: "serverCalculatedPoint")
    UserDefaults.standard.set(nil, forKey: "tradeInititationPrice")
    
    
    self.redLineView.isHidden = true
    self.greenLineView.isHidden = true
    self.redLineLabelValue.isHidden = true
    self.greenLineLabelValue.isHidden = true
    self.orangeLineView.isHidden = true
    
    self.chartView.notifyDataSetChanged()
    
    
    
    tradeTimer.invalidate()
    UserDefaults.standard.setValue(nil, forKey: ConfigManager.shared.exchange.identifierId)
    
    
    var profitOrLossAmount = 0
    var totalValueText = 0
    if ConfigManager.shared.orderInfo.loss > 0 {
      totalValueText = currentAmountValue - Int(ConfigManager.shared.orderInfo.loss)
    }else if ConfigManager.shared.orderInfo.profit > 0 {
      totalValueText = currentAmountValue + Int(ConfigManager.shared.orderInfo.profit)
    }
    
    self.takeProfitviewAmountLabel.text = "\(CommonValues.indianRpsUTF)\(totalValueText)"
    
    // update balance from server for the logged in user
    self.performGetBalanceAPI()
    
    self.hideRightActivityIndicatory()
    
    // reset amount index
    ConfigManager.shared.exchange.amountIndex = 0
    self.currentAmountValue = 0
    amountValuelbl.text =  "\(CommonValues.indianRpsUTF)\(currentAmountValue.rupeesFormatt())"
  }
  
  func printTimeStampToDate(_ timestamp : Double) {
    print("timestamp \(timestamp) to date \(Date(timeIntervalSince1970: (timestamp/1000)))")
  }
  
  
  @IBAction func btnSpeakerClicked(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
  }
  
  @IBAction func btnChartBarClicked(_ sender: UIButton) {
    
    
    if sender.isSelected == false {
      self.leftChartBarClicked = true
      
      self.view.bringSubview(toFront: self.notsureFollowTheCrowdView)
      self.view.sendSubview(toBack: self.notsureAdviceMeView)
      self.view.sendSubview(toBack: self.notsureFlipACoinView)
      
      notSureGradientTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateNotSureSlider), userInfo: nil, repeats: true)
    }
    else {
      self.leftChartBarClicked = false
      self.view.sendSubview(toBack: self.notsureFollowTheCrowdView)
      self.tradeView.isHidden = false
      self.notSureView.isHidden = true
      self.takeProfitView.isHidden = true
      
      self.notSureGradientTimer.invalidate()
    }
    
    sender.isSelected = !sender.isSelected
    
  }
  
  @IBAction func btnVideoClicked(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
  }
  
  @IBAction func btnHelpClicked(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
  }
  
  
}
