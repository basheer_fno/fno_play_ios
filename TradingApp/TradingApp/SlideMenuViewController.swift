//
//  SlideMenuViewController.swift
//  TradingApp
//
//  Created by ideas2IT-Durai on 12/06/17.
//  Copyright © 2017 ideas2it. All rights reserved.
//

import UIKit
import Foundation
import QuartzCore

class SlideMenuViewController: UIViewController, /*UITableViewDelegate, UITableViewDataSource ,*/ NMOutlineViewDatasource {

    @IBOutlet var outlineView: NMOutlineView!
    @IBOutlet weak var slideMenuTableView: UITableView!
    var datasource: [TreeNode<[String: Any]>]?

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //let menuArray: NSArray = ["Deposit","WithDraw Funds","History","Settings","Support","Rate us"/*,"Logout"*/]
    let menuArray: NSArray = ["TRADING HISTORY","SUPPORT"/*,"Logout"*/]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupExampleDatasource()
//        self.view.backgroundColor = UIColor(patternImage: UIImage.init(named: "Slide_BG")!)

        self.view.backgroundColor = UIColor(patternImage: UIImage.init(named: "New_Settings_BG_White")!)

        
        
        outlineView.datasource = self
//        outlineView.tableView.separatorStyle = .none
        outlineView.tableView.backgroundColor = UIColor.clear//UIColor.init(hex: 0x171E32)
        outlineView.tableView.separatorColor = UIColor.init(hex: 0x546174)
        outlineView.tableView.frame =  CGRect(x: 0, y: 0, width: outlineView.frame.size.width - 150, height: outlineView.frame.size.height)
        outlineView.tableView.separatorInset = UIEdgeInsetsMake(0, 5, 0, 5)
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Example Datasource
    
    func setupExampleDatasource() {
        
        let Balance = TreeNode<[String : Any]>(value: ["name":"BALANCE" , "flag":"1"])  // Parent
        let deposit = TreeNode<[String : Any]>(value: ["name": "Deposit"])
        let withDraw = TreeNode<[String : Any]>(value: ["name": "Withdraw funds"])
        let operationHistory = TreeNode<[String : Any]>(value: ["name": "Operation history"])
        let tradingHistory = TreeNode<[String : Any]>(value: ["name": "Trading history"])

        Balance.addChild(deposit)
        Balance.addChild(withDraw)
        Balance.addChild(operationHistory)
        Balance.addChild(tradingHistory)
        
        
        let Settings = TreeNode<[String : Any]>(value: ["name":"SETTINGS" , "flag":"1"])  // Parent
        let push = TreeNode<[String : Any]>(value: ["name": "Push notification settings"])
        let general = TreeNode<[String : Any]>(value: ["name": "General settings"])
        let reset = TreeNode<[String : Any]>(value: ["name": "Reset"])

        Settings.addChild(general)
        Settings.addChild(reset)
        Settings.addChild(push)
        
//        let logout = TreeNode<[String : Any]>(value: ["name":"Log out"])  // Parent
        
        let support = TreeNode<[String : Any]>(value: ["name":"SUPPORT" , "flag":"1"])  // Parent

        let rateOurApp = TreeNode<[String : Any]>(value: ["name":"Rate our app" , "flag":"1"])  // Parent
        let termsconditions = TreeNode<[String : Any]>(value: ["name":"Terms & conditions" , "flag":"1"])  // Parent

        let tradingHistoryParent = TreeNode<[String : Any]>(value: ["name":"TRADING HISTORY" , "flag":"1"])  // Parent
        //datasource = [Balance, Settings, /*logout,*/ support, rateOurApp, termsconditions]
        datasource = [tradingHistoryParent, support]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK : 
    
    // MARK: NMOutlineView datasource
    
    func outlineView(_ outlineView: NMOutlineView, numberOfChildrenOfCell parentCell: NMOutlineViewCell?) -> Int {
        
        guard let datasource = datasource else {
            return 0
        }
        if let parentNode = parentCell?.value as? TreeNode<[String: Any]> {
            return parentNode.children.count
        } else {
            // Top level items
            return datasource.count
        }
    }
    
    
    func outlineView(_ outlineView: NMOutlineView, childCell index: Int, ofParentAtIndexPath parentIndexPath: IndexPath?) -> NMOutlineViewCell {
        
        guard let datasource = datasource else {
            return NMOutlineViewCell()
        }
        
        if let parentIndexPath = parentIndexPath,
            let rootIndex = parentIndexPath.first {
            
            // Item that has a parent
            let rootNode = datasource[rootIndex]
            if let node = rootNode.nodeAtIndexPath(parentIndexPath) {
                let childNode = node.children[index]
                
                if childNode.children.count > 0 {
                    // Region
                    let cell = outlineView.dequeReusableCell(withIdentifier: "RegionCell", style: .default)
                    cell.textLabel?.font = UIFont.init(name: "Roboto-Regular", size: 12)
                    cell.textLabel?.textColor = .gray
                    cell.backgroundColor = UIColor.clear//UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.00)
                    let region = childNode.value
//                    let resortsCount = childNode.childCount()
                    if let name = region["name"] as? String {
                        cell.textLabel?.text = "\(name)"
                    }
                    
                    cell.value = childNode
                    return cell
                } else {
                    // Resort
                    let cell = outlineView.dequeReusableCell(withIdentifier: "ResortCell", style: .subtitle)
                    cell.textLabel?.font = UIFont.init(name: "Roboto-Regular", size: 12)
                    cell.detailTextLabel?.font = UIFont.init(name: "Roboto-Regular", size: 11)
                    cell.detailTextLabel?.textColor = .gray
                    
                    let resort = childNode.value

                    if let name = resort["name"] as? String {
                        cell.textLabel?.text = name
                        let imageView = UIImageView()
                        imageView.image = UIImage.init(named: "menuSeparator")//UIImage.init(named: "lineBG.png")
                        imageView.frame = CGRect(x: 5, y: cell.contentView.frame.size.height-1, width: cell.contentView.frame.size.width - 130, height: 1)
                        cell.contentView.addSubview(imageView)
                    }
                    if let tracks = resort["tracks"] as? String {
                        cell.detailTextLabel?.text = tracks
                    }
                    
                    
                    cell.value = childNode
                    return cell
                }
            } else {
                print("Error: no child node found")
                return NMOutlineViewCell()
            }
            
        } else {
            // Root level -> Country
            let cell = outlineView.dequeReusableCell(withIdentifier: "CountryCell", style: .default)
            cell.textLabel?.font = UIFont.init(name: "Roboto-Regular", size: 12)
            cell.textLabel?.textColor = .darkGray
            cell.backgroundColor = UIColor.clear//UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.00)
            let node = datasource[index]
            let country = node.value
//            let resortsCount = node.childCount()
            

            if let name = country["name"] as? String {
                if name == "Support1"  || name == "Rate our app"{
                    let cell1 = NMOutlineViewCell(style: .default, reuseIdentifier: "buttonCell")  //outlineView.dequeReusableCell(withIdentifier: "buttonCell", style: .default)
                    let button = UIButton.init(type: .custom)
                    button.setTitle(name, for: .normal)
                    button.frame = CGRect(x: 5, y: 5, width: 190, height: cell.contentView.frame.size.height - 10)
                    button.layer.borderColor = UIColor.init(hex: 0x75839d).cgColor
                    button.layer.borderWidth = 1.0
                    button.layer.cornerRadius = 5.0
                    button.titleLabel?.textColor = UIColor.init(hex: 0x75839d)
                    button.titleLabel?.font = UIFont.init(name: "Roboto-Regular", size: 12)
                    cell1.contentView.addSubview(button)
                    cell1.value = node
                    cell1.selectionStyle = .none
                    outlineView.tableView.separatorColor = UIColor.clear
                    return cell1
                }
                else{
                    
                    if name == "TRADING HISTORY" {
                        cell.imageView?.image = UIImage.init(named: "menuTradingHistory")
                    }
                    else if name == "SUPPORT" {
                        cell.imageView?.image = UIImage.init(named: "menuSupport")
                    }
                    cell.textLabel?.text = "\(name)"
                    cell.textLabel?.textColor = UIColor.init(hex: 0x243694)
                    print("name ==== \(name)")
                    if name == "Log out" || name == "Terms & conditions" {
//                        cell.contentView.addBottomLayer(color:  UIColor.init(hex: 0x75839d))
                        print("Nothing")
                    }else{
                        let imageView = UIImageView()
                        imageView.image = UIImage.init(named: "menuSeparator") //UIImage.init(named: "lineBG.png")
                        imageView.frame = CGRect(x: 5, y: cell.contentView.frame.size.height-1, width: cell.contentView.frame.size.width * 0.7, height: 1)
                        cell.contentView.addSubview(imageView)
                    }
                }
            }
            
           
//            if let flag = country["flag"] as? String {
//                let image = UIImage(named: flag)
//                cell.imageView?.image = image
//            }
            cell.value = node
            
            cell.selectionStyle = .none
            
            cell.backgroundColor = UIColor.red
            return cell
        }
    }
    
    
    func outlineView(_ outlineView: NMOutlineView, isCellExpandable cell: NMOutlineViewCell) -> Bool {
        guard let node = cell.value as? TreeNode<[String: Any]> else {
            return false
        }
        
        if node.children.count > 0 {
            return true
        } else {
            return false
        }
    }

    
    
    func outlineView(_ outlineView: NMOutlineView, didSelectCell cell: NMOutlineViewCell) {
        guard let node = cell.value as? TreeNode<[String: Any]> else {
            return
        }
        
        cell.toggleButtonAction(sender: cell.toggleButton)
        let nodeValue = node.value
        if let name = nodeValue["name"] {
            print("Selected \(name)")
            
            if node.childCount() > 0 {
                if name as! String == "Log out" {
                    self.appDelegate.loadUserLoggedOutUI()
                }else if name as! String == "Reset" {
                    NotificationCenter.default.post(name: Notification.Name("resetTrade"), object: nil)
                }
                else{
                    DispatchQueue.main.async {
                        let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "detailVc") as! DetailViewController
                        self.appDelegate.sideMenuVC.navigationController?.pushViewController(detailsVc, animated: true)
                        
                    }
                }
            }
        }
        
    }
    
    /*
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = slideMenuTableView.dequeueReusableCell(withIdentifier: "slideMenuCell", for: indexPath)
        let title : UILabel = cell.viewWithTag(10) as! UILabel
        title.text = menuArray[indexPath.row] as? String
        cell.selectionStyle = .none

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 6 {
            appDelegate.loadUserLoggedOutUI()
        }
    }*/
}

class TreeNode<T> {
    var value: T
    
    weak var parent: TreeNode<T>?
    var children = [TreeNode<T>]()
    
    init(value: T) {
        self.value = value
    }
    
    func addChild(_ node: TreeNode<T>) {
        children.append(node)
        node.parent = self
    }
    
    
    func nodeAtIndexPath(_ indexPath: IndexPath) -> TreeNode<T>? {
        if indexPath.count == 1 {
            return self
        } else {
            let nextPath = indexPath.dropFirst()
            let childNode = self.children[nextPath.first!]
            return childNode.nodeAtIndexPath(nextPath)
        }
    }
    
    // Counts children recursively
    func childCount() -> Int {
        if self.children.count > 0 {
            var count = 0
            for child in self.children {
                count += child.childCount()
            }
            return count
            
        } else {
            return 1
        }
    }
    
}
