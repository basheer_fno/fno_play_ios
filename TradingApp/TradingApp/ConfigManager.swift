//
//  ConfigManager.swift
//  TradingApp
//
//  Created by Basheer on 26/02/18.
//  Copyright © 2018 FnO. All rights reserved.
//

import UIKit

class Exchange {
  var name : String = ""
  var identifierId : String = ""
  var currency : String = ""
  var amount : [Int] = [Int]()
  var amountIndex = 0
  
  func getAmount () -> Int {
    
    if self.amountIndex >= self.amount.count {
      self.amountIndex = self.amount.count
    }
    if self.amountIndex <= 0 {
      self.amountIndex = 0
    }
    
    var amountValue = 0

    if self.amountIndex > 0  && self.amountIndex <= self.amount.count {
      amountValue = self.amount[self.amountIndex-1]
    }
    
    return amountValue
  }
}

class APIURL {
  var feedServiceURL : String = ""
  var calcualtePointsURL : String = ""
  var placeOrderURL : String = ""
  var orderDetailURL : String = ""
  var closeOrderURL : String = ""
  var getBalanceURL : String = ""
  var loginURL : String = ""
}

class AppConfig {
  var appVersion : Double = 0.0
  var maintainanceMessage : String = ""
  var forceUpdate : String = "no"
  var cache : Int = 0
}

class OrderInfo {
  var orderId : String = ""
  var status : String = ""
  var totalPoints : Double = 0.0
  var tradeInititationPrice : Double = 0.0
  var profit : Double = 0.0
  var loss : Double = 0.0
}

class UserInfo {
  var userId : String = ""
  var userName : String = ""
}

class ConfigManager: NSObject {
  static let shared = ConfigManager()
  
  // when this flag is true, app will connect to FnO API server and load data
  var enableFnOServer : Bool = true
  
  var exchange : Exchange = Exchange()
  var apiURL : APIURL = APIURL()
  var appVersion : AppConfig = AppConfig()
  var orderInfo : OrderInfo = OrderInfo()
  var userInfo : UserInfo = UserInfo()
}
