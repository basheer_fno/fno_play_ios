//
//  HistoryFile.swift
//  TradingApp
//
//  Created by Pandiyaraj on 20/09/17.
//  Copyright © 2017 ideas2it. All rights reserved.
//

import UIKit

class HistoryFile: NSObject {
    
    static let shared = HistoryFile()
    
    var secondXAxis = Dictionary<String,Any>()
    var secondXAxisDateTime = Dictionary<String,Any>()
    var secondHistory = Dictionary<String,Any>()
    
    
    var minuteXAxis = Dictionary<String,Any>()
    var minuteXAxisDateTime = Dictionary<String,Any>()
    var minuteHistory = Dictionary<String,Any>()
    
    
    var hourXAxis = Dictionary<String,Any>()
    var hourXAxisDateTime = Dictionary<String,Any>()
    var hourHistory = Dictionary<String,Any>()
    
    var tenSecXAxis = Dictionary<String,Any>()
    var tenSecXAxisDateTime = Dictionary<String,Any>()
    var tenSecHistory = Dictionary<String,Any>()
    
    var tenMinXAxis = Dictionary<String,Any>()
    var tenMinXAxisDateTime = Dictionary<String,Any>()
    var tenMinHistory = Dictionary<String,Any>()
    
    var fiveSecXAxis = Dictionary<String,Any>()
    var fiveSecXAxisDateTime = Dictionary<String,Any>()
    var fiveSecHistory = Dictionary<String,Any>()

}
