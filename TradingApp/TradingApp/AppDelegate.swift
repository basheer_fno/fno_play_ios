//
//  AppDelegate.swift
//  TradingApp
//
//  Created by Pandiyaraj on 06/06/17.
//  Copyright © 2017 ideas2it. All rights reserved.
//

import UIKit



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let sideMenuVC = SlideMenuParentViewController()

  var orientationLock = UIInterfaceOrientationMask.all
  
  func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
    return self.orientationLock
  }
  
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
//        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
           self.setRootViewController()
//        }
        
        self.clearUserDefaults()
//        TestFairy.begin("8e230488c7afc059781f5b749f31cbe44feafe6a")

      // core data
      
      
        self.loadUserLoggedOutUI()
      
        return true
    }
    
    func setRootViewController() -> Void {
        let mainVcIntial = setIntialMainViewController("mainChartVc")
        self.window?.rootViewController = mainVcIntial
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
      
//      UserDefaults.standard.setValue(ConfigManager.shared.enableFnOServer, forKey: "enableFnOServer")
//      UserDefaults.standard.synchronize()
    }
  

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
      
//      UserDefaults.standard.setValue(ConfigManager.shared.enableFnOServer, forKey: "enableFnOServer")
//      UserDefaults.standard.synchronize()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
      
//      if UserDefaults.standard.value(forKey: "enableFnOServer") != nil {
//        ConfigManager.shared.enableFnOServer = UserDefaults.standard.value(forKey: "enableFnOServer") as! Bool
//      }
//      else {
//        ConfigManager.shared.enableFnOServer = false  // by default
//      }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ConnectWebSocket"), object: nil)
      
//      if UserDefaults.standard.value(forKey: "enableFnOServer") != nil {
//        ConfigManager.shared.enableFnOServer = UserDefaults.standard.value(forKey: "enableFnOServer") as! Bool
//      }
//      else {
//        ConfigManager.shared.enableFnOServer = false  // by default
//      }
      
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
      
//      UserDefaults.standard.setValue(ConfigManager.shared.enableFnOServer, forKey: "enableFnOServer")
//      UserDefaults.standard.synchronize()
      CoreDataStack.sharedInstance.saveContext()
    }

    func loadUserLoggedOutUI() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loggedOut"), object: nil)
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
        UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeDirection)
        UserDefaults.standard.set(0, forKey: CommonValues.depositAmountValue)
        UserDefaults.standard.set(0, forKey: CommonValues.depositFnOAmountValue)
        self.window?.rootViewController?.removeFromParentViewController()
        self.window?.rootViewController = nil
        let loginNav = mainStoryboard.instantiateInitialViewController() as! LoginViewController
        self.window?.rootViewController = loginNav
        self.window?.makeKeyAndVisible()
    }

    func setIntialMainViewController(_ aStoryBoardID: String)->(SlideMenuParentViewController){
        let sideMenuObj = mainStoryboard.instantiateViewController(withIdentifier: "slideMenuVc")
        let mainVcObj = mainStoryboard.instantiateViewController(withIdentifier: aStoryBoardID)
        let navigationController : UINavigationController = UINavigationController(rootViewController: mainVcObj)
        navigationController.isNavigationBarHidden = true
        sideMenuVC.view.frame = UIScreen.main.bounds
        sideMenuVC.mainViewController(navigationController)
        sideMenuVC.menuViewController(sideMenuObj)
        return sideMenuVC
    }
    func setMainViewController(_ aStoryBoardID: String)->(SlideMenuParentViewController){
        let mainVcObj = mainStoryboard.instantiateViewController(withIdentifier: aStoryBoardID)
        let navigationController : UINavigationController = UINavigationController(rootViewController: mainVcObj)
        navigationController.isNavigationBarHidden = true
        sideMenuVC.view.frame = UIScreen.main.bounds
        sideMenuVC.mainViewController(navigationController)
        return sideMenuVC
    }

    func clearUserDefaults () -> Void{
        UserDefaults.standard.set(nil, forKey: "currentScaleValue")
        UserDefaults.standard.set(0, forKey: "lot_size")
        UserDefaults.standard.set(0, forKey: "investedAmount")
        UserDefaults.standard.setValue(nil, forKey: "ActualDatapoint")
        UserDefaults.standard.setValue(nil, forKey: "Datapoint")
        UserDefaults.standard.setValue(nil, forKey: "PreDatapoint")
        UserDefaults.standard.setValue(nil, forKey: "ActualTopLinepoint")
        UserDefaults.standard.setValue(nil, forKey: "TopLinepoint")
        UserDefaults.standard.setValue(nil, forKey: "ActualBottomLinepoint")
        UserDefaults.standard.setValue(nil, forKey: "BottomLinepoint")
        
        UserDefaults.standard.set(false, forKey: "TradePoint")
        UserDefaults.standard.set("", forKey: "TradeTime")
        UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeNowMarkerPoint)
        UserDefaults.standard.setValue(nil, forKey: "olderEntries")
        UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeCGPoint)
        UserDefaults.standard.setValue(nil, forKey: CommonValues.tradeDirection)
        UserDefaults.standard.set(false, forKey: "TradePointChanged")
        UserDefaults.standard.set(0, forKey: CommonValues.depositAmountValue)
        UserDefaults.standard.set(0, forKey: CommonValues.depositFnOAmountValue)
        UserDefaults.standard.synchronize()
    }
    
}

