//
//  InstrumentsInHours+CoreDataProperties.swift
//  
//
//  Created by Basheer on 24/01/18.
//
//

import Foundation
import CoreData


extension InstrumentsInHours {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<InstrumentsInHours> {
        return NSFetchRequest<InstrumentsInHours>(entityName: "InstrumentsInHours")
    }

    @NSManaged public var close: Int64
    @NSManaged public var high: Int64
    @NSManaged public var lasttradetime: Double
    @NSManaged public var low: String?
    @NSManaged public var open: String?
    @NSManaged public var openinterest: Int64
    @NSManaged public var quotationlot: Int64
    @NSManaged public var tradedqty: Int64
  
  

}
