//
//  CoreDataManager.swift
//  TradingApp
//
//  Created by Basheer on 23/01/18.
//  Copyright © 2018 FnO. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager : NSObject {
  
  static let sharedInstance = CoreDataManager()
  private override init() {}
  
  private func createInstrumentsEntityFrom(dictionary: [String: AnyObject]) -> NSManagedObject? {
    
    let context = CoreDataStack.sharedInstance.managedObjectContext
    if let entity = NSEntityDescription.insertNewObject(forEntityName: "Instruments", into: context) as? Instruments {
      
      entity.exchange = dictionary["EXCHANGE"] as? String
      entity.expiry = dictionary["EXPIRY"] as? String
      entity.identifier = dictionary["IDENTIFIER"] as? String
      entity.indexname = dictionary["INDEXNAME"] as? String
      entity.name = dictionary["NAME"] as? String
      entity.optiontype = dictionary["OPTIONTYPE"] as? String
      if dictionary["PRICEQUOTATIONUNIT"] as? String != "" {
        entity.pricequotationunit = (dictionary["PRICEQUOTATIONUNIT"] as? Double)!
      }
      else {
        entity.pricequotationunit = 0.0
      }
      entity.product = dictionary["PRODUCT"] as? String
      entity.productmonth = dictionary["PRODUCTMONTH"] as? String
      
      if dictionary["STRIKEPRICE"] as? String != "" {
        entity.strikeprice = (dictionary["STRIKEPRICE"] as? Double)!
      }
      else {
        entity.strikeprice = 0.0
      }
      
      
      entity.tradesymbol = dictionary["TRADESYMBOL"] as? String
      entity.underlyingasset = dictionary["UNDERLYINGASSET"] as? String
      entity.underlyingassetexpiry = dictionary["UNDERLYINGASSETEXPIRY"] as? String
      
      return entity
    }
    return nil
  }
  
  private func createInstrumentsInSecondsEntityFrom(dictionary: [String: AnyObject]) -> NSManagedObject? {
    
    let context = CoreDataStack.sharedInstance.managedObjectContext
    if let entity = NSEntityDescription.insertNewObject(forEntityName: "InstrumentsInSeconds", into: context) as? InstrumentsInSeconds {
      
      entity.buyprice = dictionary["BUYPRICE"] as? String
      entity.buyqty = (dictionary["BUYQTY"] as? Int64)!
      entity.lasttradeprice = dictionary["LASTTRADEPRICE"] as? String
      entity.lasttradetime = ((dictionary["LASTTRADETIME"] as! Double)/1000)
      entity.quotationlot = (dictionary["QUOTATIONLOT"] as? Int64)!
      entity.sellprice = dictionary["SELLPRICE"] as? String
      entity.sellqty = (dictionary["SELLQTY"] as? Int64)!
      entity.tradedqty = (dictionary["TRADEDQTY"] as? Int64)!
      
      let tickDate = Date(timeIntervalSince1970: entity.lasttradetime)
      entity.xAxisTradeDate = tickDate
      //print("entity date \(tickDate)")
      entity.xAxisTradeTime = tickDate.xAxisDateStr
      
      return entity
    }
    return nil
  }
  
  private func createInstrumentsInMinutesEntityFrom(dictionary: [String: AnyObject]) -> NSManagedObject? {
    
    let context = CoreDataStack.sharedInstance.managedObjectContext
    if let minutesEntity = NSEntityDescription.insertNewObject(forEntityName: "InstrumentsInMinutes", into: context) as? InstrumentsInMinutes {
      minutesEntity.close = (dictionary["CLOSE"] as? Int64)!
      minutesEntity.high = (dictionary["HIGH"] as? Int64)!
      minutesEntity.lasttradetime = ((dictionary["LASTTRADETIME"] as! Double)/1000)
      minutesEntity.openinterest = (dictionary["OPENINTEREST"] as? Int64)!
      minutesEntity.quotationlot = (dictionary["QUOTATIONLOT"] as? Int64)!
      minutesEntity.tradedqty = (dictionary["TRADEDQTY"] as? Int64)!
      minutesEntity.low = dictionary["LOW"] as? String
      minutesEntity.open = dictionary["OPEN"] as? String
      
      return minutesEntity
    }
    return nil
  }
  
  private func createInstrumentsInHoursEntityFrom(dictionary: [String: AnyObject]) -> NSManagedObject? {
    
    let context = CoreDataStack.sharedInstance.managedObjectContext
    if let hoursEntity = NSEntityDescription.insertNewObject(forEntityName: "InstrumentsInHours", into: context) as? InstrumentsInHours {
      hoursEntity.close = (dictionary["CLOSE"] as? Int64)!
      hoursEntity.high = (dictionary["HIGH"] as? Int64)!
      hoursEntity.lasttradetime = ((dictionary["LASTTRADETIME"] as! Double)/1000)
      hoursEntity.openinterest = (dictionary["OPENINTEREST"] as? Int64)!
      hoursEntity.quotationlot = (dictionary["QUOTATIONLOT"] as? Int64)!
      hoursEntity.tradedqty = (dictionary["TRADEDQTY"] as? Int64)!
      hoursEntity.low = dictionary["LOW"] as? String
      hoursEntity.open = dictionary["OPEN"] as? String
      
      return hoursEntity
    }
    return nil
  }
  
  func saveInstrumentsInCoreDataWith(array: [[String: AnyObject]]) {
    _ = array.map{self.createInstrumentsEntityFrom(dictionary: $0)}
    do {
      try CoreDataStack.sharedInstance.managedObjectContext.save()
    } catch let error {
      print(error)
    }
  }
  
  func saveInstrumentsHoursInCoreDataWith(array: [[String: AnyObject]]) {
    _ = array.map{self.createInstrumentsInHoursEntityFrom(dictionary: $0)}
    do {
      try CoreDataStack.sharedInstance.managedObjectContext.save()
    } catch let error {
      print(error)
    }
  }
  
  func saveInstrumentsMinutesInCoreDataWith(array: [[String: AnyObject]]) {
    _ = array.map{self.createInstrumentsInMinutesEntityFrom(dictionary: $0)}
    do {
      try CoreDataStack.sharedInstance.managedObjectContext.save()
    } catch let error {
      print(error)
    }
  }
  
  func saveInstrumentsSecondsInCoreDataWith(array: [[String: AnyObject]]) {
    _ = array.map{self.createInstrumentsInSecondsEntityFrom(dictionary: $0)}
    do {
      try CoreDataStack.sharedInstance.managedObjectContext.save()
    } catch let error {
      print(error)
    }
  }
  
  
  //MARK: Count
  
  func getInstrumentsCount () -> Int {
    do {
      var count = 0
      if #available(iOS 10.0, *) {
        count = try CoreDataStack.sharedInstance.managedObjectContext.count(for: Instruments.fetchRequest())
      } else {
        // Fallback on earlier versions
      }
      return count
    } catch let error as NSError {
      print("Error: \(error.localizedDescription)")
      return 0
    }
  }
  
  func getInstrumentsInHoursCount () -> Int {
    do {
      var count = 0
      if #available(iOS 10.0, *) {
        count = try CoreDataStack.sharedInstance.managedObjectContext.count(for: InstrumentsInHours.fetchRequest())
      } else {
        // Fallback on earlier versions
      }
      return count
    } catch let error as NSError {
      print("Error: \(error.localizedDescription)")
      return 0
    }
  }
  
  func getInstrumentsInMinutesCount () -> Int {
    do {
      var count = 0
      if #available(iOS 10.0, *) {
        count = try CoreDataStack.sharedInstance.managedObjectContext.count(for: InstrumentsInMinutes.fetchRequest())
      } else {
        // Fallback on earlier versions
      }
      return count
    } catch let error as NSError {
      print("Error: \(error.localizedDescription)")
      return 0
    }
  }
  
  func getInstrumentsInSecondsCount () -> Int {
    do {
      var count = 0
      if #available(iOS 10.0, *) {
        count = try CoreDataStack.sharedInstance.managedObjectContext.count(for: InstrumentsInSeconds.fetchRequest())
      } else {
        // Fallback on earlier versions
      }
      return count
    } catch let error as NSError {
      print("Error: \(error.localizedDescription)")
      return 0
    }
  }
  
  //MARK:  Predicate
  
  func getFilteredInstruments(filterString: String) -> [Instruments]{
    
    do {
      let fetchRequest : NSFetchRequest<Instruments> = Instruments.fetchRequest()
      
      let sortDescriptor = NSSortDescriptor(key: "tradesymbol", ascending: true)
      fetchRequest.predicate = NSPredicate(format: "name CONTAINS %@", filterString)
      fetchRequest.sortDescriptors = [sortDescriptor]
      //fetchRequest.resultType = .dictionaryResultType
      fetchRequest.returnsDistinctResults = true
      
      let fetchedResults = try CoreDataStack.sharedInstance.managedObjectContext.fetch(fetchRequest)
      if fetchedResults.count > 0 {
        return fetchedResults
      }
    }
    catch {
      print ("fetch task failed", error)
    }
    
    return [Instruments]()
    
  }
  
  func getCurrentMonthFutureFromInstruments(filterString: String) -> [Instruments]{
    
    do {
      let fetchRequest : NSFetchRequest<Instruments> = Instruments.fetchRequest()
      
      fetchRequest.predicate = NSPredicate(format: "tradesymbol == %@", filterString)
      let fetchedResults = try CoreDataStack.sharedInstance.managedObjectContext.fetch(fetchRequest)
      if fetchedResults.count > 0 {
        return fetchedResults
      }
    }
    catch {
      print ("fetch task failed", error)
    }
    
    return [Instruments]()
    
  }
  
  func getInstrumentsInSecondsValues(fetchLimit: Int) -> [InstrumentsInSeconds] {
    
    do {
      let fetchRequest : NSFetchRequest<InstrumentsInSeconds> = InstrumentsInSeconds.fetchRequest()
      if fetchLimit > 0 {
        fetchRequest.fetchLimit = fetchLimit
      }
      
      let fetchedResults = try CoreDataStack.sharedInstance.managedObjectContext.fetch(fetchRequest)
      print (fetchedResults.count)
      return fetchedResults
    }
    catch {
      print ("fetch task failed", error)
    }
    
    return [InstrumentsInSeconds]()
    
  }
  
  //MARK:  Delete
  
  func clearSecondsInstruments () {
    
    do {
      
      let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "InstrumentsInSeconds")
      let fetchRequest = NSBatchDeleteRequest(fetchRequest: fetch)
      
      try CoreDataStack.sharedInstance.managedObjectContext.execute(fetchRequest)
      
    }
    catch {
      print ("fetch task failed", error)
    }
    
    
    
  }
}
