//
//  Instruments+CoreDataProperties.swift
//  
//
//  Created by Basheer on 24/01/18.
//
//

import Foundation
import CoreData


extension Instruments {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Instruments> {
        return NSFetchRequest<Instruments>(entityName: "Instruments")
    }

    @NSManaged public var exchange: String?
    @NSManaged public var expiry: String?
    @NSManaged public var identifier: String?
    @NSManaged public var indexname: String?
    @NSManaged public var name: String?
    @NSManaged public var optiontype: String?
    @NSManaged public var pricequotationunit: Double
    @NSManaged public var product: String?
    @NSManaged public var productmonth: String?
    @NSManaged public var strikeprice: Double
    @NSManaged public var tradesymbol: String?
    @NSManaged public var underlyingasset: String?
    @NSManaged public var underlyingassetexpiry: String?

}
