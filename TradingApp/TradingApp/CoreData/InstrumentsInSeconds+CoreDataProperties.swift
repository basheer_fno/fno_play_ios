//
//  InstrumentsInSeconds+CoreDataProperties.swift
//  
//
//  Created by Basheer on 24/01/18.
//
//

import Foundation
import CoreData


extension InstrumentsInSeconds {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<InstrumentsInSeconds> {
        return NSFetchRequest<InstrumentsInSeconds>(entityName: "InstrumentsInSeconds")
    }

    @NSManaged public var buyprice: String?
    @NSManaged public var buyqty: Int64
    @NSManaged public var lasttradeprice: String?
    @NSManaged public var lasttradetime: Double
    @NSManaged public var quotationlot: Int64
    @NSManaged public var sellprice: String?
    @NSManaged public var sellqty: Int64
    @NSManaged public var tradedqty: Int64
    @NSManaged public var xAxisTradeTime: String?
    @NSManaged public var xAxisTradeDate: Date

}
