//
//  SharedMethods.swift
//  TradingApp
//
//  Created by Pandiyaraj on 04/10/17.
//  Copyright © 2017 ideas2it. All rights reserved.
//

import UIKit

class SharedMethods: NSObject {
    static let shared = SharedMethods()
    
    static func checkIfFileExists(fileName : String) -> Bool {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent(fileName)?.path
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: filePath!)
    }

    
    static func checkIsInstrumentExpired(existingDownloadedDate : Date) -> Bool{
        //        let downloadedDate = Date() //26
        let downloadedDate = existingDownloadedDate.dateAtOnlyMonthandday()
        let lastThursdayofCurrentMonth = self.getLastThursday(selectedDate: Date()) //28
        
        let currentDate = Date().dateAtOnlyMonthandday() //08
        var isInstrumentReload : Bool = false
        if downloadedDate.currentMonthStr == currentDate.currentMonthStr {
            let result = currentDate.compare(lastThursdayofCurrentMonth)
            if result.rawValue >= 0 {
                // Current date is higher than or equal to expiry date
                
                let result1 = downloadedDate.compare(lastThursdayofCurrentMonth)
                if result1.rawValue < 0 {
                    // downloaded date  is lower than expiry date ... Need to download new data
                    // add date with next month name
                    isInstrumentReload = true
                }else{
                    isInstrumentReload = false
                }// No need
            }else{
                //Current date is lower - No need to download
                isInstrumentReload = false
            }
            
        }else{
            let previousMonthLastThursday = self.getLastThursday(selectedDate: currentDate.previousMonth)
            let result2 = downloadedDate.compare(previousMonthLastThursday)
            if result2.rawValue >= 0 {
                //no need to download
                isInstrumentReload = false
                if Date().compare(lastThursdayofCurrentMonth).rawValue >= 0 {
                    isInstrumentReload = true
                }
            }else{
                //Need to download new data
                isInstrumentReload = true
            }
        }
        return isInstrumentReload
    }
    
    static func getLastThursday(selectedDate : Date) -> Date {
        var incrementVal : Int = 0
        switch selectedDate.lastDayOfMonth().weekday {
        case 1:
            incrementVal = -3
            break
        case 2:
            incrementVal = -4
            break
        case 3:
            incrementVal = -5
            break
        case 4:
            incrementVal = -6
            break
        case 5:
            incrementVal = 0
            break
        case 6:
            incrementVal = -1
            break
        case 7:
            incrementVal = -2
            break
        default:
            break
        }
        return selectedDate.lastDayOfMonth().adding(.day, value: incrementVal).dateAtOnlyMonthandday()
    }
    
    static func isHoliday(date : Date) -> Bool {
        return Date().holidayLists().contains(date.dateAtOnlyMonthandday())
    }
    
}
