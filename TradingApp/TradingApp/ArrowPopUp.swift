//
//  ArrowPopUp.swift
//  ArrowPopUp
//
//  Created by Pandiyaraj on 11/07/17.
//  Copyright © 2017 Pandiyaraj. All rights reserved.
//

import UIKit

class ArrowPopUp: UIView {

    let kArrowHeight : CGFloat = 10
    let labelTextColor = UIColor.init(hex: 0x75839d)
    let labelTextFont = UIFont.init(name: "Roboto-Regular", size: 10)
    let bgColor = UIColor.init(hex: 0x121624)
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        let context : CGContext = UIGraphicsGetCurrentContext()!
        let fillPath = UIBezierPath()
        fillPath.move(to: CGPoint(x: self.bounds.origin.x + kArrowHeight, y: 0))
       
        fillPath.addLine(to: CGPoint(x: kArrowHeight, y: self.bounds.size.height/2-(kArrowHeight/2)))
       
        fillPath.addLine(to: CGPoint(x: 0, y: self.bounds.size.height/2))
      
        fillPath.addLine(to: CGPoint(x: kArrowHeight, y: self.bounds.size.height/2+(kArrowHeight/2)))
       
        fillPath.addLine(to: CGPoint(x: kArrowHeight, y: self.bounds.size.height))
       
        fillPath.addLine(to: CGPoint(x: self.bounds.size.width, y: self.bounds.size.height ))
        
        fillPath.addLine(to: CGPoint(x: self.bounds.size.width, y: 0))
        
        fillPath.close()
        context.addPath(fillPath.cgPath)
        context.setFillColor(bgColor.cgColor)
        context.fillPath()
      
    }
 
    func addMessage(textStr : String) -> Void {
        let label = UILabel(frame: CGRect(x: kArrowHeight + 5, y: 5, width: self.frame.size.width - (kArrowHeight * 2), height: self.frame.size.height - 10))
        label.text = textStr
        label.numberOfLines = 0
        label.textColor = labelTextColor
        label.font = labelTextFont
        self.addSubview(label)
    }
}
