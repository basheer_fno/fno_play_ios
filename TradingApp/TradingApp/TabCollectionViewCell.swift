//
//  TabCollectionViewCell.swift
//  TradingApp
//
//  Created by Pandiyaraj on 07/06/17.
//  Copyright © 2017 ideas2it. All rights reserved.
//

import UIKit

class TabCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var tabImageView: UIImageView!
    @IBOutlet weak var tabTitleLbl: UILabel!
    @IBOutlet weak var tabCancelBtn: UIButton!
    @IBOutlet weak var tabDetailLbl: UILabel!
    @IBOutlet weak var titleLabelTrailingConstraint: NSLayoutConstraint!
    var triView = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        triView.frame = CGRect(x: 0, y: 34, width: 10, height: 10)
        triView.triangledraw(triView.frame,corner: "BottomLeft")
        triView.backgroundColor = UIColor.init(hex: 0x75839D)//IColor.green
        //self.addSubview(triView)
    }
    func showCancelButtton(show:Bool)
    {
        if show {
            tabCancelBtn.isHidden = false
            self.titleLabelTrailingConstraint.constant = 45;
        }else{
            tabCancelBtn.isHidden = true
            self.titleLabelTrailingConstraint.constant = 10;
        }
    }
    
    func showPointerButtton(show:Bool)
    {
        if show {
            triView.isHidden = false
        }else{
            triView.isHidden = true
        }
    }
    @IBAction func tabCancelBtnAction(_ sender: Any) {
    }
}
