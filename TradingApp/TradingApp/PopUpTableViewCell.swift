//
//  PopUpTableViewCell.swift
//  TradingApp
//
//  Created by Pandiyaraj on 08/06/17.
//  Copyright © 2017 ideas2it. All rights reserved.
//

import UIKit

class PopUpTableViewCell: UITableViewCell {

//    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var companyProfitLbl: UILabel!
    @IBOutlet weak var companyFavBtn: UIButton!
    @IBOutlet weak var companySymbolLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
