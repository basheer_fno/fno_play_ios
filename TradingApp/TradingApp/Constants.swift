//
//  Constants.swift
//  MeetWisePOC
//
//  Created by Pandiyaraj on 10/01/17.
//  Copyright © 2017 ideas2it. All rights reserved.
//

import UIKit


enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}


struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE            = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static var IS_SIMULATOR: Bool {
        return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
    }
}


struct CommonValues {
  
    // HOST
    static let serverIp =   "http://nimblerest.lisuns.com:4531" //-Prod   //"http://test.lisuns.com:4531" //-Dev
  static let serverFnOIp =   "http://35.154.177.24:8081"  //"http://52.33.16.166:8081" //"http://54.149.155.38:8081" //-FnO Dev   //"http:/http://54.149.155.38:8081/test.lisuns.com:4531" //-Dev  // http://35.154.177.24:8081/configuration
  
    // API
    static let configAPI = "configuration"
    static let fnoHistory = "history"
    static let getinstruments = "GetInstruments"
    static let gethistory = "GetHistory"
    static let getLastQuote = "GetLastQuote"
  
    static let api_key = "edg9mtjdz9fophhq"
    static let api_secret = "mn6orrzy2gvn9ompgog247m8gjm4d84d"
    static let jsonApplication: String = "application/json"
    static let urlencoded: String = "application/x-www-form-urlencoded"
    static let accessKey = "14bf598e-6aa5-43ef-9e6a-e562de9bc90d"   //"b061ed91-7868-41ec-810c-7135d1ba520e"
    static let exchange = "NFO"
    
    static let Mode_Quote = "quote"
    static let Mode_Full = "full"
    static let Mode_ltp = "ltp"
    
    static let subscribe = "subscribe"
   
    
    static let isKiteConnect = false

    
    static let fileExtenstion = CommonValues.isKiteConnect ? "csv" : "json"
    
    static let instrumentFileName =  "temp.\(fileExtenstion)"
    
    static let yesterDayFileName = "temp\(Date().yesterday.currentDateStr).\(fileExtenstion)"
    
  
//    static let instrumentFileName = "instruments.\(fileExtenstion)"
    
    static let indianRpsUTF = "\u{20B9}"
    
    static let depositAmountValue = "depositAmount"
    static let depositFnOAmountValue = "depositFnOAmount"
    
    
    static let currentYear = "\(Date().currentYear)"
    static let nextYear = "\(Date().nextMonth.nextYearString)"
    
    static let xAxisDateFormat = "d/M \n H:mm"
    
    static let xAxisTodayDateFormat = "\nH:mm"
    static let secondDateFormat = "ss"

    
    
    
    
//    static let currentYear = 
//    static let indianRpsUTF = "$"//"\u{20B9}"
//    static let indianRpsUTF = "$"//"\u{20B9}"
    
    static let dataPointsPerMin:Double = 60.0
    
    static let minimumTimeIntervl = 5.0
    
    static let currentNifty = "NIFTY\(Date().currentYear)\(Date().currentMonthStr)FUT"
    
    static let symbolFile = "allSymbols"
    
    static let futureSegments = "futureSegments"
  
    static let instrumentIdentifier = CommonValues.isKiteConnect ? "instrument_token" : "IDENTIFIER"
    static let tradingSymbol = CommonValues.isKiteConnect ? "tradingsymbol" : "TRADESYMBOL"
    
    static let expiryStr = CommonValues.isKiteConnect ? "expiry" : "EXPIRY"
    static let dateFormat = CommonValues.isKiteConnect ? "yyyy-MM-dd" : "ddMMMyyyy"
    static let exchangeKey = CommonValues.isKiteConnect ? "exchange" : "EXCHANGE"
    
    static let lastDownloadedTime = "lastDownloadedTime"
  
    // Periodicity
    static let tick = "TICK"
    static let minute = "MINUTE"
    static let hour = "HOUR"
    static let day = "DAY"
    
    static let selectedSegmentIndex = "selectedIndex"
    
    static let tradeNowMarkerPoint = "TradeNowMarkerPoint"
    static let tradeCGPoint = "TradeCGPoint"
    static let tradeDirection = "TradeDirection"
    static let tradePriceYAxis = "tradePriceYAxis"
    
    static let networkMessage = "No network available. Please enable Wifi/Mobiledata to use the app"
    
    static let accessDeniedMsg = "Access Denied. Key Invalid."
    
    static let authRequestReceivedMsg = "Authentication request received. Try request data in next moment."
    
    static let dataUnavalibaleMsg = "Data unavailable."

}

struct AppFont {
    
    // if regularOrBold  == false Bold font
    static func pixelToPoint(_ pixels : CGFloat) -> CGFloat{
        let pointsPerInch : CGFloat = 72.0
        let scale : CGFloat = 1.0
        var pixelPerInch : CGFloat = 0.0
        if DeviceType.IS_IPAD {
            pixelPerInch = 132 * scale
        }else if DeviceType.IS_IPHONE{
            pixelPerInch = 163 * scale
        }
        let fontSize = pixels * pointsPerInch / pixelPerInch
        return fontSize
    }
    
    struct FontName {
        static let bold = "Avenir-Heavy" //"Roboto-Bold"
        static let regular = "Avenir-Roman" //"Roboto-Regular"
        static let medium = "Avenir-Medium" //"Roboto-Medium"
        static let light = "Avenir-Light"//"Roboto-Light"

    }
    
    static func getBold(pixels:CGFloat) -> UIFont {
        return UIFont.init(name: FontName.bold, size: pixelToPoint(pixels))!
    }
    
    static func getMedium(pixels:CGFloat) -> UIFont {
        return UIFont.init(name: FontName.medium, size: pixelToPoint(pixels))!
    }
    static func getRegular(pixels:CGFloat) -> UIFont {
        return UIFont.init(name: FontName.regular, size: pixelToPoint(pixels))!
    }
    
    static func getLight(pixels:CGFloat) -> UIFont {
        return UIFont.init(name: FontName.light, size: pixelToPoint(pixels))!
    }

}

struct DataPointTrailigPoint {
    
    static func tralingPoint(_ value : Double) -> Int{
        return Int(value - (value * 0.25))
    }
}

struct AppUtility {
  
  static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
    
    if let delegate = UIApplication.shared.delegate as? AppDelegate {
      delegate.orientationLock = orientation
    }
  }
  
  /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
  static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
    
    self.lockOrientation(orientation)
    
    UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
  }
  
}


