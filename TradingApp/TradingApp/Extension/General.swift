//
//  General.swift
//  SkyBravo
//
//  Created by Ideas2IT -Ragav on 02/12/16.
//  Copyright © 2016 ideas2it. All rights reserved.
//

import UIKit

extension Data {
    /// Returns a hex string from the given data
    ///
    /// - Returns: String
    func hexString() -> String {
        return self.reduce("") { string, byte in
            string + String(format: "%02X", byte)
        }
    }
}


extension Optional {
    
    /// Attempts to unwrap the optional, and executes the closure if a value exists
    ///
    /// - Parameter block: The closure to execute if a value is found
    /// - Throws: throws (Apply catch)
    public func unwrap( block: (Wrapped) throws -> ()) rethrows {
        if let value = self {
            try block(value)
        }
    }
}


extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func rupeesFormatt() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}

extension Int {
    func rupeesFormatt() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}


//MARK:- View Extension

extension UISearchBar {
    
    var textColor:UIColor? {
        get {
            if let textField = self.value(forKey: "searchField") as?
                UITextField  {
                return textField.textColor
            } else {
                return nil
            }
        }
        
        set (newValue) {
            if let textField = self.value(forKey: "searchField") as?
                UITextField  {
                textField.textColor = newValue
            }
        }
    }
}
extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func triangledraw(_ rect: CGRect,corner:String) {
        
        // Get Height and Width
        let layerHeight = layer.frame.height
        let layerWidth = layer.frame.width
        
        // Create Path
        let bezierPath = UIBezierPath()
        
        // Draw Points
        if corner == "BottomRight"{
            bezierPath.move(to: CGPoint(x: 0, y: layerHeight))
            bezierPath.addLine(to: CGPoint(x: layerWidth, y: layerHeight))
            bezierPath.addLine(to: CGPoint(x: layerWidth, y: 0))
            bezierPath.addLine(to: CGPoint(x: 0, y: layerHeight))
            bezierPath.lineJoinStyle = .round
        }
        else if corner == "BottomLeft"{
            bezierPath.move(to: CGPoint(x: 0, y: 0))
            bezierPath.addLine(to: CGPoint(x: 0, y: layerHeight))
            bezierPath.addLine(to: CGPoint(x: layerWidth, y: layerHeight))
            bezierPath.addLine(to: CGPoint(x: 0, y: 0))
            bezierPath.lineJoinStyle = .round
        }
        
        
        
        //        bezierPath.addLine(to: CGPoint(x: layerWidth / 2, y: 0))
        //        bezierPath.addLine(to: CGPoint(x: 0, y: layerHeight))
        bezierPath.close()
        
        // Apply Color
        UIColor.green.setFill()
        bezierPath.fill()
        
        // Mask to Path
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = bezierPath.cgPath
        layer.mask = shapeLayer
    }
}

// MARK:- Button Extension
extension UIButton{
    func roundCornersButton(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension NSMutableAttributedString{
    func setColorForText(_ textToFind: String, with color: UIColor) {
        let range = self.mutableString.range(of: textToFind, options: .caseInsensitive)
        if range.location != NSNotFound {
            addAttribute(NSForegroundColorAttributeName, value: color, range: range)
            addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 8), range: range)
        }
    }
}


public extension Double {
    /// Returns a random floating point number between 0.0 and 1.0, inclusive.
    public static var random:Double {
        get {
            return Double(arc4random()) / 0xFFFFFFFF
        }
    }
    /**
     Create a random number Double
     
     - parameter min: Double
     - parameter max: Double
     
     - returns: Double
     */
    public static func random(min: Double, max: Double) -> Double {
        return Double.random * (max - min) + min
    }
    
}

