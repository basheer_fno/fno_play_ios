//
//  LoginViewController.swift
//  TradingApp
//
//  Created by Pandiyaraj on 20/06/17.
//  Copyright © 2017 ideas2it. All rights reserved.
//

import UIKit

import UIKit

@IBDesignable
public class DottedLineView: UIView {
  
  @IBInspectable
  public var lineColor: UIColor = UIColor.black
  
  @IBInspectable
  public var lineWidth: CGFloat = CGFloat(4)
  
  @IBInspectable
  public var round: Bool = false
  
  @IBInspectable
  public var horizontal: Bool = true
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
    initBackgroundColor()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initBackgroundColor()
  }
  
  override public func prepareForInterfaceBuilder() {
    initBackgroundColor()
  }
  
  override public func draw(_ rect: CGRect) {
    
    let path = UIBezierPath()
    path.lineWidth = lineWidth
    
    if round {
      configureRoundPath(path: path, rect: rect)
    } else {
      configurePath(path: path, rect: rect)
    }
    
    lineColor.setStroke()
    
    path.stroke()
  }
  
  func initBackgroundColor() {
    if backgroundColor == nil {
      backgroundColor = UIColor.clear
    }
  }
  
  private func configurePath(path: UIBezierPath, rect: CGRect) {
    if horizontal {
      let center = rect.height * 0.5
      let drawWidth = rect.size.width - (rect.size.width.truncatingRemainder(dividingBy: lineWidth * 2)) + lineWidth
      let startPositionX = (rect.size.width - drawWidth) * 0.5 + lineWidth
      
      path.move(to: CGPoint(x: startPositionX, y: center))
      path.addLine(to: CGPoint(x: drawWidth, y: center))
      
    } else {
      let center = rect.width * 0.5
      let drawHeight = rect.size.height - (rect.size.height.truncatingRemainder(dividingBy: lineWidth * 2)) + lineWidth
      let startPositionY = (rect.size.height - drawHeight) * 0.5 + lineWidth
      
      path.move(to: CGPoint(x: center, y: startPositionY))
      path.addLine(to: CGPoint(x: center, y: drawHeight))
    }
    
    let dashes: [CGFloat] = [lineWidth, lineWidth]
    path.setLineDash(dashes, count: dashes.count, phase: 0)
    path.lineCapStyle = CGLineCap.butt
  }
  
  private func configureRoundPath(path: UIBezierPath, rect: CGRect) {
    if horizontal {
      let center = rect.height * 0.5
      let drawWidth = rect.size.width - (rect.size.width.truncatingRemainder(dividingBy: lineWidth * 2))
      let startPositionX = (rect.size.width - drawWidth) * 0.5 + lineWidth
      
      path.move(to: CGPoint(x: startPositionX, y: center))
      path.addLine(to: CGPoint(x: drawWidth, y: center))
      
    } else {
      let center = rect.width * 0.5
      let drawHeight = rect.size.height - (rect.size.height.truncatingRemainder(dividingBy: lineWidth * 2))
      let startPositionY = (rect.size.height - drawHeight) * 0.5 + lineWidth
      
      path.move(to: CGPoint(x: center, y: startPositionY))
      path.addLine(to: CGPoint(x: center, y: drawHeight))
    }
    
    let dashes: [CGFloat] = [0, lineWidth * 2]
    path.setLineDash(dashes, count: dashes.count, phase: 0)
    path.lineCapStyle = CGLineCap.round
  }
  
}

class LoginViewController: UIViewController,UIWebViewDelegate, UITextFieldDelegate {

    @IBOutlet var loginButton : UIButton!
  @IBOutlet var activityIndicatoryView : UIView!
  @IBOutlet weak var usernameTextField: FnOFloatingTextfield!
  @IBOutlet weak var passwordTextField: FnOFloatingTextfield!

    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      self.usernameTextField.delegate = self
      
      self.loginButton.isEnabled = false
      self.loginButton.isSelected = false

      self.downloadConfig()
      
      self.makePrefix()
      
      self.usernameTextField.text = "+919982600143" //"+918976509616"  //8108014333

      self.passwordTextField.text = "123456"
        
        
    }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    AppUtility.lockOrientation(.portrait)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    AppUtility.lockOrientation(.portrait)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    AppUtility.lockOrientation(.landscape)
  }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginBtnAction() ->Void{
      
//      ConfigManager.shared.userInfo.userId = "2"
        // Check the internet connection. If connected do the process.
        if  Reachability()?.isReachable == true{
          ConfigManager.shared.userInfo.userName = self.usernameTextField.text ?? ""
          ConfigManager.shared.userInfo.userName = ConfigManager.shared.userInfo.userName.replace(" ", with: "")
          ConfigManager.shared.userInfo.userName = ConfigManager.shared.userInfo.userName.replace("+91", with: "")
          DispatchQueue.main.async {
            self.activityIndicatoryView.isHidden = false
            self.view.bringSubview(toFront: self.activityIndicatoryView)
          }
            self.performLoginAPI()
        }else{
            self.showAlert(title: "Error", contentText: CommonValues.networkMessage, actions: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

  func textFieldDidBeginEditing(_ textField: UITextField) {
    
    self.usernameTextField.modifyClearButtonWithImage(image: UIImage.init(named: "loginClearBG")!)
    self.passwordTextField.modifyClearButtonWithImage(image: UIImage.init(named: "loginClearBG")!)
  }
  
  func makePrefix() {
    let attributedString = NSMutableAttributedString(string: "+ 91")
    attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSMakeRange(0,4))
    usernameTextField.attributedText = attributedString
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //This makes the new text black.
    
    if textField == usernameTextField {
      
      textField.typingAttributes = [NSForegroundColorAttributeName:UIColor.white]
      let protectedRange = NSMakeRange(0, 4)
      let intersection = NSIntersectionRange(protectedRange, range)
      if intersection.length > 0 {
        
        return false
      }
      if range.location == 15 {
        self.showRigthView()
        
        return true
      }
      else if range.location > 15 {
        self.showRigthView()
        
        return false
      }
      else {
        self.usernameTextField.rightView?.isHidden = true
      }
//      if range.location + range.length > 12 {
//        self.showRigthView()
//        return false
//      }
      
      self.usernameTextField.text = self.formattedNumber(number: self.usernameTextField.text!)
      return true
    }
    else if textField == passwordTextField {
      if (textField.text?.count)! < 6 {
        return true
      }
      else {
        return false
      }
    }
    return true
  }
  
  func showRigthView () {
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
    let image = UIImage(named: "loginValidated")
    imageView.image = image
    usernameTextField.rightView = imageView
    usernameTextField.rightViewMode = .always
    self.usernameTextField.rightView?.isHidden = false
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    
    // check username and password has text
    if usernameTextField.text != "" && passwordTextField.text != "" {
      self.loginButton.isEnabled = true
      self.loginButton.isSelected = true
    }
    else {
      self.loginButton.isEnabled = false
      self.loginButton.isSelected = false
    }
  }
  
  func performLoginAPI() {
    let urlString = ConfigManager.shared.apiURL.loginURL
    //history? identifierId =FUTIDX_NIFTY_22FEB2018_XX_0&periodicity=TICK&max=100
    
    var mainDict = [String : Any]()
    mainDict["deviceInfo"] = self.getDeviceInfo()
    mainDict["userInfo"] = self.getLoginUserInfo()
    
    
    var requestDict = [String : Any]()
    requestDict["serviceRequest"] = mainDict
    print("request body \(requestDict)")
    
    NetworkUtilities.sendAsynchronousRequestToServer(actionName: urlString, httpMethod: "POST", requestBody: requestDict as AnyObject, contentType: CommonValues.jsonApplication) { (obj) in
      print("obj \(obj)")
      if (obj as? NSDictionary != nil){
        
        DispatchQueue.main.async {
          self.activityIndicatoryView.isHidden = true
        }
        
        guard obj.allKeys != nil else {
          self.showAlert(title: "Login Failed", contentText: "" , actions: nil)
          return
        }
        guard obj.allKeys.count > 0 else {
          self.showAlert(title: "Login Failed", contentText: "" , actions: nil)
          return
        }
        
        let serviceResponse = (obj as? NSDictionary)?.value(forKey: "serviceResponse") as! [String : Any]
        
        guard serviceResponse != nil else {
          self.showAlert(title: "Login Failed", contentText: "" , actions: nil)
          return
        }
        
        let responseInfo = serviceResponse["responseInfo"] as! [String : Any]
        
        guard responseInfo["responseCode"] != nil else {
          self.showAlert(title: "Login Failed", contentText: "" , actions: nil)
          return
        }
        
        if (responseInfo["responseCode"] as AnyObject).intValue  == 1 {
          // login failed
          DispatchQueue.main.async {
            if self.passwordTextField.text == "1111" {
              DispatchQueue.main.async {
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                self.activityIndicatoryView.isHidden = true
                self.view.sendSubview(toBack: self.activityIndicatoryView)
                
                self.appDelegate.setRootViewController()
              }
            }
            else {
              self.showAlert(title: "Login Failed", contentText: "" , actions: nil)
            }
          }
          
          
        }
        else if (responseInfo["responseCode"] as AnyObject).intValue  == 0 {
          
          guard serviceResponse["userInfo"] != nil else {
            return
          }
          
          let userInfo = serviceResponse["userInfo"] as! [String : Any]
          
          guard userInfo["userId"] != nil else {
            return
          }
          
          ConfigManager.shared.userInfo.userId = (userInfo["userId"] as AnyObject) as! String
          
          
//          guard userInfo["balance"] != nil else {
//            return
//          }
//          
//          let currentDepositAmountValue = (userInfo["balance"] as AnyObject).doubleValue
//          
//          DispatchQueue.main.async {
//            UserDefaults.standard.set(currentDepositAmountValue, forKey: CommonValues.depositFnOAmountValue)
//            UserDefaults.standard.synchronize()
//          }
          
          // login success
          DispatchQueue.main.async {
            UserDefaults.standard.set(true, forKey: "isLoggedIn")
            self.activityIndicatoryView.isHidden = true
            self.view.sendSubview(toBack: self.activityIndicatoryView)
            
            self.appDelegate.setRootViewController()
          }
        }
        else {
          self.showAlert(title: "Login Failed", contentText: "" , actions: nil)
        }
      }
      
    }
  }
  
  func getDeviceInfo() -> [String: Any] {
    var deviceInfo = [String : Any]()
    deviceInfo["ip"] = ""
    deviceInfo["deviceId"] = ""
    deviceInfo["os"] = "ios"
    deviceInfo["geoCode"] = ""
    return deviceInfo
  }
  
  func getUserInfo() -> [String: Any] {
    var userInfo = [String : Any]()
    userInfo["userId"] = ConfigManager.shared.userInfo.userId
    return userInfo
  }
  
  func getLoginUserInfo() -> [String: Any] {
    var userInfo = [String : Any]()
    userInfo["userName"] = ConfigManager.shared.userInfo.userName
    userInfo["password"] = self.passwordTextField.text
    return userInfo
  }
  
  
  func downloadConfig() {
    let urlString =  "\(CommonValues.serverFnOIp)/\(CommonValues.configAPI)"
    NetworkUtilities.downloadConfigFromServer(url: urlString, completionHandler: { (object) in
      
      
      print(object)
      let response = object as? [String : Any]
      guard response!["apiUrls"] != nil else {
        return
      }
      
      // API URLs
      let apiurlDict = response!["apiUrls"] as! [String : Any]
      guard apiurlDict["feedServiceUrl"] != nil else {
        return
      }
        
      ConfigManager.shared.apiURL.feedServiceURL = apiurlDict["feedServiceUrl"] as! String // "http://13.127.73.230:8085/history"
        
      guard apiurlDict["calculatePointsUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.calcualtePointsURL = apiurlDict["calculatePointsUrl"] as! String
      
      guard apiurlDict["placeOrderUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.placeOrderURL = apiurlDict["placeOrderUrl"] as! String
      
      guard apiurlDict["orderDetailUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.orderDetailURL = apiurlDict["orderDetailUrl"] as! String
      
      guard apiurlDict["closeOrderUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.closeOrderURL = apiurlDict["closeOrderUrl"] as! String
      
      guard apiurlDict["loginUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.loginURL = apiurlDict["loginUrl"] as! String
      
      guard apiurlDict["balanceUrl"] != nil else {
        return
      }
      ConfigManager.shared.apiURL.getBalanceURL = apiurlDict["balanceUrl"] as! String
      
      
      // App Version
      guard response!["updateAppVersion"] != nil else {
        return
      }
      let appversionDict = response!["updateAppVersion"] as! [String : Any]
      guard appversionDict["appVersion"] != nil else {
        return
      }
      ConfigManager.shared.appVersion.appVersion = Double(appversionDict["appVersion"] as! String)!
      
      guard appversionDict["cache"] != nil else {
        return
      }
      ConfigManager.shared.appVersion.cache = Int(appversionDict["cache"] as! String)!
      
      guard appversionDict["forceUpdate"] != nil else {
        return
      }
      ConfigManager.shared.appVersion.forceUpdate = (appversionDict["forceUpdate"] as! String)
      
      guard appversionDict["maintainanceMessage"] != nil else {
        return
      }
      ConfigManager.shared.appVersion.maintainanceMessage = appversionDict["maintainanceMessage"] as! String
      
      guard response!["exchange"] != nil else {
        return
      }
      let exchangeDict = response!["exchange"] as! [String : Any]
      
      guard exchangeDict["currency"] != nil else {
        return
      }
      ConfigManager.shared.exchange.currency = exchangeDict["currency"] as! String
      
      guard exchangeDict["identifierId"] != nil else {
        return
      }
      ConfigManager.shared.exchange.identifierId = exchangeDict["identifierId"] as! String
      
      
      
      guard exchangeDict["name"] != nil else {
        return
      }
      ConfigManager.shared.exchange.name = exchangeDict["name"] as! String
      
      guard exchangeDict["amount"] != nil else {
        return
      }
      ConfigManager.shared.exchange.amount = exchangeDict["amount"] as! [Int]
      
      
      DispatchQueue.main.async {
        self.activityIndicatoryView.isHidden = true
        self.view.sendSubview(toBack: self.activityIndicatoryView)
        
      }
      
    })
  }
  
  
  
  
  func formattedNumber(number: String) -> String {
    var cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    var mask = "+ XX XXXXX XXXXX"
    
    var result = ""
    var index = cleanPhoneNumber.startIndex
    for ch in mask.characters {
      if index == cleanPhoneNumber.endIndex {
        break
      }
      if ch == "X" {
        result.append(cleanPhoneNumber[index])
        index = cleanPhoneNumber.index(after: index)
      } else {
        result.append(ch)
      }
    }
    return result
  }
  
}
